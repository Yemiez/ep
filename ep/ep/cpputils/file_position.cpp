/********************************************************
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*			   file_position.cpp				*	*
*  	*			   Created by Yamiez				*	*
*  	*	Do not forget to give credit when using		*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*********************************************************/
#include "file_position.h"

cpputils::file_position::file_position( )
	: row_( 0 ), column_( 0 )
{ }

cpputils::file_position::file_position( int row, int column )
	: row_( std::move( row ) ), column_( std::move( column ) )
{ }

std::string cpputils::file_position::pretty_format( ) const
{
	return "(" + std::to_string( row_ ) + ", " + std::to_string( column_ ) + ")";
}

int & cpputils::file_position::get_row( )
{
	return row_;
}

const int & cpputils::file_position::get_row( ) const
{
	return row_;
}

int & cpputils::file_position::get_column( )
{
	return column_;
}

const int & cpputils::file_position::get_column( ) const
{
	return column_;
}
