/********************************************************
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*			     tokenizer.cpp					*	*
*  	*			   Created by Yamiez				*	*
*  	*	Do not forget to give credit when using		*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*********************************************************/
#include "tokenizer.h"
#include <sstream>

cpputils::cpp_token::cpp_token( )
	: pos_( ), block_( ), kw_( ), op_( op_none ), empty_( true )
{ }

cpputils::cpp_token::cpp_token( file_position pos, std::string block, token_keyword kw )
	: pos_( std::move( pos ) ), block_( std::move( block ) ), kw_( std::move( kw ) ), op_( op_none ),
	  tk_( token_type::tk_keyword ), empty_( false )
{ }

cpputils::cpp_token::cpp_token( file_position pos, std::string block, token_operator op, int presedence, bool is_left_to_right )
	: pos_( std::move( pos ) ), block_( std::move( block ) ), op_( std::move( op ) ), kw_( ),
	  tk_( token_type::tk_operator ), presedence_( std::move( presedence ) ), ltor_( std::move( is_left_to_right ) ), empty_( false )
{ }

cpputils::cpp_token::cpp_token( file_position pos, std::string block, token_type tk )
	: pos_( std::move( pos ) ), block_( std::move( block ) ), tk_( std::move( tk ) ),
	  kw_( ), op_( op_none ), empty_( false )
{ }

cpputils::cpp_token::token_type & cpputils::cpp_token::get_type( )
{
	return tk_;
}

cpputils::file_position & cpputils::cpp_token::get_position( )
{
	return pos_;
}

std::string & cpputils::cpp_token::get_block( )
{
	return block_;
}

cpputils::cpp_token::token_keyword & cpputils::cpp_token::get_keyword( )
{
	return kw_;
}

cpputils::cpp_token::token_operator & cpputils::cpp_token::get_operator( )
{
	return op_;
}

int & cpputils::cpp_token::get_presedence( )
{
	return presedence_;
}

const cpputils::cpp_token::token_type & cpputils::cpp_token::get_type( ) const
{
	return tk_;
}

const cpputils::file_position & cpputils::cpp_token::get_position( ) const
{
	return pos_;
}

const std::string & cpputils::cpp_token::get_block( ) const
{
	return block_;
}

const cpputils::cpp_token::token_keyword & cpputils::cpp_token::get_keyword( ) const
{
	return kw_;
}

const cpputils::cpp_token::token_operator & cpputils::cpp_token::get_operator( ) const
{
	return op_;
}

const int & cpputils::cpp_token::get_presedence( ) const
{
	return presedence_;
}

bool cpputils::cpp_token::is_keyword( ) const
{
	return tk_ == tk_keyword;
}

bool cpputils::cpp_token::is_operator( ) const
{
	return tk_ == tk_operator;
}

bool cpputils::cpp_token::is_decimal( ) const
{
	return tk_ == tk_decimal;
}

bool cpputils::cpp_token::is_hexadecimal( ) const
{
	return tk_ == tk_hexadecimal;
}

bool cpputils::cpp_token::is_string( ) const
{
	return tk_ == tk_string;
}

bool cpputils::cpp_token::is_identifier( ) const
{
	return tk_ == tk_identifier;
}

bool cpputils::cpp_token::is_right_associated( ) const
{
	return !ltor_;
}

bool cpputils::cpp_token::is_left_associated( ) const
{
	return ltor_;
}

bool cpputils::cpp_token::is_assignment_operator( ) const
{
	return this->op_ >= op_assign && this->op_ <= op_assign_bitwise_xor;
}

bool cpputils::cpp_token::is_empty( ) const
{
	return empty_;
}

std::string cpputils::cpp_token::pretty_format( ) const
{
	std::ostringstream stream;
	stream << "cpp_token" << pos_.pretty_format( ) << std::endl << "^\tvalue(\"" << block_ << "\")";
	return stream.str( );
}

cpputils::cpp_tokenizer::cpp_tokenizer( )
	: tokens_( ), fpc_( )
{ }

void cpputils::cpp_tokenizer::tokenize( const std::string & contents )
{ 
	reset( );
	auto it = contents.begin( ), end = contents.end( );
	while ( it < end )
	{
		skip_block( it, end, fpc_ );
		if ( !( it < end ) )
			break;
		else if ( is_comment( it, end ) )
			eat_comment( it, end );
		else if ( is_multi_comment( it, end ) )
			eat_multi_comment( it, end );
		else if ( is_alpha( it, end ) )
			tokens_.emplace_back( std::move( get_alpha( it, end ) ) );
		else if ( is_hexa( it, end ) )
			tokens_.emplace_back( std::move( get_hexa( it, end ) ) );
		else if ( is_decimal( it, end ) )
			tokens_.emplace_back( std::move( get_decimal( it, end ) ) );
		else if ( is_string( it, end ) )
			tokens_.emplace_back( std::move( get_string( it, end ) ) );
		else if ( *it == '\0' )
			break;
		else
			tokens_.emplace_back( std::move( get_operator( it, end ) ) );
	}

	for ( auto it = tokens_.begin( ), end = tokens_.end( );
		  it < end;
		  ++it )
	{
		if ( it->is_empty( ) )
		{
			it = tokens_.erase( it );
			end = tokens_.end( );
		}
	}
}

void cpputils::cpp_tokenizer::tokenize_file( const std::string & filename )
{ 
	return tokenize( load_file( filename ) );
}

std::vector<cpputils::cpp_token>& cpputils::cpp_tokenizer::get_tokens( )
{
	return tokens_;
}

const std::vector<cpputils::cpp_token>& cpputils::cpp_tokenizer::get_tokens( ) const
{
	return tokens_;
}

std::vector<cpputils::cpp_token>::iterator cpputils::cpp_tokenizer::begin( )
{
	return tokens_.begin( );
}

std::vector<cpputils::cpp_token>::iterator cpputils::cpp_tokenizer::end( )
{
	return tokens_.end( );
}

void cpputils::cpp_tokenizer::reset( )
{ 
	fpc_.get_row( ) = fpc_.get_column( ) = 1;
	tokens_.clear( );
}

std::string cpputils::cpp_tokenizer::load_file( const std::string & filename )
{
	return std::string( std::istreambuf_iterator<char>( std::ifstream( filename ) ), std::istreambuf_iterator<char>( ) );
}

void cpputils::cpp_tokenizer::skip_block( iterator &it, const iterator &end, file_position & pos )
{
	while ( it < end )
		if ( *it == '\n' || *it == '\r\n' || *it == '\r' )
			increment( it, pos );
		else if ( *it == ' ' || *it == '\t' )
			increment( it, pos );
		else
			break;
}

void cpputils::cpp_tokenizer::eat_comment( iterator & it, const iterator & end )
{ 
	while ( it < end && *it != '\n' )
		increment( it, fpc_ );
	if ( it < end )
		increment( it, fpc_ );
}

void cpputils::cpp_tokenizer::eat_multi_comment( iterator & it, const iterator & end )
{ 
	while( it < end )
	{
		if ( *it == '*' && ( it + 1 ) < end && *( it + 1 ) == '/' )
			break;
		increment( it, fpc_ );
	}
	increment( it, fpc_ ); // '*'
	increment( it, fpc_ ); // '/'
}

void cpputils::cpp_tokenizer::increment( iterator & it, file_position & pos )
{ 
	auto &row = pos.get_row( ), &column = pos.get_column( );
	if ( *( it++ ) == '\n' )
		++row,
		column = 1;
	else
		++column;
}

bool cpputils::cpp_tokenizer::is_comment( const iterator & it, const iterator & end ) const
{
	return *it == '/' && ( it + 1 ) < end && *( it + 1 ) == '/';
}

bool cpputils::cpp_tokenizer::is_multi_comment( const iterator & it, const iterator & end ) const
{
	return *it == '/' && ( it + 1 ) < end && *( it + 1 ) == '*';
}

bool cpputils::cpp_tokenizer::is_alpha( const iterator & it, const iterator & end ) const
{
	return ::isalpha( *it ) || *it == '_';
}

bool cpputils::cpp_tokenizer::is_decimal( const iterator & it, const iterator & end ) const
{
	return ::isdigit( *it );
}

bool cpputils::cpp_tokenizer::is_hexa( const iterator & it, const iterator & end ) const
{
	return *it == '0' && ( ( it + 1 ) < end ) && *( it + 1 ) == 'x';
}

bool cpputils::cpp_tokenizer::is_string( const iterator & it, const iterator & end ) const
{
	return *it == '"';
}

cpputils::cpp_token cpputils::cpp_tokenizer::get_alpha( iterator & it, const iterator & end )
{
	auto beg = it;
	increment( it, fpc_ );
	while ( it < end && ( is_alpha( it, end ) || is_decimal( it, end ) ) )
		increment( it, fpc_ );
	return cpp_token( fpc_, std::string( beg, it ), cpp_token::tk_identifier );
}

cpputils::cpp_token cpputils::cpp_tokenizer::get_decimal( iterator & it, const iterator & end )
{
	auto beg = it;
	increment( it, fpc_ );
	while ( it < end && ( is_decimal( it, end ) ) )
		increment( it, fpc_ );
	return cpp_token( fpc_, std::string( beg, it ), cpp_token::tk_decimal );
}

cpputils::cpp_token cpputils::cpp_tokenizer::get_hexa( iterator & it, const iterator & end )
{
	auto beg = it;
	increment( it, fpc_ );
	increment( it, fpc_ );
	while ( it < end && ::isxdigit( *it ) )
		increment( it, fpc_ );
	return cpp_token( fpc_, std::string( beg, it ), cpp_token::tk_hexadecimal );
}

cpputils::cpp_token cpputils::cpp_tokenizer::get_string( iterator & it, const iterator & end )
{
	increment( it, fpc_ );
	auto beg = it;
	while ( it < end && *it != '"' )
	{
		if ( *it == '\\' && *( it + 1 ) == '"' )
		{
			increment( it, fpc_ );
			increment( it, fpc_ );
			continue;
		}

		if ( *it == '\n' )
			throw cpp_error( "unexpected endline when parsing string", fpc_ );
		increment( it, fpc_ );
	}
	increment( it, fpc_ );
	return cpp_token( fpc_, std::string( beg, it - 1 ), cpp_token::tk_string );
}

cpputils::cpp_token cpputils::cpp_tokenizer::get_operator( iterator & it, const iterator & end )
{
	switch ( *it )
	{
	case '=':
		increment( it, fpc_ );
		if ( *it == '=' )
		{
			increment( it, fpc_ );
			return cpp_token( fpc_, "==", cpp_token::op_equal_to, 9, true );
		}
		return cpp_token( fpc_, "=", cpp_token::op_assign, 15 );
		break;
	case ':':
		increment( it, fpc_ );
		if ( *it == ':' )
		{
			increment( it, fpc_ );
			return cpp_token( fpc_, "::", cpp_token::op_scope_resolution, 1, true );
		}
		return cpp_token( fpc_, ":", cpp_token::op_colon, 1, true );
		break;
	case '+':
		increment( it, fpc_ );
		if ( *it == '+' )
		{
			increment( it, fpc_ );
			return cpp_token( fpc_, "++", cpp_token::op_postfix_increment, 1 );
		}
		else if ( *it == '=' )
		{
			increment( it, fpc_ );
			return cpp_token( fpc_, "+=", cpp_token::op_assign_add, 15 );
		}
		return cpp_token( fpc_, "+", cpp_token::op_unary_add, 6, true );
		break;
	case '-':
		increment( it, fpc_ );
		if ( *it == '-' )
		{
			increment( it, fpc_ );
			return cpp_token( fpc_, "--", cpp_token::op_postfix_decrement, 1 );
		}
		else if ( *it == '=' )
		{
			increment( it, fpc_ );
			return cpp_token( fpc_, "-=", cpp_token::op_assign_sub, 15 );
		}
		else if ( *it == '>' )
		{
			increment( it, fpc_ );
			return cpp_token( fpc_, "->", cpp_token::op_ptm_access, 4, true );
		}
		else if ( ::isdigit( *it ) )
		{
			auto dec = get_decimal( it, end );
			dec.get_block( ).insert( dec.get_block( ).begin( ), '-' );
			return dec;
		}

		return cpp_token( fpc_, "-", cpp_token::op_unary_sub, 6, true );
		break;
	case '*':
		increment( it, fpc_ );
		if ( *it == '=' )
		{
			increment( it, fpc_ );
			return cpp_token( fpc_, "*=", cpp_token::op_assign_mul, 15 );
		}
		return cpp_token( fpc_, "*", cpp_token::op_mul, 5, true );
		break;
	case '/':
		increment( it, fpc_ );
		if ( *it == '=' )
		{
			increment( it, fpc_ );
			return cpp_token( fpc_, "/=", cpp_token::op_assign_div, 15 );
		}
		return cpp_token( fpc_, "/", cpp_token::op_div, 5, true );
		break;
	case '&':
		increment( it, fpc_ );
		if ( *it == '&' )
		{
			increment( it, fpc_ );
			return cpp_token( fpc_, "&&", cpp_token::op_logical_and, 13, true );
		}
		else if ( *it == '=' )
		{
			increment( it, fpc_ );
			return cpp_token( fpc_, "&=", cpp_token::op_assign_bitwise_and, 15 );
		}
		return cpp_token( fpc_, "&", cpp_token::op_bitwise_and, 10, true );
		break;
	case '|':
		increment( it, fpc_ );
		if ( *it == '|' )
		{
			increment( it, fpc_ );
			return cpp_token( fpc_, "||", cpp_token::op_logical_or, 14, true );
		}
		else if ( *it == '=' )
		{
			increment( it, fpc_ );
			return cpp_token( fpc_, "|=", cpp_token::op_assign_bitwise_or, 15 );
		}
		return cpp_token( fpc_, "|", cpp_token::op_bitwise_or, 12, true );
		break;
	case '^':
		increment( it, fpc_ );
		if ( *it == '=' )
		{
			increment( it, fpc_ );
			return cpp_token( fpc_, "^=", cpp_token::op_assign_bitwise_xor, 15 );
		}
		return cpp_token( fpc_, "^", cpp_token::op_bitwise_xor, 11, true );
		break;
	case '>':
		increment( it, fpc_ );
		if ( *it == '>' )
		{
			increment( it, fpc_ );
			if ( *it == '=' )
			{
				increment( it, fpc_ );
				return cpp_token( fpc_, ">>=", cpp_token::op_assign_bitwise_right, 15 );
			}
			return cpp_token( fpc_, ">>", cpp_token::op_bitwise_right, 7, true );
		}
		else if ( *it == '=' )
		{
			increment( it, fpc_ );
			return cpp_token( fpc_, ">=", cpp_token::op_greater_than_equals, 8, true );
		}
		return cpp_token( fpc_, ">", cpp_token::op_greater, 8, true );
		break;
	case '<':
		increment( it, fpc_ );
		if ( *it == '<' )
		{
			increment( it, fpc_ );
			if ( *it == '=' )
			{
				increment( it, fpc_ );
				return cpp_token( fpc_, "<<=", cpp_token::op_assign_bitwise_left, 15 );
			}
			return cpp_token( fpc_, "<<", cpp_token::op_bitwise_left, 6, true );
		}
		else if ( *it == '=' )
		{
			increment( it, fpc_ );
			return cpp_token( fpc_, "<=", cpp_token::op_less_than_equals, 8, true );
		}
		return cpp_token( fpc_, "<", cpp_token::op_less, 8, true );
		break;
	case '%':
		increment( it, fpc_ );
		if ( *it == '=' )
		{
			increment( it, fpc_ );
			return cpp_token( fpc_, "%=", cpp_token::op_assign_modulu, 15 );
		}
		return cpp_token( fpc_, "%", cpp_token::op_modulo, 5, true );
		break;
	case '!':
		increment( it, fpc_ );
		if ( *it == '=' )
		{
			increment( it, fpc_ );
			return cpp_token( fpc_, "!=", cpp_token::op_not_equal_to, 9 );
		}
		return cpp_token( fpc_, "!", cpp_token::op_logical_not, 3 );
		break;
	case '~':
		increment( it, fpc_ );
		return cpp_token( fpc_, "~", cpp_token::op_bitwise_not, 3 );
	case ',':
		increment( it, fpc_ );
		return cpp_token( fpc_, ",", cpp_token::op_comma, 16 );
	case '.':
		increment( it, fpc_ );
		if ( *it == '=' )
		{
			increment( it, fpc_ );
			return cpp_token( fpc_, ".*", cpp_token::op_ptm_period, 3, true );
		}
		return cpp_token( fpc_, ".", cpp_token::op_period, 3 );
	case '(':
		increment( it, fpc_ );
		return cpp_token( fpc_, "(", cpp_token::op_roundbracket_open, 1 );
	case ')':
		increment( it, fpc_ );
		return cpp_token( fpc_, ")", cpp_token::op_roundbracket_close, 1 );
	case '[':
		increment( it, fpc_ );
		return cpp_token( fpc_, "[", cpp_token::op_squarebracket_open, 1 );
	case ']':
		increment( it, fpc_ );
		return cpp_token( fpc_, "]", cpp_token::op_squarebracket_close, 1 );
	case '{':
		increment( it, fpc_ );
		return cpp_token( fpc_, "{", cpp_token::op_squigglybracket_open, 1 );
	case '}':
		increment( it, fpc_ );
		return cpp_token( fpc_, "}", cpp_token::op_squigglybracket_close, 1 );
	case ';':
		increment( it, fpc_ );
		return cpp_token( fpc_, ";", cpp_token::op_semicolon, 1 );
	case '#':
		increment( it, fpc_ );
		return cpp_token( fpc_, "#", cpp_token::op_preprocessor_hash, 1 );
	case '\'':
	{
		auto beg_it = it++;
		while ( ( *it ) != '\'' && it < end )
			increment( it, fpc_ );
		if ( *it != '\'' )
			throw cpp_error( "Unmatched '", fpc_ );
		increment( it, fpc_ );
		return cpp_token( fpc_, { beg_it, it }, cpp_token::tk_identifier );
	}
	default:
		break;
	}
	return cpp_token( );
}

cpputils::cpp_error::cpp_error( const char * reason, file_position pos )
	: std::exception( reason ), pos_( std::move( pos ) )
{ }

cpputils::cpp_error::cpp_error( std::string reason, file_position pos )
	: cpp_error( reason.c_str( ), std::move( pos ) )
{ }

std::string cpputils::cpp_error::pretty_format( ) const
{
	std::ostringstream stream;
	stream << "cpp_error at file_position" << pos_.pretty_format( ) << std::endl
		   << "^\t" << what( ) << std::endl;
	return stream.str( );
}
