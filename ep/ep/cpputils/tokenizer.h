/********************************************************
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*			      tokenizer.h					*	*
*  	*			   Created by Yamiez				*	*
*  	*	Do not forget to give credit when using		*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*********************************************************/
#pragma once
#include <fstream>
#include <vector>
#include <sstream>
#include "file_position.h"

namespace cpputils
{

	class cpp_token : public cpp_formatable
	{
	public:
		enum token_type
		{
			tk_string,
			tk_decimal,
			tk_hexadecimal,
			tk_operator,
			tk_keyword,
			tk_identifier,
		};

		enum token_keyword
		{
			kw_alignas, kw_alignof, kw_and, kw_and_eq, kw_asm, kw_atomic_cancel, kw_atomic_commit,
			kw_atomic_noexcept, kw_auto, kw_bitand, kw_bitor, kw_bool, kw_break, kw_case, kw_catch, kw_char,
			kw_char16_t, kw_char32_t, kw_class, kw_compl, kw_concept, kw_const, kw_constexpr, kw_const_cast,
			kw_continue, kw_decltype, kw_default, kw_delete, kw_do, kw_double, kw_dynamic_cast, kw_else,
			kw_enum, kw_explicit, kw_export, kw_extern, kw_false, kw_float, kw_for, kw_friend, kw_goto,
			kw_if, kw_inline, kw_int, kw_import, kw_long, kw_module, kw_mutable, kw_namespace, kw_new,
			kw_noexcept, kw_not_eq, kw_nullptr, kw_operator, kw_or, kw_or_eq, kw_private, kw_protected,
			kw_public, kw_register, kw_reinterpret_cast, kw_requires, kw_return, kw_short, kw_signed,
			kw_sizeof, kw_static, kw_static_assert, kw_static_cast, kw_struct, kw_switch, kw_synchronized,
			kw_template, kw_this, kw_thread_local, kw_throw, kw_true, kw_try, kw_typedef, kw_typeid,
			kw_typename, kw_union, kw_unsigned, kw_using, kw_virtual, kw_void, kw_volatile, kw_wchar_t,
			kw_while, kw_xor, kw_xor_eq, kw_override, kw_final, kw_transaction_safe, kw_transaction_safe_dynamic,
			kw_unknown
		};

		enum token_operator
		{
			// Presedence 1 is highest-

			/* 1 */
			op_scope_resolution, op_colon,
			op_semicolon, op_single_quote,

			/* 2 */
			op_postfix_increment, op_postfix_decrement,
			op_roundbracket_open, op_roundbracket_close,
			op_squarebracket_open, op_squarebracket_close,
			op_squigglybracket_open, op_squigglybracket_close,
			op_access, op_period,

			/* 3 */
			op_unary_add, op_unary_sub,
			/* op_prefix_increment,  op_prefix_decrement, */
			op_logical_not, op_bitwise_not,
			op_indirect, op_bitwise_and,

			/* 4 */
			op_ptm_access, op_ptm_period,


			/* 5 */
			op_mul, op_div,
			op_modulo,

			/* 6 */
			op_bitwise_left, op_bitwise_right,

			/* 7 */
			op_less, op_greater,
			op_less_than_equals, op_greater_than_equals,

			/* 8 */
			op_equal_to, op_not_equal_to,

			/* 9 */
			op_bitwise_xor, op_bitwise_or,
			op_logical_and, op_logical_or,
			op_ternary,

			/* 10 */
			op_assign, op_assign_add,
			op_assign_sub, op_assign_div,
			op_assign_mul, op_assign_modulu,
			op_assign_bitwise_left, op_assign_bitwise_right,
			op_assign_bitwise_and, op_assign_bitwise_or,
			op_assign_bitwise_xor,

			/* 11 */
			op_comma, op_preprocessor_hash,

			// None
			op_none
		};

	public:
		cpp_token( );
		cpp_token( file_position pos, std::string block, token_keyword kw );
		cpp_token( file_position pos, std::string block, token_operator op, int presedence, bool is_left_to_right = false );
		cpp_token( file_position pos, std::string block, token_type tk );
		
		token_type &get_type( );

		file_position &get_position( );

		std::string &get_block( );

		token_keyword &get_keyword( );

		token_operator &get_operator( );
		
		int &get_presedence( );

		const token_type &get_type( ) const;

		const file_position &get_position( ) const;

		const std::string &get_block( ) const;

		const token_keyword &get_keyword( ) const;

		const token_operator &get_operator( ) const;
		
		const int &get_presedence( ) const;

		bool is_keyword( ) const;

		bool is_operator( ) const;

		bool is_decimal( ) const;

		bool is_hexadecimal( ) const;

		bool is_string( ) const;

		bool is_identifier( ) const;
		
		bool is_right_associated( ) const;

		bool is_left_associated( ) const;

		bool is_assignment_operator( ) const;
		
		bool is_empty( ) const;

		std::string pretty_format( ) const override;

		template<typename T>
		T get_t( )
		{
			T t = T( );
			std::istringstream stream( get_block( ) );
			if ( is_decimal( ) )
				stream >> t;
			else if ( is_hexadecimal() )
				stream >> std::hex >> t;
			else
				stream >> t;
			return t;
		}

	private:
		file_position pos_;
		std::string block_;
		token_keyword kw_;
		token_operator op_;
		token_type tk_;
		int presedence_;
		bool ltor_, empty_;
	};
	
	class cpp_tokenizer
	{
	public:
		using iterator = std::string::const_iterator;
	
	public:
		cpp_tokenizer( );

		void tokenize( const std::string &contents );
		
		void tokenize_file( const std::string &filename );

		std::vector<cpp_token> &get_tokens( );

		const std::vector<cpp_token> &get_tokens( ) const;

		std::vector<cpp_token>::iterator begin( );

		std::vector<cpp_token>::iterator end( );

		void reset( );

	private:
		
		std::string load_file( const std::string &filename );
		
		void skip_block( iterator &it, const iterator &end, file_position &pos );

		void eat_comment( iterator &it, const iterator &end );

		void eat_multi_comment( iterator &it, const iterator &end );

		void increment( iterator &it, file_position &pos );

		bool is_comment( const iterator &it, const iterator &end ) const;

		bool is_multi_comment( const iterator &it, const iterator &end ) const;

		bool is_alpha( const iterator &it, const iterator &end ) const;

		bool is_decimal( const iterator &it, const iterator &end ) const;
		
		bool is_hexa( const iterator &it, const iterator &end ) const;

		bool is_string( const iterator &it, const iterator &end ) const;

		cpp_token get_alpha( iterator &it, const iterator &end );

		cpp_token get_decimal( iterator &it, const iterator &end );

		cpp_token get_hexa( iterator &it, const iterator &end );

		cpp_token get_string( iterator &it, const iterator &end );

		cpp_token get_operator( iterator &it, const iterator &end );

	private:
		std::vector<cpp_token> tokens_;
		file_position fpc_;
	};

	class cpp_error : public std::exception, cpp_formatable
	{
	public:
		cpp_error( const char *reason, file_position pos );
		cpp_error( std::string reason, file_position pos );

		std::string pretty_format( ) const override;

	private:
		file_position pos_;
	};
	


}