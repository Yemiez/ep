/********************************************************
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*			    file_position.h					*	*
*  	*			   Created by Yamiez				*	*
*  	*	Do not forget to give credit when using		*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*********************************************************/
#pragma once
#include "defines.h"

namespace cpputils
{

	class file_position : public cpp_formatable
	{
	public:
		file_position( );
		file_position( int row, int column );

		std::string pretty_format( ) const override;

		int& get_row( );

		const int& get_row( ) const;

		int& get_column( );

		const int& get_column( ) const;

	private:
		int row_;
		int column_;
	};

}