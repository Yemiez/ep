#include "ep.h"

namespace ep
{
	value_reference_ptr void_return = std::make_shared<value_reference>( );
	method_ptr default_ctor = std::make_shared<method>( "ctor", method_argument_list{ }, []( variable_list, dispatch_engine_ptr )
	{
		return void_return;
	} );

	method_ptr default_dtor = std::make_shared<method>( "dtor", method_argument_list{ }, []( variable_list, dispatch_engine_ptr )
	{
		return void_return;
	} );
}