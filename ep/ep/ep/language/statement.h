/********************************************************
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*				  statement.h					*	*
*  	*			   Created by Yamiez				*	*
*  	*	Do not forget to give credit when using		*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*********************************************************/
#pragma once
#include "all_defines.h"


namespace ep
{

	class statement
	{
	public:
		statement( std::vector<tokenizer_it> ref, 
				   tokenizer_it &current, 
				   const tokenizer_it &end, 
				   dispatch_engine_ptr engine, 
				   statement *parent = nullptr, 
				   bool is_in_while = false );

		bool eval( dispatch_engine_ptr engine, dispatch_state_ptr state, debugger_ptr debugger );

		bool eval_or_eval_and_return( dispatch_engine_ptr engine, dispatch_state_ptr state, debugger_ptr debugger );

		value_reference_ptr get_return( );

		bool is_return( ) const;

		bool is_empty_expression( ) const;

		bool is_function_call( ) const;

		bool is_var_decl( ) const;

		bool is_var_decl_initalizer( ) const;

		bool is_constexpr( ) const;

		bool is_constant( ) const;

		bool is_while( ) const;

		bool is_if( ) const;

		bool is_else_if( ) const;

		bool is_else( ) const;

		bool is_block_type( ) const;

		bool is_var_modify( ) const;

		bool is_inside_of_while( ) const;

		bool is_this_assignment( ) const;

		bool lhsv( const std::string &x ) const;

		bool is_expression( ) const;

		bool has_else_statement( ) const;

		bool is_break_statement( ) const;
		
		bool is_continue_statement( ) const;

		bool has_always_break_statement( ) const;

		const std::string &get_var_decl( ) const;

		bool this_expr_is_mem_access_of( const std::string &x ) const;

		expression_ptr get_expression( ) const;

		void set_expression( expression_ptr expr );

		void make_infinite_loop( );

		void make_if_statement( );

		void make_var_decl_initalizer( );

		void recursive_set_return( value_reference_ptr &ptr );

		const statement_list &get_dups( ) const;

		statement_list &get_dups( );

		statement_ptr get_else_statement( );

		std::string pretty_format( ) const;

		void make_ref_assign( );

		void set_var_decl( );

	private:
		// Parsing stuff
		void parse( tokenizer_it &current, const tokenizer_it &end, dispatch_engine_ptr &engine );

		void parse_var( tokenizer_it &current, 
						const tokenizer_it &end, dispatch_engine_ptr &engine, 
						bool is_auto, 
						bool is_dyn );

		void parse_optional_identifier( tokenizer_it &current, const tokenizer_it &end, dispatch_engine_ptr &engine );

		void parse_while( tokenizer_it &current, const tokenizer_it &end, dispatch_engine_ptr &engine );

		void parse_if( tokenizer_it &current, const tokenizer_it &end, dispatch_engine_ptr &engine );

		void parse_else( tokenizer_it &current, const tokenizer_it &end, dispatch_engine_ptr &engine );

	private:
		enum flags
		{
			return_flag,
			function_call_flag,
			expression_var_decl_flag,
			var_decl_flag,
			expression_var_modify,
			empty_expression_flag,
			while_flag,
			if_flag,
			else_if_flag,
			else_flag,
			break_flag,
			continue_flag,
			evaluate_expression_flag,
			after_added_ref,
			infinite_loop,
		};
		std::vector<tokenizer_it> refs_;
		tokenizer_it begin_;
		statement *parent_;
		value_reference_ptr return_;
		expression_ptr expr_;
		statement_list dups_;
		statement_ptr else_;
		int flag_;
		bool while_, this_access_lhs_, is_dyn_;
		std::string lhsv_, lhsop_;
	};

}