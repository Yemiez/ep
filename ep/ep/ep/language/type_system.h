#pragma once
#include "all_defines.h"
#include <typeinfo>

namespace ep
{

	class type_info
	{
	public:
		type_info( std::string name,
				   std::function<void( value_reference* )> dtor = nullptr,
				   std::function<value_reference_ptr( value_reference * )> clone = nullptr,
				   method_list methods = { },
				   method_argument_list members = { } );

		const std::string &get_name( ) const;

		std::string &get_name( );

		const std::function<void(value_reference*)> &get_dtor( ) const;

		std::function<void( value_reference* )> &get_dtor( );

		const std::function<value_reference_ptr( value_reference* )> &get_clone( ) const;

		std::function<value_reference_ptr( value_reference* )> &get_clone( );

		const method_list &get_methods( ) const;

		method_list &get_methods( );

		method_ptr get_method( const std::string &id );

		bool has_method( const std::string &id );

		void add_method( method_ptr method );

		variable_list create_members( ) const;

		method_argument_list get_members( );

	public:

		bool bare_equal( const type_info &other ) const;

		bool operator==( const type_info &other ) const;
		
		bool operator!=( const type_info &other ) const;
		
	private:

		method_list::const_iterator find_method( const std::string &id ) const;

		method_list::iterator find_method( const std::string &id );

		method_list::const_iterator bad_method_it( ) const;

		method_list::iterator bad_method_it( );

	private:
		std::string name_;
		std::function<void( value_reference* )> dtor_;
		std::function<value_reference_ptr( value_reference *)> clone_;
		method_list methods_;
		method_argument_list members_;
	};

}