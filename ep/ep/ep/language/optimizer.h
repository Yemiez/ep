#pragma once
#include "all_defines.h"

namespace ep
{

	class optimizer
	{
	public:
		optimizer( );
		~optimizer( );

		bool optimize_all( statement_list &block );

		bool dead_code( statement_list &block );

		bool constant_fold( statement_list &block );

		bool unused_variables( statement_list &block );

		bool fix_blocks( statement_list &block );

	private:
		dispatch_engine_ptr cfolder_;
	};

}