/********************************************************
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*			 expression_action.cpp				*	*
*  	*			   Created by Yamiez				*	*
*  	*	Do not forget to give credit when using		*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*********************************************************/
#include "expression_action.h"
#include "value_reference.h"

ep::expression_action::expression_action( value_reference_ptr value )
	: ref_( std::move( value ) ), id_( ), method_( false )
{ }

ep::expression_action::expression_action( std::string name, bool is_method )
	: id_( std::move( name ) ), method_( is_method ), ref_( nullptr )
{ }

bool ep::expression_action::is_ref( ) const
{
	return ref_ != nullptr;
}

ep::value_reference_ptr ep::expression_action::get_ref( )
{
	return ref_;
}


const std::string & ep::expression_action::get_ref_name( ) const
{
	return id_;
}

std::string & ep::expression_action::get_ref_name( )
{
	return id_;
}

bool ep::expression_action::is_variable( ) const
{
	return !method_ && !id_.empty( ) && !ref_;
}

bool ep::expression_action::is_method( ) const
{
	return method_;
}
