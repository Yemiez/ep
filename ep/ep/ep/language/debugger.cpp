#include "debugger.h"
#include "../ep.h"
#include <iostream>

ep::debugger::debugger( std::ostream & output )
	: op_( output )
{ }

void ep::debugger::step_into( method_ptr & method, dispatch_state_ptr & state )
{
	op_ << "<debug_method>\n\t";

	op_ << std::endl << "</debug_method>\n\n";
}

void ep::debugger::step_into( expression_ptr & expression, dispatch_state_ptr & state )
{
	op_ << "<debug_expression>\n\t";

	op_ << std::endl << "</debug_expression>\n\n";
}

void ep::debugger::step_into( statement_ptr & statement, dispatch_state_ptr & state )
{ 
	op_ << "<debug_statement>\n\t";
	op_ << "stepping into statement: " << statement->pretty_format( );
	op_ << "current stack: \n" << state->pretty_format( );
	op_ << std::endl << "</debug_statement>\n\n";
	op_ << "press enter to step into statement." << std::endl;
	std::string tmp;
	std::getline( std::cin, tmp );
}
