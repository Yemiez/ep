#include "optimizer.h"
#include "statement.h"
#include "dispatch_engine.h"
#include "dispatch_state.h"
#include "expression.h"
#include "expression_action.h"
#include "variable.h"
#include "value_reference.h"
#include "libs.h"
#include <iostream>

ep::optimizer::optimizer( )
	: cfolder_( new dispatch_engine( ) )
{ 
	import_bo( cfolder_ );
}

ep::optimizer::~optimizer( )
{ }

bool ep::optimizer::optimize_all( statement_list & block )
{
	dead_code( block );
	constant_fold( block );
	return false;
	fix_blocks( block );
	unused_variables( block );
}

bool ep::optimizer::dead_code( statement_list & block )
{
	bool bDidSomething = false;
	for ( auto it = block.begin( ), end = block.end( );
		  it < end; )
	{
		if ( ( *it )->is_return( ) )
		{
			if ( ( it + 1 ) < end )
			{
				it = block.erase( ( it + 1 ), end );
				bDidSomething = true;
				end = block.end( );
			}
			else
				++it;
		}
		else if ( ( *it )->is_empty_expression( ) )
		{
			bDidSomething = true;
			it = block.erase( it );
			end = block.end( );
		}
		else if ( ( *it )->is_continue_statement( ) && !( ( it + 1 ) < end ) )
		{
			it = block.erase( it );
			end = block.end( );
		}
		else if ( ( *it )->is_block_type( ) )
		{
			auto &dups = ( *it )->get_dups( );
			if ( this->dead_code( dups ) )
				bDidSomething = true;
			++it;
		}
		else
			++it;
	}
	return bDidSomething;
} 

bool ep::optimizer::constant_fold( statement_list & block )
{
	bool bDidSomething = false;
	for ( auto it = block.begin( ), end = block.end( );
		  it < end;
		  ++it )
		if ( ( *it )->is_var_modify( ) && ( *it )->is_constexpr( ) && !( *it )->is_constant( ) )
		{
			if ( ( *it )->get_expression( )->get_actions( ).size( ) < 5 )
				continue;

			auto state = cfolder_->get_global_state( );
			state->reset( );
			auto expr = std::make_shared<expression>( *( *it )->get_expression( ) );
			expr->get_actions( ).erase( expr->get_actions( ).begin( ) ); // remove 'identifier' and '='
			expr->get_actions( ).erase( expr->get_actions( ).end( ) - 1 );
			if ( expr->get_actions_size( ) > 1 ) // if the expr is larger than 1 (e.g 1 + 1)
			{
				bDidSomething = true;
				// evaluate the expression with the constant evaluation engine
				// and emplace it to the temporary expression
				expr->get_actions( ).emplace_back( new expression_action( std::move( expr->evaluate_actions( cfolder_, state ) ) ) );
				// erase the other ones keeping only the resulting expression
				expr->get_actions( ).erase( expr->get_actions( ).begin( ), expr->get_actions( ).end( ) - 1 );
				// replace the original expression's actions
				auto orig_expr = ( *it )->get_expression( );
				orig_expr->get_actions( ).erase( orig_expr->get_actions( ).begin( ) + 1, orig_expr->get_actions( ).end( ) - 1 );
				orig_expr->get_actions( ).insert( orig_expr->get_actions( ).begin( ) + 1, expr->get_actions( ).at( 0 ) );
				// re-format the debug string.
				orig_expr->set_string_format( orig_expr->get_actions( ).at( 0 )->get_ref_name( ) +
											  " = " +
											  std::to_string( *expr->get_actions( ).at( 0 )->get_ref( )->get_pointer<int>( ) ) +
											  ";" );
			}
		}
		else if ( ( *it )->is_block_type( ) )
		{
			auto dups = ( *it )->get_dups( );
			if ( this->constant_fold( dups ) )
				bDidSomething = true;
		}
	return bDidSomething;
}

//
bool ep::optimizer::unused_variables( statement_list & block )
{
	struct var_reference
	{
		statement_ptr p_var_decl;
		statement_list &p_var_list_location;
		std::vector<std::pair<statement_ptr, statement_list&>> p_weaks,
			p_refs;
		int is_referenced_, is_weak_refeferenced_;
		bool return_;
	};
	std::vector<var_reference> var_decls;

	auto add_ref = [&var_decls]( auto &p_decl,
								 auto &location )
	{
		var_decls.emplace_back( var_reference{ p_decl, location, {}, {}, 0, 0, false } );
	};

	auto exists = [&var_decls]( statement_ptr &p_accessor )->var_reference*
	{
		if ( p_accessor->is_function_call( ) || 
			 p_accessor->is_if( ) || 
			 p_accessor->is_else_if( ) || 
			 p_accessor->is_while( ) ||
			 p_accessor->is_expression( ) ||
			 p_accessor->is_return( ) )
		{
			for ( auto &x : p_accessor->get_expression( )->get_actions( ) )
			{
				for ( auto &y : var_decls )
					if ( y.p_var_decl->get_var_decl( ) == x->get_ref_name( ) )
						return &y;
			}
		}
		else if ( p_accessor->is_var_modify( ) )
		{
			for ( auto &x : var_decls )
				if ( x.p_var_decl->get_var_decl( ) == p_accessor->get_var_decl( ) )
					return &x;
		}
		return nullptr;
	};

	auto find_it = []( auto &elem, auto &vec )
	{
		for ( auto it = vec.begin( ), end = vec.end( );
			  it < end;
			  ++it )
			if ( *it == elem )
				return it;
		return vec.end( );
	};

	auto is_before_ref = [&find_it]( auto &var_decl, auto &it )
	{
		auto xIt = var_decl.p_var_list_location.end( );
		for ( auto &x : var_decl.p_refs )
		{
			if ( x.second == var_decl.p_var_list_location )
			{
				xIt = find_it( x.first, x.second );
				break;
			}
		}
		return find_it( it->first, it->second ) < xIt;
	};

	auto find_good_weak = [&is_before_ref]( auto &var_decl )
	{
		auto last = var_decl.p_weaks.end( );
		for ( auto it = var_decl.p_weaks.begin( ), end = var_decl.p_weaks.end( );
			  it < end;
			  ++it )
			if ( it->second == var_decl.p_var_list_location && is_before_ref( var_decl, it ) )
				last = it;
		return last;
	};
	
	std::function<void( statement_list & )> do_block = 
		[&do_block, &var_decls, &exists, &add_ref]( statement_list &block )
	{
		for ( auto it = block.begin( ), end = block.end( );
			  it < end;
			  ++it )
		{
			auto &x = *it;
			if ( x->is_var_decl( ) || x->is_var_decl_initalizer( ) )
				add_ref( x, block );
			else if ( x->is_function_call( ) || 
					  x->is_var_modify( ) || 
					  x->is_expression( ) ||
					  x->is_return( ) )
			{
				auto ex = exists( x );
				if ( !ex )
					continue;
				if ( x->is_expression( ) && !x->is_var_modify( ) )
				{
					ex->p_refs.emplace_back( x, block );
					++ex->is_referenced_;
				}
				else if ( x->is_var_modify( ) )
				{
					++ex->is_weak_refeferenced_;
					ex->p_weaks.emplace_back( x, block );
				}
				else
				{
					if ( x->is_return( ) )
						ex->return_ = true;
					ex->p_refs.emplace_back( x, block );
					++ex->is_referenced_;
				}
			}
			else if ( x->is_block_type( ) )
			{
				if ( x->is_if( ) || x->is_else_if( ) || x->is_while( ) )
				{
					auto ex = exists( x );
					if ( ex )
					{
						ex->p_refs.emplace_back( x, block );
						++ex->is_referenced_;
					}
				}
				do_block( x->get_dups( ) );
			}
		}
	};
	do_block( block );
	
	for ( auto &x : var_decls )
	{
		if ( !x.is_referenced_ ) // doesn't have any strong references (run-time possible modifications)
		{
			auto it = find_it( x.p_var_decl, x.p_var_list_location );
			x.p_var_list_location.erase( it );

			// remove weaks
			for ( auto &weak : x.p_weaks )
			{
				auto it = find_it( weak.first, weak.second );
				if ( it != weak.second.end( ) )
					weak.second.erase( it );
			}
		}
		else
		{
			if ( x.is_weak_refeferenced_ )
			{
				auto last_weak = find_good_weak( x ); // Find the latest used weak reference
				if ( last_weak == x.p_weaks.end( ) ) 
					continue;
				auto new_expr = last_weak->first->get_expression( ); 
				new_expr->get_actions( ).erase( new_expr->get_actions( ).begin( ) ); // remove 'identifier' and '=' from [identifier expression... '=']
				new_expr->get_actions( ).erase( new_expr->get_actions( ).end( ) - 1 );
				auto fmt = new_expr->pretty_format( );
				fmt.erase( 0, fmt.find( '=' ) + 1 );
				while ( fmt.begin( ) < fmt.end( ) && *fmt.begin( ) == ' ' )
					fmt.erase( fmt.begin( ) );
				new_expr->set_string_format( fmt );
				x.p_var_decl->set_expression( new_expr );
				x.p_var_decl->make_var_decl_initalizer( ); // Incase the var decl was something like 'var x;', make sure we change it just incase.
				x.p_var_list_location.erase( find_it( x.p_weaks.begin( )->first, x.p_var_list_location ),
											 find_it( last_weak->first, last_weak->second ) + 1 ); // erase the weaks inbetween
			}
		}
	}

	return true;
}

bool ep::optimizer::fix_blocks( statement_list & block )
{
	auto append = []( auto &block, auto it, auto &dups )->auto
	{
		for ( auto dit = dups.begin( ), dend = dups.end( );
			  dit < dend;
			  ++dit )
			it = block.insert( it, *dit );
		return it;
	};

	auto append_while = []( auto &block, auto it, auto &dups )->auto
	{
		for ( auto dit = dups.begin( ), dend = dups.end( );
			  dit < dend;
			  ++dit )
		{
			if ( ( *dit )->is_break_statement( ) || ( *dit )->is_continue_statement( ) )
				break;
			it = block.insert( it, *dit );
		}
		return it;
	};
	
	std::function<void( statement_list& )> do_block = 
		[&do_block, &append, &append_while, this]( statement_list &block )
	{
		for ( auto it = block.begin( ), end = block.end( );
			  it < end; )
		{
			if ( ( *it )->is_block_type( ) )
			{
				do_block( ( *it )->get_dups( ) );
				if ( ( *it )->is_while( ) )
				{
					if ( ( *it )->get_expression( )->is_constexpr( ) )
					{
						auto value = ( *it )->get_expression( )->evaluate_actions( cfolder_,
																				   cfolder_->get_global_state( ),
																				   nullptr );
						if ( *value->get_pointer<int>( ) )
						{
							if ( ( *it )->has_always_break_statement( ) )
							{
								auto dups = ( *it )->get_dups( );
								it = block.erase( it );
								it = append_while( block, it, dups );
								end = block.end( );
							}
							else
								( *it )->make_infinite_loop( );
						}
						else
						{
							it = block.erase( it );
							end = block.end( );
						}

					}
				}
				else if ( ( *it )->is_if( ) ) // REMOVE THIS IF VALUE IS CONSTEXPR, AND HAS AN ELSE
				{
					if ( ( *it )->get_expression( )->is_constexpr( ) )
					{
						auto value = ( *it )->get_expression( )->evaluate_actions( cfolder_,
																				   cfolder_->get_global_state( ),
																				   nullptr );
						if ( *value->get_pointer<int>( ) )
						{
							// remove this blockerino
							auto dups = ( *it )->get_dups( );
							it = block.erase( it );
							it = append( block, it, dups );
							end = block.end( );
						}
						else if ( ( *it )->has_else_statement( ) && ( *it )->get_else_statement( )->is_else( ) )
						{
							auto e_stmnt = ( *it )->get_else_statement( );
							auto dups = e_stmnt->get_dups( );
							it = block.erase( it );
							it = append( block, it, dups );
							end = block.end( );
						}
						else if ( ( *it )->has_else_statement( ) ) // replace if( 0 ) [...] else if ( 1 ) [...] with body of 2nd else [...]
						{
							auto e_stmnt = ( *it )->get_else_statement( );
							e_stmnt->make_if_statement( );
							it = block.insert( block.erase( it ), e_stmnt );
							end = block.end( );
							if ( it != block.begin( ) )
								--it;
						}
						else
						{
							it = block.erase( it );
							end = block.end( );
						}
					}
					else
						++it;
				}
			}
			else
				++it;
		}
	};
	do_block( block );
	return true;
}
