/********************************************************
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*				  expression.h					*	*
*  	*			   Created by Yamiez				*	*
*  	*	Do not forget to give credit when using		*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*********************************************************/
#pragma once
#include "all_defines.h"
#include "../../cpputils/tokenizer.h"

namespace ep
{

	class expression
	{
	public:
		expression( std::vector<tokenizer_it> refs,
			        tokenizer_it &it,
					const tokenizer_it &end,
					dispatch_engine_ptr engine, 
					bool skip_semi = false );

		expression( const expression &expr );

		expression( expression &&expr );

		const expression_action_list &get_actions( ) const;

		expression_action_list &get_actions( );

		size_t get_actions_size( ) const;

		value_reference_ptr evaluate_actions( dispatch_engine_ptr engine, 
											  dispatch_state_ptr state, 
											  bool is_ret = false,
											  bool as_ref = false );

		bool is_constexpr( ) const;

		bool has_access( ) const;

		bool has_fcalls( ) const;

		void set_string_format( std::string fmt );

		std::string pretty_format( ) const;

		cpputils::file_position get_position( ) const;

	private:
		void create_expression_list( std::vector<tokenizer_it> &refs,
			                         tokenizer_it &it, 
									 const tokenizer_it &end,
									 dispatch_engine_ptr engine, 
									 bool skip );

		void insert_to_output( std::vector<tokenizer_it> &refs,
							   tokenizer_it &it,
							   const tokenizer_it &end,
							   expression_action_list &output,
							   dispatch_engine_ptr &engine );

		expression_action_ptr parse_anonymous_method( std::vector<tokenizer_it> &refs,
													  tokenizer_it &it,
													  const tokenizer_it &end,
													  dispatch_engine_ptr engine );

		variable_list arg_compile( method_argument_list &args,
								   std::stack<expression_action_ptr> &stack,
								   dispatch_state_ptr &state );

	private:
		expression_action_list list_;
		std::ostringstream os_;
		cpputils::file_position where_;
	};

}