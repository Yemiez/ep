/********************************************************
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*				  eval_error.h					*	*
*  	*			   Created by Yamiez				*	*
*  	*	Do not forget to give credit when using		*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*********************************************************/
#pragma once
#include "all_defines.h"

namespace ep
{

	class ep_error : public std::exception
	{
	public:
		ep_error( );
		ep_error( const std::string &err );

		virtual std::string pretty_format( ) const;
	};

	class empty_code_error : public ep_error
	{
	public:
		empty_code_error( );

		std::string pretty_format( ) const;
	};

	class library_identifier_collide_error : public ep_error
	{
	public:
		library_identifier_collide_error( std::string name );

		std::string pretty_format( ) const;
	private:
		std::string name_;
	};

	class group_constructor_provides_no_initalizer_error : public ep_error
	{
	public:
		group_constructor_provides_no_initalizer_error( method_ptr ctor,
			                                      std::string members,
												  tokenizer_it where );

		std::string pretty_format( ) const override;
	private:
		std::string format_;
	};

	class expected_identifier_error : public ep_error
	{
	public:
		expected_identifier_error( std::vector<tokenizer_it> refs,
								   tokenizer_it where,
								   tokenizer_it end );

		std::string pretty_format( ) const override;
	private:
		std::string format_;
	};

	class expected_opening_bracket_error : public ep_error
	{
	public:
		expected_opening_bracket_error( std::vector<tokenizer_it> refs,
										tokenizer_it where,
										tokenizer_it end );

		std::string pretty_format( ) const override;
	private:
		std::string format_;
	};

	class expected_closing_bracket_error : public ep_error
	{
	public:
		expected_closing_bracket_error( std::vector<tokenizer_it> refs,
										tokenizer_it where,
										tokenizer_it end );

		std::string pretty_format( ) const override;
	private:
		std::string format_;
	};

	class expected_opening_parenthesis_error : public ep_error
	{
	public:
		expected_opening_parenthesis_error( std::vector<tokenizer_it> refs,
										tokenizer_it where,
										tokenizer_it end );

		std::string pretty_format( ) const override;
	private:
		std::string format_;
	};

	class expected_closing_parenthesis_error : public ep_error
	{
	public:
		expected_closing_parenthesis_error( std::vector<tokenizer_it> refs,
										tokenizer_it where,
										tokenizer_it end );

		std::string pretty_format( ) const override;
	private:
		std::string format_;
	};

	class expected_semicolon_error : public ep_error
	{
	public:
		expected_semicolon_error( std::vector<tokenizer_it> refs,
								  tokenizer_it where,
								  tokenizer_it end );

		std::string pretty_format( ) const override;
	private:
		std::string format_;
	};

	class expected_class_specifiers_error : public ep_error
	{
	public:
		expected_class_specifiers_error( std::vector<tokenizer_it> refs,
										 tokenizer_it where,
										 tokenizer_it end );

		std::string pretty_format( ) const override;
	private:
		std::string format_;
	};

	class this_not_allowed_as_identifier_error : public ep_error
	{
	public:
		this_not_allowed_as_identifier_error( std::vector<tokenizer_it> refs,
											  tokenizer_it where,
											  tokenizer_it end );

		std::string pretty_format( ) const override;
	private:
		std::string format_;
	};

	class ran_out_of_tokens_error : public ep_error
	{
	public:
		ran_out_of_tokens_error( std::vector<tokenizer_it> refs,
							     tokenizer_it where,
							     tokenizer_it end );

		std::string pretty_format( ) const override;
	private:
		std::string format_;
	};

	class library_is_undefined_error : public ep_error
	{
	public:
		library_is_undefined_error( tokenizer_it where );

		std::string pretty_format( ) const override;
	private:
		tokenizer_it where_;
	};

	class expected_import_identifier_error : public ep_error
	{
	public:
		expected_import_identifier_error( tokenizer_it where );

		std::string pretty_format( ) const override;
	private:
		tokenizer_it where_;
	};

	class unmatched_single_quote_error : public ep_error
	{
	public:
		unmatched_single_quote_error( std::vector<tokenizer_it> refs,
								      tokenizer_it where,
								      tokenizer_it end );

		std::string pretty_format( ) const override;
	private:
		std::string format_;
	};

	class misusage_break_error : public ep_error
	{
	public:
		misusage_break_error( std::vector<tokenizer_it> refs,
							  tokenizer_it where,
							  tokenizer_it end );

		std::string pretty_format( ) const override;
	private:
		std::string format_;
	};

	class misusage_continue_error : public ep_error
	{
	public:
		misusage_continue_error( std::vector<tokenizer_it> refs,
							     tokenizer_it where,
							     tokenizer_it end );

		std::string pretty_format( ) const override;
	private:
		std::string format_;
	};

	class unknown_token_error : public ep_error
	{
	public:
		unknown_token_error( std::vector<tokenizer_it> refs,
							 tokenizer_it where,
							 tokenizer_it end );

		std::string pretty_format( ) const override;
	private:
		std::string format_;
	};

	class cannot_deduce_auto_type_error : public ep_error
	{
	public:
		cannot_deduce_auto_type_error( std::vector<tokenizer_it> refs,
									   tokenizer_it where,
									   tokenizer_it end );

		std::string pretty_format( ) const override;
	private:
		std::string format_;
	};

	class expected_expression_error : public ep_error
	{
	public:
		expected_expression_error( std::vector<tokenizer_it> refs,
								   tokenizer_it where,
								   tokenizer_it end );

		std::string pretty_format( ) const override;
	private:
		std::string format_;
	};

	class comma_was_misplaced_error : public ep_error
	{
	public:
		comma_was_misplaced_error( std::vector<tokenizer_it> refs,
								   tokenizer_it where,
								   tokenizer_it end );

		std::string pretty_format( ) const override;
	private:
		std::string format_;
	};

	class expected_opening_specialize_operator_error : public ep_error
	{
	public:
		expected_opening_specialize_operator_error( std::vector<tokenizer_it> refs,
													tokenizer_it where,
													tokenizer_it end );

		std::string pretty_format( ) const override;
	private:
		std::string format_;
	};

	class expected_closing_specialize_operator_error : public ep_error
	{
	public:
		expected_closing_specialize_operator_error( std::vector<tokenizer_it> refs,
													tokenizer_it where,
													tokenizer_it end );

		std::string pretty_format( ) const override;
	private:
		std::string format_;
	};

	class expected_method_after_specialize_error : public ep_error
	{
	public:
		expected_method_after_specialize_error( std::vector<tokenizer_it> refs,
													tokenizer_it where,
													tokenizer_it end );

		std::string pretty_format( ) const override;
	private:
		std::string format_;
	};

	class dyanamic_object_no_initialization : public ep_error
	{
	public:
		dyanamic_object_no_initialization( std::vector<tokenizer_it> refs,
										   tokenizer_it where,
										   tokenizer_it end );

		std::string pretty_format( ) const override;
	private:
		std::string format_;
	};


	// runtime errors

	class conversion_error : public ep_error
	{
	public:
		conversion_error( type_info_ptr method_type,
						  method_ptr scope,
						  type_info_ptr from,
						  std::vector<type_info_ptr> to );

		std::string pretty_format( ) const override;

	private:
		type_info_ptr method_ti_;
		method_ptr method_;
		type_info_ptr from_;
		std::vector<type_info_ptr> to_;
	};

	class arity_error : public ep_error
	{
	public:
		arity_error( type_info_ptr method_type,
					 method_ptr scope,
					 expression* where );

		std::string pretty_format( ) const override;

	private:
		expression* where_;
		type_info_ptr method_ti_;
		method_ptr method_;
	};

	class vector_for_each_error : public ep_error
	{
	public:
		vector_for_each_error( method_ptr method );

		std::string pretty_format( ) const override;
	private:
		method_ptr method_;
	};

	class undefined_identifier_error : public ep_error
	{
	public:
		undefined_identifier_error( std::string id );

		std::string pretty_format( ) const override;
	private:
		std::string id_;
	};

	class symbol_collision_error : public ep_error
	{
	public:
		symbol_collision_error( type_info_ptr type,
								method_ptr method,
							    std::string id );
		symbol_collision_error( type_info *type,
								method_ptr method,
								std::string id );


		std::string pretty_format( ) const override;
	private:
		method_ptr method_;
		type_info* ti_;
		std::string id_;
	};

	// different meaning
	class type_redefiniton_error : public ep_error
	{
	public:
		type_redefiniton_error( type_info_ptr existing,
								type_info_ptr attempted );

		
		std::string pretty_format( ) const override;

	private:
		type_info_ptr existing_, attempted_;
	};
	
	class undefined_type_error : public ep_error
	{
	public:
		undefined_type_error( std::string id );

		std::string pretty_format( ) const override;
	private:
		std::string id_;
	};

	class member_is_inaccessible : public ep_error
	{
	public:
		member_is_inaccessible( value_reference_ptr where,
								variable_ptr member,
								expression *expr );

		std::string pretty_format( ) const override;
	private:
		std::string format_;
	};

	class invalid_access_usage_error : public ep_error
	{
	public:
		invalid_access_usage_error( expression *expr );

		std::string pretty_format( ) const override;
	private:
		expression *expr_;
	};

	class lambda_creation_failed_error : public ep_error
	{
	public:
		lambda_creation_failed_error( method *method );

		std::string pretty_format( ) const override;
	private:
		method *method_;
	};

	class bad_value_cast_error : public ep_error
	{
	public:
		bad_value_cast_error( const std::type_info *t1,
							  const std::type_info *t2 );

		std::string pretty_format( ) const override;
	private:
		const std::type_info *t1_, *t2_;
	};

	class bad_vref_steal_error : public ep_error
	{
	public:
		bad_vref_steal_error( std::string id );

		std::string pretty_format( ) const override;
	private:
		std::string id_;
	};

	class ep_error_compilation : public ep_error
	{
	public:
		ep_error_compilation( std::vector<std::string> errors );

		std::string pretty_format( ) const override;
	private:
		std::vector<std::string> errors_;
	};

	class invalid_specialization_expression_error : public ep_error
	{
	public:
		invalid_specialization_expression_error( ep::method *real_method,
												 ep::method_ptr spec_method );

		std::string pretty_format( ) const override;
	private:
		std::string format_;
	};


	class break_exception : public std::exception
	{
	public:
		break_exception( )
			: std::exception( "break" )
		{ }
	};

	class continue_exception : public std::exception
	{
	public:
		continue_exception( )
			: std::exception( "continue" )
		{ }
	};

	class exit_exception : public std::exception
	{
	public:
		exit_exception( )
			: std::exception( "exit" )
		{ }
	};
}