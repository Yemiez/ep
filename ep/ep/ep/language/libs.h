/********************************************************
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*				     libs.h						*	*
*  	*			   Created by Yamiez				*	*
*  	*	Do not forget to give credit when using		*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*********************************************************/
#pragma once
#include "all_defines.h"


namespace ep
{
	extern void import_bo( dispatch_engine_ptr ); /* basic operations */
	extern void import_ios( dispatch_engine_ptr ); /* input/output */
	extern void import_math( dispatch_engine_ptr ); /* Advanced math library */
	extern void import_vector( dispatch_engine_ptr );
	extern bool is_std_bo( const std::string &name );
}