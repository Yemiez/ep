/********************************************************
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*			   dispatch_engine.h				*	*
*  	*			   Created by Yamiez				*	*
*  	*	Do not forget to give credit when using		*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*********************************************************/
#pragma once
#include "all_defines.h"

namespace ep
{

	class dispatch_engine
	{
	public:
		dispatch_engine( );
		dispatch_state_ptr create_new_dispatch_state( std::string name, 
													  const method_argument_list &args, 
													  variable_list actual_args, 
													  dispatch_state_ptr parent );
		dispatch_state_ptr create_new_dispatch_state( std::string name, 
													  const method_argument_list &args, 
													  variable_list actual_args );
		dispatch_state_ptr create_new_dispatch_state( std::string name, 
													  dispatch_state_ptr parent );

		method_list &get_methods( );

		method_ptr get_method( const std::string &id );

		void add_method( method_ptr method );

		variable_list &get_variables( );

		variable_ptr get_variable( const std::string &id );

		void add_variable( variable_ptr global );

		dispatch_state_ptr get_global_state( );

		debugger_ptr &get_debugger( );

		void add_type( type_info_ptr ti );

		type_info_ptr get_type_info( const std::string &it );

		type_info_list &get_type_infos( );

	private:
		method_list::iterator find_method( const std::string &id );

		method_list::iterator bad_method_it( );

		type_info_list::iterator find_ti( const std::string &id );

		type_info_list::iterator bad_ti_it( );

	private:
		method_list methods_;
		dispatch_state_ptr state_;
		debugger_ptr dbg_;
		type_info_list tis_;
		
	};

}