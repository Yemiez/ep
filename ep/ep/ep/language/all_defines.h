/********************************************************
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*			     all_defines.h					*	*
*  	*			   Created by Yamiez				*	*
*  	*	Do not forget to give credit when using		*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*********************************************************/
#pragma once
#include <memory>
#include <vector>
#include <sstream>
#include <map>
#include <functional>
#include <exception>
#include <stack>
#include "../../cpputils/tokenizer.h"

namespace ep
{
	class type_info;
	class variable;
	class statement;
	class expression;
	class expression_action;
	class value_reference;
	class method;
	class method_argument;
	class dispatch_engine;
	class dispatch_state;
	class module;
	class optimizer;
	class debugger;
	class ep_error;
	class conversion_error;
	class plugin;
	using plugin_ptr = std::shared_ptr<plugin>;
	using type_info_ptr = std::shared_ptr<type_info>;
	using variable_ptr = std::shared_ptr<variable>;
	using statement_ptr = std::shared_ptr<statement>;
	using expression_ptr = std::shared_ptr<expression>;
	using expression_action_ptr = std::shared_ptr<expression_action>;
	using value_reference_ptr = std::shared_ptr<value_reference>;
	using method_ptr = std::shared_ptr<method>;
	using method_argument_ptr = std::shared_ptr<method_argument>;
	using dispatch_engine_ptr = std::shared_ptr<dispatch_engine>;
	using dispatch_state_ptr = std::shared_ptr<dispatch_state>;
	using module_ptr = std::shared_ptr<module>;
	using optimizer_ptr = std::shared_ptr<optimizer>;
	using debugger_ptr = std::shared_ptr<debugger>;
	using type_info_list = std::vector<type_info_ptr>;
	using variable_list = std::vector<variable_ptr>;
	using expression_list = std::vector<expression_ptr>;
	using expression_action_list = std::vector<expression_action_ptr>;
	using method_list = std::vector<method_ptr>;
	using method_argument_list = std::vector<method_argument_ptr>;
	using statement_list = std::vector<statement_ptr>;
	using plugin_list = std::vector<plugin_ptr>;
	using tokenizer_it = std::vector<cpputils::cpp_token>::iterator;
	extern value_reference_ptr void_return;
	extern method_ptr default_ctor, default_dtor;
}
