/********************************************************
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*				  module.cpp					*	*
*  	*			   Created by Yamiez				*	*
*  	*	Do not forget to give credit when using		*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*********************************************************/
#include "../ep.h"
#include "debugger.h"
#include "libs.h"
#include <fstream>
#include "optimizer.h"
#include "../plugin/plugin.h"
#include "../plugin/manual_map.h"
#include "../plugin/compiled_plugin_loader.h"
#include <algorithm>
#include <experimental\filesystem>

ep::module::module( )
	: engine_( new dispatch_engine( ) ), import_funs_( { { "bo", &ep::import_bo }, 
	                                                     { "ios", &ep::import_ios },
                                                      	 { "math", &ep::import_math },
														 { "vector", &ep::import_vector } } ),
	 optimizer_( new optimizer() )
{
	import_funs_.emplace_back( "type_traits", [this, engine_ = engine_]( dispatch_engine_ptr )mutable
	{
		this->engine_->add_method( ep::make_method( "t_has_method",
													[engine_]( ep::argument_ptr_ref arg, const std::string &id )
		{
			try
			{
				arg.variable->get_ref( )->get_method( id );
				return std::make_shared<ep::value_reference>( new int( 1 ),
															  engine_->get_type_info( "integral" ) );
			}
			catch( ep::undefined_identifier_error & )
			{
				return std::make_shared<ep::value_reference>( new int( 0 ),
															  engine_->get_type_info( "integral" ) );
			}
		} ) );

		this->engine_->add_method( ep::make_method( "is_callable",
													[engine_]( ep::argument_ptr_ref arg )
		{
			auto &var = arg.variable;
			auto ref = var->get_ref( );
			if ( ref->is_anonymous_method( ) )
				return std::make_shared<ep::value_reference>( new int( 1 ),
															  engine_->get_type_info( "integral" ) );
			else if ( ref->type_info( ) && ref->type_info( )->has_method( "()" ) )
				return std::make_shared<ep::value_reference>( new int( 1 ),
															  engine_->get_type_info( "integral" ) );
			return std::make_shared<ep::value_reference>( new int( 0 ),
														  engine_->get_type_info( "integral" ) );
		} ) );

		this->eval( R"(
import bo;

method is_integral( &type )
{
	return 0;
};

specialize<typeid(type) == decltype( "integral" );>
method is_integral( &type )
{
	return 1;
};

method is_floating( &type )
{
	return 0;
};

specialize<typeid(type) == decltype( "floating" );>
method is_integral( &type )
{
	return 1;
};

method is_string_literal( &type )
{
	return 0;
};	

specialize<typeid(type) == decltype( "string_literal" );>
method is_string_literal( &type )
{
	return 1;
};

method is_arithmetic( &type )
{
	return 0;
};

method has_arithmetic_operators( &type )
{
	return 0;
};

specialize<t_has_method( type, "+" ) && t_has_method( type, "-" ) &&
		   t_has_method( type, "*" ) && t_has_method( type, "/" ) &&
		   t_has_method( type, "==" ) && t_has_method( type, "!=" ) &&
		   t_has_method( type, ">" ) && t_has_method( type, "<" ) &&
		   t_has_method( type, ">=" ) && t_has_method( type, "<=" );>
method has_arithmetic_operators( &type )
{
	return 1;
};

specialize<is_integral( type ) || is_floating( type ) || has_arithmetic_operators( type );>
method is_arithmetic( &type )
{
	return 1;
};

method t_has_operator( &t, &op )
{
	return t_has_method( t, op ); 
};

)" );

	} );
}

ep::module::module( plugin_list plugins )
	: module( )
{ 
	for ( auto &x : plugins )
		import_funs_.emplace_back( x->get_name_fn( )( ), x->get_load_fn( ) );
}

ep::module::~module( )
{ 
	for ( auto &x : libs_ )
		std::experimental::filesystem::resize_file( x, 0 );
}

ep::statement_list ep::module::compile( std::string contents )
{
	namespace cu = cpputils;
	if ( contents.empty( ) )
		throw empty_code_error( );
	statement_list block;
	if ( contents.capacity( ) > 4 )
	{
		auto str = contents.c_str( );
		if ( *str == 'o' &&
			 *( str + 1 ) == 'e' &&
			 *( str + 2 ) == 'p' &&
			 *( str + 3 ) == '\n' )
			block = deobfuscate_contents( contents );
	}
	auto tokens = std::make_shared<cu::cpp_tokenizer>( );
	tokens->tokenize( contents );
	for ( auto &x : parse( tokens->begin( ), tokens->end( ) ) )
		block.emplace_back( std::move( x ) );
	optimizer_->optimize_all( block );
	return block;
}

ep::statement_list ep::module::compile_file( const std::string & file )
{
	return compile( { std::istreambuf_iterator<char>( std::ifstream( file, std::ios::binary ) ), std::istreambuf_iterator<char>( ) } );
}

void ep::module::eval( statement_list & statements )
{ 
	internal_eval( statements );
}

void ep::module::eval( std::string contents )
{ 
	auto block = compile( contents );
	internal_eval( block );
}

void ep::module::eval_file( std::string file )
{ 
	return eval( { std::istreambuf_iterator<char>( std::ifstream( file, std::ios::binary ) ), std::istreambuf_iterator<char>( ) } );
}

ep::dispatch_engine_ptr ep::module::get_engine( )
{
	return engine_;
}

void ep::module::add_method( method_ptr method )
{ 
	engine_->add_method( std::move( method ) );
}

ep::method_list &ep::module::get_methods( )
{
	return engine_->get_methods( );
}

ep::method_ptr ep::module::get_method( const std::string & id )
{
	return engine_->get_method( id );
}

void ep::module::add_global( variable_ptr ptr )
{
	engine_->add_variable( std::move( ptr ) );
}

ep::variable_list ep::module::get_globals( )
{
	return engine_->get_variables( );
}

ep::variable_ptr ep::module::get_global( const std::string & id )
{
	return engine_->get_variable( id );
}

void ep::module::add_lib_fun( std::string name, std::function<void( dispatch_engine_ptr )> fun )
{ 
	if ( find_import_fun( name ) != bad_import_fun( ) )
		throw library_identifier_collide_error( std::move( name ) );
	import_funs_.emplace_back( std::move( name ), std::move( fun ) );
}

bool ep::module::is_included( const std::string & file ) const
{
	for ( auto &x : includes_ )
		if ( x == file )
			return true;
	return false;
}

struct _Header
{
	char intro[4];
	unsigned char pad[10];
	unsigned char channels[3];
	unsigned int size;
};

#include <iostream>
ep::statement_list ep::module::deobfuscate_contents( std::string & contents )
{ 
	if ( obfuscation_algorithm( contents ) )
	{
		auto header = *reinterpret_cast< const _Header* >( contents.data( ) );
		auto copy_contents = contents;
		copy_contents.erase( 0, header.size );
		contents.erase( header.size, contents.size( ) );
		contents.erase( 0, sizeof( _Header ) );

		auto copy = copy_contents;
		std::vector<std::string> libs;
		while ( obfuscation_algorithm( copy ) )
		{
			header = *reinterpret_cast< const _Header* >( copy.data( ) );
			copy_contents = copy.substr( 0, header.size );
			copy_contents.erase( 0, sizeof( _Header ) );
			copy.erase( 0, header.size );
			libs.emplace_back( std::move( copy_contents ) );
		}
		copy.erase( 0, sizeof( _Header ) );
		libs.emplace_back( std::move( copy ) );

		statement_list statements;
		ep::compiled_plugin_loader loader( std::move( libs ) );
		for ( auto &x : loader.get_plugins( ) )
		{
			auto load = x->get_load_fn( );
			auto name = x->get_name_fn( );
			auto preq = x->get_prerequisites_fn( );
			if ( !load || !name )
				throw std::exception( "unable to load *.oep file because; one of it's modules are invalid." );
			std::string sname( name( ) );
			if ( is_included( sname ) )
				continue;
			includes_.emplace_back( sname );
			if ( preq )
			{
				cpputils::cpp_tokenizer tokenizer;
				tokenizer.tokenize( preq( ) );
				if ( tokenizer.begin( )->get_block( ) == "import" )
					parse_import( tokenizer.begin( ) + 1, tokenizer.end( ), statements );
			}
			load( engine_ );
			libs_.emplace_back( x->get_path( ) );
		}
		return statements;
	}
	else
		contents.erase( 0, sizeof( _Header ) );
	return{ };
}

bool ep::module::obfuscation_algorithm( std::string & contents )
{ 
	if ( contents.empty( ) )
		return false;
	auto do_truple = []( _Header *ctx,
						 char *point,
						 unsigned char *channel,
						 unsigned int truples )
	{
		auto tentuples = truples / 10;
		for ( auto i = 0;
			  i < 10;
			  ++i )
		{
			for ( auto x = point;
				  x < point + tentuples;
				  ++x )
				*x ^= ( ( static_cast< int >( ctx->pad[i] ) + static_cast< int >( *channel ) <= 255 ) ?
						ctx->pad[i] + *channel :
						( static_cast< int >( ctx->pad[i] ) - static_cast< int >( *channel ) >= 0 ) ?
						ctx->pad[i] - *channel :
						throw std::exception( "obfuscation error (unable to find proper context and channel alignment)" ) );
			point += tentuples;
		}
	};
	if ( contents.size( ) < sizeof( _Header ) )
		throw std::exception( "unable to deobfuscate *.oep format, invalid *.oep file" );
	auto header = reinterpret_cast< _Header* >( const_cast<char*>( contents.data( ) ) );
	for ( auto c = &header->pad[0];
		  c < &header->pad[9];
		  ++c )
		*c ^= header->channels[0] + header->channels[1] + header->channels[2];

	auto obfuscated = reinterpret_cast<char*>( reinterpret_cast<char*>( header ) + sizeof( _Header ) );
	auto truples = ( header->size - sizeof( _Header ) ) / 3;
	for ( auto i = 0u;
		  i < 3;
		  ++i )
	{
		do_truple( header,
				   obfuscated,
				   &header->channels[i],
				   truples );
		obfuscated += truples;
	}
	return header->size != contents.size( );
}

void ep::module::fix_block( method_argument_list & members, statement_list & list ) const
{ 
	for ( auto &x : list )
	{
		if ( x->is_block_type( ) )
			fix_block( members, x->get_dups( ) );
		else
		{
			for ( auto y = members.begin( ), end = members.end( );
				  y < end;
				  ++y )
			{
				if ( ( *y )->is_reference( ) && 
					( x->lhsv( ( *y )->get_name( ) ) || 
					  x->this_expr_is_mem_access_of( ( *y )->get_name( ) ) ) )
				{
					x->make_ref_assign( );
					members.erase( y );
					break;
				}
			}
		}
	}
}

void ep::module::constructor_fix_statements( method_ptr ctor,
											 method_argument_list members, 
											 method_ptr & method, 
											 tokenizer_it where ) const
{
	fix_block( members, method->get_statements( ) );
	if ( !members.empty( ) )
	{
		std::stringstream ss;
		bool b = false;
		for ( auto &x : members )
			if ( x->is_reference( ) )
			{
				ss << "\t\"" << method->get_name( ) << "::" << x->get_name( ) << "\"" << std::endl;
				b = true;
			}

		if ( b )
			throw group_constructor_provides_no_initalizer_error( std::move( ctor ), 
															      ss.str( ), 
		                                                          where );
	}
}

std::function<ep::value_reference_ptr( ep::value_reference* )> ep::module::make_clone( method_list & findables ) const
{
	auto clone = std::find_if( findables.begin( ), findables.end( ), []( auto &x )
	{
		return x->get_name( ) == "clone";
	} );

	if ( clone == findables.end( ) )
		return []( value_reference *ptr )->ep::value_reference_ptr
	{
		return std::make_shared<value_reference>( );
	};
	return [clone = *clone, engine = engine_]( value_reference *ptr )
	{

		return clone->invoke( { std::make_shared<variable>( "this", 
															std::shared_ptr<value_reference>( ptr, []( value_reference *ptr ){} ), 
															true, 
															true ) }, 
						      engine );
	};
}

ep::method_ptr ep::module::make_constructor( const tokenizer_it &id ) const
{
	return std::make_shared<ep::method>( id->get_block(),
										 method_argument_list{ },
										 [id = *id]( variable_list, dispatch_engine_ptr engine )
	{
		auto vref = std::make_shared<value_reference>( nullptr,
													   engine->get_type_info( id.get_block( ) ) );
		vref->get_members( ) = vref->type_info( )->create_members( );
		return std::move( vref );
	} );
}

ep::method_ptr ep::module::make_destructor( const tokenizer_it & id ) const
{
	return std::make_shared<ep::method>( "~" + id->get_block( ),
										 method_argument_list{ },
										 []( variable_list, dispatch_engine_ptr engine )
	{
		return ep::void_return;
	} );
}

ep::statement_list ep::module::parse( tokenizer_it curr, tokenizer_it end )
{
	statement_list result;
	for ( ;
		  curr < end; )
	{
		if ( is_method( curr, end ) )
			engine_->add_method( std::move( parse_method( end, curr, end ) ) );
		else if ( is_group( curr, end ) )
			engine_->add_type( std::move( parse_group( ++curr, end ) ) );
		else if ( curr->get_block( ) == "import" )
			parse_import( ++curr, end, result );
		else if ( is_specialize( curr, end ) )
			parse_specialize( end, ++curr, end );
		else
			parse_statement( curr, end, result );
	}
	return result;
}

void ep::module::internal_eval( statement_list & block )
{
	auto state_ = engine_->get_global_state( );
	for ( auto &x : block )
	{
		try
		{
			if ( x->eval_or_eval_and_return( engine_, state_, nullptr ) )
				break;
		}
		catch ( exit_exception & )
		{
			break;
		}
	}
}

bool ep::module::is_method( tokenizer_it & curr, const tokenizer_it & end ) const
{ 
	return curr->is_identifier( ) && curr->get_block( ) == "method";
}

bool ep::module::is_group( tokenizer_it & curr, const tokenizer_it & end ) const
{
	return curr->is_identifier( ) && curr->get_block( ) == "group";
}

bool ep::module::is_specialize( tokenizer_it & curr, const tokenizer_it & end ) const
{
	return curr->is_identifier( ) && curr->get_block( ) == "specialize";
}

ep::type_info_ptr ep::module::parse_group( tokenizer_it & curr, const tokenizer_it & end ) const
{
	if ( !curr->is_identifier( ) )
		throw expected_identifier_error( { }, curr, end );
	auto id = curr++;

	if ( ( curr++ )->get_block( ) != "{" )
		throw expected_opening_bracket_error( { id }, curr, end );

	method_argument_list members;
	method_list methods;
	method_ptr ctor, dtor;
	while ( curr < end )
	{
		if ( curr->get_block( ) == "members" && ( curr + 1 )->get_block( ) == ":" )
		{
			++curr;
			++curr;
			while ( curr < end )
			{
				if ( curr->get_block( ) == "public" || curr->get_block( ) == "private" )
				{
					auto pub = ( curr++ )->get_block( ) == "public";
					if ( curr->get_block( ) != "var" )
						throw expected_class_specifiers_error( { id }, curr, end );
					auto var_id = ++curr;
					bool is_ref = false;
					if ( var_id->get_block( ) == "this" )
						throw this_not_allowed_as_identifier_error( { id }, curr, end );

					if ( !var_id->is_identifier( ) && var_id->get_block( ) != "&" )
						throw expected_identifier_error( { id }, curr, end );
					if ( var_id->get_block( ) == "&" )
						is_ref = true, var_id = ++curr;

					members.emplace_back( new method_argument( var_id->get_block( ), is_ref, pub ) );
					++curr;

					if ( curr->get_block( ) != ";" )
						throw  expected_semicolon_error( { id, var_id }, curr, end );
					++curr;
				}
				else if ( curr->get_block( ) == "var" )
				{
					auto var_id = ++curr;
					bool is_ref = false;
					if ( var_id->get_block( ) == "this" )
						throw this_not_allowed_as_identifier_error( { id }, curr, end );

					if ( !var_id->is_identifier( ) && var_id->get_block( ) != "&" )
						throw expected_identifier_error( { id }, curr, end );
					if ( var_id->get_block( ) == "&" )
						is_ref = true, var_id = ++curr;

					members.emplace_back( new method_argument( var_id->get_block( ), is_ref ) );
					++curr;

					if ( curr->get_block( ) != ";" )
						throw  expected_semicolon_error( { id, var_id }, curr, end );
					++curr;
				}
				else
					break;
			}
		}
		else if ( curr->get_block( ) == "methods" && ( curr + 1 )->get_block( ) == ":" )
		{
			++curr;
			++curr;

			while ( curr < end )
			{
				if ( curr->get_block( ) == id->get_block( ) )
					ctor = parse_constructor( id, curr, end );
				else if ( curr->get_block( ) == "~" &&
					( curr + 1 )->get_block( ) == id->get_block( ) )
					dtor = parse_destructor( id, ++curr, end );
				else if ( is_method( curr, end ) )
					methods.emplace_back( std::move( parse_method( id, curr, end ) ) );
				else if ( curr->get_block( ) == "specialize" )
				{
					auto specialize = parse_specialize( id, ++curr, end );
					for ( auto &x : methods )
					{
						if ( x->get_name( ) == specialize->get_name( ) )
						{
							x->add_specialization( std::move( specialize ) );
							break;
						}
					}
					if ( specialize != nullptr )
						auto nigger = 0;
				}
				else
					break;
			}
		}
		else if ( curr->get_block( ) == "}" && ( curr + 1 )->get_block( ) == ";" )
			break;
		else
			throw expected_closing_bracket_error( { id }, curr, end );
	}
	++curr;
	++curr;

	if ( !ctor )
		ctor = std::move( make_constructor( id ) );

	if ( !dtor )
		dtor = std::move( make_destructor( id ) );

	constructor_fix_statements( ctor, members, ctor, id );


	engine_->add_method( std::make_shared<ep::method>( id->get_block( ),
													   ctor->get_arguments( ),
													   [ctor]( variable_list args, dispatch_engine_ptr engine )
	{
		auto thisptr = std::make_shared<ep::variable>( "this",
													   std::make_shared<value_reference>( nullptr,
																						  engine->get_type_info( ctor->get_name( ) ) ),
													   true,
													   true );
		auto ref = thisptr->get_ref( );
		ref->get_members( ) = ref->type_info( )->create_members( );
		args.emplace( args.begin( ), thisptr );
		if ( !ctor->get_fun( ) )
			ctor->create_lambda( );
		ctor->get_fun( )( std::move( args ), std::move( engine ) );
		return std::move( thisptr->steal( ) );
	} ) );

	auto dtor_ = [dtor, engine = engine_]( value_reference *ptr )
	{
		auto tp = std::make_shared<variable>( "this", std::shared_ptr<value_reference>( ptr, []( value_reference *ptr ){} ),
											  true, true );
		dtor->invoke( { tp }, engine );
	};

	auto clone = make_clone( methods );
	
	return std::make_shared<ep::type_info>( id->get_block( ),
											dtor_,
											clone,
											std::move( methods ),
											std::move( members ) );
}

ep::method_ptr ep::module::parse_constructor( const tokenizer_it &group, tokenizer_it & curr, const tokenizer_it & end ) const
{
	method_argument_list arguments;
	statement_list statements;
	auto name = curr++;

	if ( curr->get_block( ) != "(" )
		throw expected_opening_parenthesis_error( { group, name }, curr, end );

	if ( ( ++curr )->get_block( ) == ")" )
	{
		if ( ( ++curr )->get_block( ) != "{" )
			throw expected_opening_bracket_error( { group, name }, curr, end );
		statements = std::move( parse_method_body( group, name, ++curr, end ) );
	}
	else
	{
		while ( true )
		{
			if ( curr == end )
				throw ran_out_of_tokens_error( { group, name }, --curr, end );
			bool is_ref = false;
			if ( curr->get_block( ) == "&" )
				++curr, is_ref = true;

			if ( curr->is_identifier( ) )
				arguments.emplace_back( new method_argument( ( curr++ )->get_block( ), is_ref ) );
			else
				throw expected_identifier_error( { group, name }, curr, end );

			if ( curr->get_block( ) == ")" )
				break;
			else if ( curr->get_block( ) != "," )
				throw expected_closing_parenthesis_error( { group, name }, curr, end );
			++curr;
		}

		if ( ( ++curr )->get_block( ) != "{" )
			throw expected_opening_bracket_error( { group, name }, curr, end );

		statements = std::move( parse_method_body( group, name, ++curr, end ) );
	}
	optimizer_->optimize_all( statements );
	return std::make_shared<ep::method>( name->get_block( ),
										 std::move( arguments ),
										 std::move( statements ) );
}

ep::method_ptr ep::module::parse_destructor( const tokenizer_it &group, tokenizer_it & curr, const tokenizer_it & end ) const
{ 
	method_argument_list arguments;
	statement_list statements;
	auto name = curr++;
	name->get_block( ).insert( name->get_block( ).begin( ), '~' );

	if ( curr->get_block( ) != "(" )
		throw expected_opening_parenthesis_error( { group, name }, curr, end );

	if ( ( ++curr )->get_block( ) == ")" )
	{
		if ( ( ++curr )->get_block( ) != "{" )
			throw expected_opening_bracket_error( { group, name }, curr, end );
		statements = std::move( parse_method_body( group, name, ++curr, end ) );
	}
	else
	{
		while ( true )
		{
			if ( curr == end )
				throw ran_out_of_tokens_error( { group, name }, curr, end );
			bool is_ref = false;
			if ( curr->get_block( ) == "&" )
				++curr, is_ref = true;

			if ( curr->is_identifier( ) )
				arguments.emplace_back( new method_argument( ( curr++ )->get_block( ), is_ref ) );
			else
				throw expected_identifier_error( { group, name }, curr, end );

			if ( curr->get_block( ) == ")" )
				break;
			else if ( curr->get_block( ) != "," )
				throw expected_closing_parenthesis_error( { group, name }, curr, end );
			++curr;
		}

		if ( ( ++curr )->get_block( ) != "{" )
			throw expected_opening_bracket_error( { group, name }, curr, end );

		statements = std::move( parse_method_body( group, name, ++curr, end ) );
	}
	optimizer_->optimize_all( statements );
	return std::make_shared<ep::method>( name->get_block( ),
										 std::move( arguments ),
										 std::move( statements ) );
}

void ep::module::parse_import( tokenizer_it & curr, const tokenizer_it & end, statement_list &block )
{ 
	if ( curr->is_identifier( ) )
	{
		auto id = curr++;
		if ( curr->get_block( ) != ";" )
			throw expected_semicolon_error( { }, curr, end );

		auto fn = find_import_fun( id->get_block( ) );
		if ( fn == bad_import_fun( ) )
			throw library_is_undefined_error( id );

		if ( is_included( fn->first ) )
			return;

		fn->second( engine_ );
		includes_.emplace_back( fn->first );
		++curr;
	}
	else if ( curr->is_string( ) )
	{
		auto id = curr++;
		if ( curr->get_block( ) != ";" )
			throw expected_semicolon_error( { id }, curr, end );

		if ( id->get_block( ).find( ".dll" ) != std::string::npos )
		{
			auto plugin = ep::load_plugin( id->get_block( ) );
			std::string name = plugin->get_name_fn( )( );
			if ( is_included( name ) )
				return;
			plugin->get_load_fn( )( engine_ );
			includes_.emplace_back( std::move( name ) );
			return;
		}

		if ( is_included( id->get_block( ) ) )
			return;

		auto tokenizer = std::make_shared<cpputils::cpp_tokenizer>( );
		tokenizer->tokenize( { std::istreambuf_iterator<char>( std::fstream( id->get_block( ) ) ),
							   std::istreambuf_iterator<char>( ) } );
		auto block2 = parse( tokenizer->begin( ),
							 tokenizer->end( ) );
		block.insert( block.empty( ) ? block.begin( ) : block.end( ) - 1,
					  block2.begin( ),
					  block2.end( ) );
		includes_.emplace_back( id->get_block( ) );
		++curr;
	}
	else if ( curr->get_block( ) == "(" )
	{
		++curr;
		while ( curr < end )
		{
			if ( curr->is_identifier( ) )
			{
				auto id = curr++;
				auto fn = find_import_fun( id->get_block( ) );
				if ( fn == bad_import_fun( ) )
					throw library_is_undefined_error( id );
				if ( is_included( fn->first ) )
					continue;
				fn->second( engine_ );
				includes_.emplace_back( fn->first );
			}
			else if ( curr->is_string( ) )
			{
				auto id = curr++;

				if ( id->get_block( ).find( ".dll" ) != std::string::npos )
				{
					auto plugin = ep::load_plugin( id->get_block( ) );
					std::string name = plugin->get_name_fn( )( );
					if ( is_included( name ) )
						continue;
					plugin->get_load_fn( )( engine_ );
					includes_.emplace_back( std::move( name ) );
					continue;
				}
				if ( is_included( id->get_block( ) ) )
					continue;
				auto tokenizer = std::make_shared<cpputils::cpp_tokenizer>( );
				tokenizer->tokenize( { std::istreambuf_iterator<char>( std::fstream( id->get_block( ) ) ),
									 std::istreambuf_iterator<char>( ) } );
				auto block2 = parse( tokenizer->begin( ),
									 tokenizer->end( ) );
				block.insert( block.empty( ) ? block.begin( ) : block.end( ) - 1,
							  block2.begin( ),
							  block2.end( ) );
				includes_.emplace_back( id->get_block( ) );
			}
			else if ( curr->get_block( ) == ")" )
				break;
			else if ( curr->get_block( ) == "," )
			{
				++curr;
				continue;
			}
			else
				throw expected_closing_parenthesis_error( { }, curr, end );
		}
		++curr;

		if ( curr->get_block( ) != ";" )
			throw expected_semicolon_error( { }, curr, end );
		++curr;
	}
	else
		throw expected_import_identifier_error( curr );
}

ep::method_ptr ep::module::parse_specialize( const tokenizer_it & group, tokenizer_it & curr, const tokenizer_it & end ) const
{
	auto id = curr - 1;
	if ( curr->get_block( ) != "<" )
		throw expected_opening_specialize_operator_error( { group, id }, curr, end );
	auto expr = std::make_shared<ep::expression>( std::vector<tokenizer_it>{ group, id }, ++curr, end, engine_ );
	if ( curr->get_block( ) != ">" )
		throw expected_closing_bracket_error( { group, id }, curr, end );
	++curr;
	if ( !is_method( curr, end ) )
		throw expected_method_after_specialize_error( { group, id }, curr, end );
	auto method = parse_method( group, curr, end );
	method->make_specialization( std::move( expr ) );
	if ( group == end )
		engine_->get_method( method->get_name( ) )->add_specialization( method );
	return method;
}

ep::method_ptr ep::module::parse_method( const tokenizer_it &group, tokenizer_it & curr, const tokenizer_it & end ) const
{
	auto make_identifier = []( auto it, auto end )
	{
		auto result = ( it++ )->get_block( );
		while ( it < end )
			result += ( it++ )->get_block( );
		return result;
	};

	method_argument_list arguments;
	statement_list statements;

	if ( !( ++curr )->is_identifier( ) && curr->get_block( ) != "'" )
		throw expected_identifier_error( { }, curr, end );
	auto name = curr++;

	if ( curr->get_block( ) != "(" )
		throw expected_opening_parenthesis_error( { name }, curr, end );

	if ( ( ++curr )->get_block( ) == ")" )
	{
		if ( ( ++curr )->get_block( ) != "{" )
			throw expected_opening_bracket_error( { name }, curr, end );
		statements = std::move( parse_method_body( group, name, ++curr, end ) );
	}
	else
	{
		while ( true )
		{
			if ( curr == end )
				throw ran_out_of_tokens_error( { name }, --curr, end );
			bool is_ref = false;
			if ( curr->get_block( ) == "&" )
				++curr, is_ref = true;

			if ( curr->is_identifier( ) )
				arguments.emplace_back( new method_argument( ( curr++ )->get_block( ), is_ref ) );
			else
				throw expected_identifier_error( { name }, curr, end );

			if ( curr->get_block( ) == ")" )
				break;
			else if ( curr->get_block( ) != "," )
				throw expected_closing_parenthesis_error( { name }, curr, end );
			++curr;
		}

		if ( ( ++curr )->get_block( ) != "{" )
			throw expected_opening_bracket_error( { name }, curr, end );

		statements = std::move( parse_method_body( group, name, ++curr, end ) );
	}

	optimizer_->optimize_all( statements );
	return std::make_shared<method>( name->get_block( ), arguments, statements );
}

void ep::module::parse_statement( tokenizer_it & curr, const tokenizer_it & end, statement_list & output )
{ 
	output.emplace_back( new statement( { }, curr, end, engine_ ) );
}

ep::statement_list ep::module::parse_method_body( const tokenizer_it &group,
												  const tokenizer_it &method,
												  tokenizer_it &curr,
												  const tokenizer_it &end ) const
{
	ep::statement_list statements;
	while ( true )
	{
		if ( curr == end )
			throw ran_out_of_tokens_error( { group, method }, --curr, end );

		if ( curr->get_block( ) == "}" )
		{
			if ( ( ++curr )->get_block( ) != ";" )
				throw expected_semicolon_error( { group, method }, curr, end );
			++curr;
			break;
		}

		statements.emplace_back( new statement( { group, method }, curr, end, engine_ ) );
	}
	
	// Optimize block
	return statements;
}

std::vector<std::pair<std::string, std::function<void( ep::dispatch_engine_ptr )>>>::iterator ep::module::find_import_fun( const std::string & id )
{
	for ( auto it = import_funs_.begin( ), end = import_funs_.end( );
		  it < end;
		  ++it )
		if ( it->first == id )
			return it;
	return bad_import_fun( );
}

std::vector<std::pair<std::string, std::function<void( ep::dispatch_engine_ptr )>>>::iterator ep::module::bad_import_fun( )
{
	return import_funs_.end( );
}

