/********************************************************
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*			   dispatch_state.h					*	*
*  	*			   Created by Yamiez				*	*
*  	*	Do not forget to give credit when using		*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*********************************************************/
#pragma once
#include "all_defines.h"


namespace ep
{

	// The state of a function
	class dispatch_state
	{
	public:
		dispatch_state( variable_list arguments, dispatch_engine *engine, dispatch_state_ptr parent );
		dispatch_state( variable_list arguments, dispatch_engine *engine );
		dispatch_state( dispatch_state_ptr parent, dispatch_engine *engine );
		~dispatch_state( );

		variable_list &get_variables( );

		variable_ptr get_variable( const std::string &id );

		variable_ptr get_this( );

		method_ptr get_method( const std::string &id );

		void add_variable( variable_ptr var );

		dispatch_engine *get_engine( );

		void reset( );

		std::string pretty_format( ) const;

	private:
		variable_list::reverse_iterator find_local( const std::string &id );
		variable_list::reverse_iterator bad_it( );

	private:
		dispatch_state_ptr parent_;
		dispatch_engine *engine_;
		variable_list locals_;
		variable_ptr thisptr_;
	};

}

