/********************************************************
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*				 parsing_error.cpp					*	*
*  	*			   Created by Yamiez				*	*
*  	*	Do not forget to give credit when using		*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*********************************************************/
#include "eval_error.h"
#include "type_system.h"
#include "method.h"
#include "method_argument.h"
#include "expression.h"
#include "variable.h"
#include "value_reference.h"


ep::ep_error::ep_error( )
	: std::exception( )
{ }

ep::ep_error::ep_error( const std::string & err )
	: std::exception( err.c_str( ) )
{ }

std::string ep::ep_error::pretty_format( ) const
{
	return what( );
}

ep::conversion_error::conversion_error( type_info_ptr method_type, method_ptr scope, type_info_ptr from, std::vector<type_info_ptr> to )
	: method_ti_( std::move( method_type ) ), method_( std::move( scope ) ), from_( std::move( from ) ), to_( std::move( to ) )
{ }

std::string ep::conversion_error::pretty_format( ) const
{
	std::stringstream ss;
	ss << "expression parser run-time error" << std::endl;
	ss << "\tconversion error, no conversion available from type '" << from_->get_name( ) << "' to ";
	for ( auto it = to_.begin( ), end = to_.end( );
		  it < end;
		  ++it )
		if ( ( it + 1 ) == end && to_.size( ) > 1 )
			ss << "and '" << ( *it )->get_name( ) << "'.";
		else if ( ( it + 1 ) == end )
			ss << "'" << ( *it )->get_name( ) << "'.";
		else
			ss << "'" << ( *it )->get_name( ) << "', ";
	ss << std::endl;
	ss << "see note: " << std::endl;
	ss << "\t'";
	if ( method_ti_ )
		ss << method_ti_->get_name( ) << "::";
	ss << method_->pretty_format( ) << "'." << std::endl;
	return ss.str( );
}

ep::arity_error::arity_error( type_info_ptr method_type, method_ptr scope, expression * where )
	: method_ti_( std::move( method_type ) ), method_( std::move( scope ) ), where_( where )
{
	
}

std::string ep::arity_error::pretty_format( ) const
{
	std::stringstream ss;
	ss << "expression parser run-time error" << std::endl;
	ss << "\tarity error, too few arguments provided (arity) to method '";
	if ( method_ti_ )
		ss << method_ti_->get_name( ) << "::";
	ss << method_->pretty_format( ) << "'" << std::endl;
	if ( where_ )
	{
		ss << "see note: " << std::endl;
		ss << "\t'" << where_->pretty_format( ) << "' (" << where_->get_position( ).get_row( ) << ")";
	}
	return ss.str( );
}

ep::vector_for_each_error::vector_for_each_error( method_ptr method )
	: method_( std::move( method ) )
{ }

std::string ep::vector_for_each_error::pretty_format( ) const
{
	std::stringstream ss;
	ss << "expression parser run-time error" << std::endl;
	ss << "\t'vector::for_each' expects argument 'method' to be of string_literal type and" << std::endl <<
		"\tis the name of a global method with two arguments." << std::endl <<
		"see note: " << std::endl <<
		"\t" << "'" << method_->pretty_format( ) << "'" << std::endl;
	return ss.str( );
}

ep::empty_code_error::empty_code_error( )
{ }

std::string ep::empty_code_error::pretty_format( ) const
{
	std::stringstream ss;
	ss << "expression parser syntax error" << std::endl;
	ss << "\tcannot evaluate empty code" << std::endl;
	return ss.str( );
}

ep::library_identifier_collide_error::library_identifier_collide_error( std::string name )
	: name_( std::move( name ) )
{ }

std::string ep::library_identifier_collide_error::pretty_format( ) const
{
	std::stringstream ss;
	ss << "expression parser compile time error" << std::endl;
	ss << "\nlibrary identifier '" << name_ << "' already exists in this module instance." << std::endl;
	return ss.str( );
}

ep::group_constructor_provides_no_initalizer_error::group_constructor_provides_no_initalizer_error( method_ptr ctor, 
																						std::string members, 
																						tokenizer_it where )
{
	std::stringstream ss;
	ss << "expression parser compile time error" << std::endl;
	ss << "\t'" << ctor->get_name( ) << "::" << ctor->pretty_format( ) << "' provides no initalizer(s) for: " << std::endl;
	ss << "\t" << members << std::endl;
	ss << "see note: " << std::endl;
	ss << "\tconstructor is located at (" << where->get_position( ).get_row( ) << ")." << std::endl;
	format_ = std::move( ss.str( ) );
}

std::string ep::group_constructor_provides_no_initalizer_error::pretty_format( ) const
{
	return format_;
}

ep::expected_identifier_error::expected_identifier_error( std::vector<tokenizer_it> refs, tokenizer_it where, tokenizer_it end )
{
	std::stringstream ss;
	ss << "expression parser compile time error" << std::endl;
	ss << "\texpected identifier" << std::endl;
	for ( auto &ref : refs )
		if ( ref != end )
		{
			ss << "see note: " << std::endl;
			ss << "\tin scope '" << ref->get_block( ) << "' (" << ref->get_position( ).get_row( ) << ")" << std::endl;
		}
	ss << "see note: " << std::endl;
	ss << "\tat line (" << where->get_position( ).get_row( ) << ")" << std::endl;
	format_ = ss.str( );
}

std::string ep::expected_identifier_error::pretty_format( ) const
{
	return format_;
}

ep::expected_opening_bracket_error::expected_opening_bracket_error( std::vector<tokenizer_it> refs, tokenizer_it where, tokenizer_it end )
{
	std::stringstream ss;
	ss << "expression parser compile time error" << std::endl;
	ss << "\texpected opening bracket" << std::endl;
	for ( auto &ref : refs )
		if ( ref != end )
		{
			ss << "see note: " << std::endl;
			ss << "\tin scope '" << ref->get_block( ) << "' (" << ref->get_position( ).get_row( ) << ")" << std::endl;
		}
	ss << "see note: " << std::endl;
	ss << "\tat line (" << where->get_position( ).get_row( ) << ")" << std::endl;
	format_ = ss.str( );
}

std::string ep::expected_opening_bracket_error::pretty_format( ) const
{
	return format_;
}

ep::expected_class_specifiers_error::expected_class_specifiers_error( std::vector<tokenizer_it> refs, tokenizer_it where, tokenizer_it end )
{
	std::stringstream ss;
	ss << "expression parser compile time error" << std::endl;
	ss << "\texpected one or more class specifiers" << std::endl;
	for ( auto &ref : refs )
		if ( ref != end )
		{
			ss << "see note: " << std::endl;
			ss << "\tin scope '" << ref->get_block( ) << "' (" << ref->get_position( ).get_row( ) << ")" << std::endl;
		}
	ss << "see note: " << std::endl;
	ss << "\tat line (" << where->get_position( ).get_row( ) << ")" << std::endl;
	format_ = ss.str( );
}

std::string ep::expected_class_specifiers_error::pretty_format( ) const
{
	return format_;
}

ep::this_not_allowed_as_identifier_error::this_not_allowed_as_identifier_error( std::vector<tokenizer_it> refs, tokenizer_it where, tokenizer_it end )
{
	std::stringstream ss;
	ss << "expression parser compile time error" << std::endl;
	ss << "\t'this' not allowed as an identifier" << std::endl;
	for ( auto &ref : refs )
		if ( ref != end )
		{
			ss << "see note: " << std::endl;
			ss << "\tin scope '" << ref->get_block( ) << "' (" << ref->get_position( ).get_row( ) << ")" << std::endl;
		}
	ss << "see note: " << std::endl;
	ss << "\tat line (" << where->get_position( ).get_row( ) << ")" << std::endl;
	format_ = ss.str( );
}

std::string ep::this_not_allowed_as_identifier_error::pretty_format( ) const
{
	return format_;
}

ep::expected_semicolon_error::expected_semicolon_error( std::vector<tokenizer_it> refs, tokenizer_it where, tokenizer_it end )
{
	std::stringstream ss;
	ss << "expression parser compile time error" << std::endl;
	ss << "\texpected semicolon" << std::endl;
	for ( auto &ref : refs )
		if ( ref != end )
		{
			ss << "see note: " << std::endl;
			ss << "\tin scope '" << ref->get_block( ) << "' (" << ref->get_position( ).get_row( ) << ")" << std::endl;
		}
	ss << "see note: " << std::endl;
	ss << "\tat line (" << where->get_position( ).get_row( ) << ")" << std::endl;
	format_ = ss.str( );
}

std::string ep::expected_semicolon_error::pretty_format( ) const
{
	return format_;
}

ep::expected_closing_bracket_error::expected_closing_bracket_error( std::vector<tokenizer_it> refs, tokenizer_it where, tokenizer_it end )
{
	std::stringstream ss;
	ss << "expression parser compile time error" << std::endl;
	ss << "\texpected closing bracket" << std::endl;
	for ( auto &ref : refs )
		if ( ref != end )
		{
			ss << "see note: " << std::endl;
			ss << "\tin scope '" << ref->get_block( ) << "' (" << ref->get_position( ).get_row( ) << ")" << std::endl;
		}
	ss << "see note: " << std::endl;
	ss << "\tat line (" << where->get_position( ).get_row( ) << ")" << std::endl;
	format_ = ss.str( );
}

std::string ep::expected_closing_bracket_error::pretty_format( ) const
{
	return format_;
}

ep::expected_opening_parenthesis_error::expected_opening_parenthesis_error( std::vector<tokenizer_it> refs, tokenizer_it where, tokenizer_it end )
{
	std::stringstream ss;
	ss << "expression parser compile time error" << std::endl;
	ss << "\texpected opening parenthesis" << std::endl;
	for ( auto &ref : refs )
		if ( ref != end )
		{
			ss << "see note: " << std::endl;
			ss << "\tin scope '" << ref->get_block( ) << "' (" << ref->get_position( ).get_row( ) << ")" << std::endl;
		}
	ss << "see note: " << std::endl;
	ss << "\tat line (" << where->get_position( ).get_row( ) << ")" << std::endl;
	format_ = ss.str( );
}

std::string ep::expected_opening_parenthesis_error::pretty_format( ) const
{
	return format_;
}

ep::expected_closing_parenthesis_error::expected_closing_parenthesis_error( std::vector<tokenizer_it> refs, tokenizer_it where, tokenizer_it end )
{ 
	std::stringstream ss;
	ss << "expression parser compile time error" << std::endl;
	ss << "\texpected closing parenthesis" << std::endl;
	for ( auto &ref : refs )
		if ( ref != end )
		{
			ss << "see note: " << std::endl;
			ss << "\tin scope '" << ref->get_block( ) << "' (" << ref->get_position( ).get_row( ) << ")" << std::endl;
		}
	ss << "see note: " << std::endl;
	ss << "\tat line (" << where->get_position( ).get_row( ) << ")" << std::endl;
	format_ = ss.str( );
}

std::string ep::expected_closing_parenthesis_error::pretty_format( ) const
{
	return format_;
}

ep::ran_out_of_tokens_error::ran_out_of_tokens_error( std::vector<tokenizer_it> refs, tokenizer_it where, tokenizer_it end )
{ 
	std::stringstream ss;
	ss << "expression parser compile time error" << std::endl;
	ss << "\tran out of tokens when parsing code" << std::endl;
	for ( auto &ref : refs )
		if ( ref != end )
		{
			ss << "see note: " << std::endl;
			ss << "\tin scope '" << ref->get_block( ) << "' (" << ref->get_position( ).get_row( ) << ")" << std::endl;
		}
	ss << "see note: " << std::endl;
	ss << "\tat line (" << where->get_position( ).get_row( ) << ")" << std::endl;
	format_ = ss.str( );
}

std::string ep::ran_out_of_tokens_error::pretty_format( ) const
{
	return format_;
}

ep::library_is_undefined_error::library_is_undefined_error( tokenizer_it where )
	: where_( std::move( where ) )
{ }

std::string ep::library_is_undefined_error::pretty_format( ) const
{
	std::stringstream ss;
	ss << "expression parser compile time error" << std::endl;
	ss << "\tlibrary identifier is undefined" << std::endl;
	ss << "see note: " << std::endl;
	ss << "\tat line (" << where_->get_position( ).get_row( ) << ")" << std::endl;
	return ss.str( );
}

ep::expected_import_identifier_error::expected_import_identifier_error( tokenizer_it where )
	: where_( std::move( where ) )
{ }

std::string ep::expected_import_identifier_error::pretty_format( ) const
{
	std::stringstream ss;
	ss << "expression parser compile time error" << std::endl;
	ss << "\texpected library identifier" << std::endl;
	ss << "see note: " << std::endl;
	ss << "\tat line (" << where_->get_position( ).get_row( ) << ")" << std::endl;
	return ss.str( );
}

ep::unmatched_single_quote_error::unmatched_single_quote_error( std::vector<tokenizer_it> refs, tokenizer_it where, tokenizer_it end )
{
	std::stringstream ss;
	ss << "expression parser compile time error" << std::endl;
	ss << "\tunmatched single quote (')" << std::endl;
	for ( auto &ref : refs )
		if ( ref != end )
		{
			ss << "see note: " << std::endl;
			ss << "\tin scope '" << ref->get_block( ) << "' (" << ref->get_position( ).get_row( ) << ")" << std::endl;
		}
	ss << "see note: " << std::endl;
	ss << "\tat line (" << where->get_position( ).get_row( ) << ")" << std::endl;
	format_ = ss.str( );
}

std::string ep::unmatched_single_quote_error::pretty_format( ) const
{
	return format_;
}

ep::misusage_break_error::misusage_break_error( std::vector<tokenizer_it> refs, tokenizer_it where, tokenizer_it end )
{ 
	std::stringstream ss;
	ss << "expression parser compile time error" << std::endl;
	ss << "\tmisusage of 'break' keyword" << std::endl;
	for ( auto &ref : refs )
		if ( ref != end )
		{
			ss << "see note: " << std::endl;
			ss << "\tin scope '" << ref->get_block( ) << "' (" << ref->get_position( ).get_row( ) << ")" << std::endl;
		}
	ss << "see note: " << std::endl;
	ss << "\tat line (" << where->get_position( ).get_row( ) << ")" << std::endl;
	format_ = ss.str( );
}

std::string ep::misusage_break_error::pretty_format( ) const
{
	return format_;
}

ep::misusage_continue_error::misusage_continue_error( std::vector<tokenizer_it> refs, tokenizer_it where, tokenizer_it end )
{
	std::stringstream ss;
	ss << "expression parser compile time error" << std::endl;
	ss << "\tmisusage of 'continue' keyword" << std::endl;
	for ( auto &ref : refs )
		if ( ref != end )
		{
			ss << "see note: " << std::endl;
			ss << "\tin scope '" << ref->get_block( ) << "' (" << ref->get_position( ).get_row( ) << ")" << std::endl;
		}
	ss << "see note: " << std::endl;
	ss << "\tat line (" << where->get_position( ).get_row( ) << ")" << std::endl;
	format_ = ss.str( );
}

std::string ep::misusage_continue_error::pretty_format( ) const
{
	return format_;
}

ep::unknown_token_error::unknown_token_error( std::vector<tokenizer_it> refs, tokenizer_it where, tokenizer_it end )
{
	std::stringstream ss;
	ss << "expression parser compile time error" << std::endl;
	ss << "\tunknown token" << std::endl;
	for ( auto &ref : refs )
		if ( ref != end )
		{
			ss << "see note: " << std::endl;
			ss << "\tin scope '" << ref->get_block( ) << "' (" << ref->get_position( ).get_row( ) << ")" << std::endl;
		}
	ss << "see note: " << std::endl;
	ss << "\ttoken '" << where->get_block( ) << "'at line (" << where->get_position( ).get_row( ) << ")" << std::endl;
	format_ = ss.str( );
}

std::string ep::unknown_token_error::pretty_format( ) const
{
	return format_;
}

ep::cannot_deduce_auto_type_error::cannot_deduce_auto_type_error( std::vector<tokenizer_it> refs, tokenizer_it where, tokenizer_it end )
{
	std::stringstream ss;
	ss << "expression parser compile time error" << std::endl;
	ss << "\tcannot deduce 'auto' type (initalizer required)" << std::endl;
	for ( auto &ref : refs )
		if ( ref != end )
		{
			ss << "see note: " << std::endl;
			ss << "\tin scope '" << ref->get_block( ) << "' (" << ref->get_position( ).get_row( ) << ")" << std::endl;
		}
	ss << "see note: " << std::endl;
	ss << "\tat line (" << where->get_position( ).get_row( ) << ")" << std::endl;
	format_ = ss.str( );
}

std::string ep::cannot_deduce_auto_type_error::pretty_format( ) const
{
	return format_;
}

ep::expected_expression_error::expected_expression_error( std::vector<tokenizer_it> refs, tokenizer_it where, tokenizer_it end )
{
	std::stringstream ss;
	ss << "expression parser compile time error" << std::endl;
	ss << "\texpected expression" << std::endl;
	for ( auto &ref : refs )
		if ( ref != end )
		{
			ss << "see note: " << std::endl;
			ss << "\tin scope '" << ref->get_block( ) << "' (" << ref->get_position( ).get_row( ) << ")" << std::endl;
		}
	ss << "see note: " << std::endl;
	ss << "\tat line (" << where->get_position( ).get_row( ) << ")" << std::endl;
	format_ = ss.str( );
}

std::string ep::expected_expression_error::pretty_format( ) const
{
	return format_;
}

ep::undefined_identifier_error::undefined_identifier_error( std::string id )
	: id_( std::move( id ) )
{ }

std::string ep::undefined_identifier_error::pretty_format( ) const
{
	std::stringstream ss;
	ss << "expression parser run-time error" << std::endl;
	ss << "\tattempted access of undefined symbol '" << id_ << "'" << std::endl;
	return ss.str( );
}

ep::symbol_collision_error::symbol_collision_error( type_info_ptr type, method_ptr method, std::string id )
	: ti_( type.get() ), method_( std::move( method ) ), id_( std::move( id ) )
{ }

ep::symbol_collision_error::symbol_collision_error( type_info * type, method_ptr method, std::string id )
	: ti_( type ), method_( std::move( method ) ), id_( std::move( id ) )
{ }

std::string ep::symbol_collision_error::pretty_format( ) const
{
	std::stringstream ss;
	ss << "expression parser run-time error" << std::endl;
	ss << "\tsymbol '";
	if ( ti_ )
		ss << ti_->get_name( ) << "::";
	if ( method_ )
		ss << method_->pretty_format( );
	else
		ss << id_;
	ss << "' already exists (symbol collision)" << std::endl;

	if ( method_ )
	{
		ss << "see note: " << std::endl;
		ss << "\tmethods are not allowed to be overloaded" << std::endl;
	}
	return ss.str( );
}

ep::type_redefiniton_error::type_redefiniton_error( type_info_ptr existing, type_info_ptr attempted )
	: existing_( std::move( existing ) ), attempted_( std::move( attempted ) )
{ }

std::string ep::type_redefiniton_error::pretty_format( ) const
{
	std::stringstream ss;
	ss << "expression parser run-time error" << std::endl;
	ss << "\ttype redefiniton, different meaning" << std::endl;
	ss << "see note: " << std::endl;
	ss << "\tfirst type definition" << std::endl;
	for ( auto &x : existing_->get_methods( ) )
		ss << "\t\t'" << existing_->get_name( ) << "::" << x->pretty_format( ) << "'" << std::endl;
	for ( auto &x : existing_->get_members( ) )
		ss << "\t\t'" << (x->is_public() ? "public var " : "private var ") << (x->is_reference() ? "&" : "") 
		   << existing_->get_name( ) << "::" << x->get_name( ) << "'" << std::endl;
	
	ss << "see note: " << std::endl;
	ss << "\tattempted type redefinition" << std::endl;
	for ( auto &x : existing_->get_methods( ) )
		ss << "\t\t'" << existing_->get_name( ) << "::" << x->pretty_format( ) << "'" << std::endl;
	for ( auto &x : existing_->get_members( ) )
		ss << "\t\t'" << ( x->is_public( ) ? "public var " : "private var " ) << ( x->is_reference( ) ? "&" : "" )
		   << existing_->get_name( ) << "::" << x->get_name( ) << "'" << std::endl;
	return ss.str( );
}

ep::undefined_type_error::undefined_type_error( std::string id )
	: id_( std::move( id ) )
{ }

std::string ep::undefined_type_error::pretty_format( ) const
{
	std::stringstream ss;
	ss << "expression parser run-time error" << std::endl;
	ss << "\tattempting to access undefined type '" << id_ << "'" << std::endl;
	return ss.str( );
}

ep::member_is_inaccessible::member_is_inaccessible( value_reference_ptr where, variable_ptr member, expression *expr )
{
	std::stringstream ss;
	ss << "expression parser run-time error" << std::endl;
	ss << "\tmember '" << where->type_info( )->get_name( ) << "::" << member->get_name( ) << "' is inaccessible" << std::endl;

	ss << "see note: " << std::endl;
	ss << "\tlocated in group '" << where->type_info( )->get_name( ) << "'" << std::endl;

	ss << "see note: " << std::endl;
	ss << "\tdeclaration 'private var " << ( member->is_reference( ) ? "&" : "" ) << member->get_name( ) << "'" << std::endl;

	ss << "see note: " << std::endl;
	ss << "\tfinally (accesse) '" << expr->pretty_format( ) << "'" << std::endl;
	format_ = ss.str( );
}

std::string ep::member_is_inaccessible::pretty_format( ) const
{
	return format_;
}

ep::invalid_access_usage_error::invalid_access_usage_error( expression * expr )
	: expr_( expr )
{ }

std::string ep::invalid_access_usage_error::pretty_format( ) const
{
	std::stringstream ss;
	ss << "expression parser run-time error" << std::endl;
	ss << "\tinvalid usage of '->' (proper usage object->member[...])" << std::endl;
	ss << "see note: " << std::endl;
	ss << "\texpression '" << expr_->pretty_format( ) << "'" << std::endl;
	return ss.str( );
}

ep::comma_was_misplaced_error::comma_was_misplaced_error( std::vector<tokenizer_it> refs, tokenizer_it where, tokenizer_it end )
{
	std::stringstream ss;
	ss << "expression parser compile time error" << std::endl;
	ss << "\tcomma was misplaced (,)" << std::endl;
	for ( auto &ref : refs )
		if ( ref != end )
		{
			ss << "see note: " << std::endl;
			ss << "\tin scope '" << ref->get_block( ) << "' (" << ref->get_position( ).get_row( ) << ")" << std::endl;
		}
	ss << "see note: " << std::endl;
	ss << "\tat line (" << where->get_position( ).get_row( ) << ")" << std::endl;
	format_ = ss.str( );
}

std::string ep::comma_was_misplaced_error::pretty_format( ) const
{
	return format_;
}

ep::lambda_creation_failed_error::lambda_creation_failed_error( method * method )
	: method_( method )
{ }

std::string ep::lambda_creation_failed_error::pretty_format( ) const
{
	std::stringstream ss;
	ss << "expression parser run-time error" << std::endl;
	ss << "\tfailed to convert method to std::function object" << std::endl;
	ss << "see note: " << std::endl;
	ss << "\tin '" << method_->pretty_format( ) << "'" << std::endl;
	return ss.str( );
}

ep::bad_value_cast_error::bad_value_cast_error( const std::type_info * t1, const std::type_info * t2 )
	: t1_( t1 ), t2_( t2 )
{ }

std::string ep::bad_value_cast_error::pretty_format( ) const
{
	std::stringstream ss;
	ss << "expression parser run-time error" << std::endl;
	ss << "\tunable to cast '" << t1_->name( ) << "' to '" << t2_->name( ) << "'" << std::endl;
	return ss.str( );
}

ep::bad_vref_steal_error::bad_vref_steal_error( std::string id )
	: id_( std::move( id ) )
{ }

std::string ep::bad_vref_steal_error::pretty_format( ) const
{
	std::stringstream ss;
	ss << "expression parser run-time error" << std::endl;
	ss << "\tunable to steal value_reference from '" << id_ << "' (it has already been stolen)" << std::endl;
	ss << "see note: " << std::endl;
	ss << "\tthis is most likely an evaluater bug, please make a detailed report on mpgh.net" << std::endl;
	return ss.str( );
}

ep::ep_error_compilation::ep_error_compilation( std::vector<std::string> errors )
	: errors_( std::move( errors ) )
{ }

std::string ep::ep_error_compilation::pretty_format( ) const
{
	std::stringstream ss;
	for ( auto &x : errors_ )
		ss << x << std::endl;
	return ss.str( );
}

ep::expected_opening_specialize_operator_error::expected_opening_specialize_operator_error( std::vector<tokenizer_it> refs, tokenizer_it where, tokenizer_it end )
{
	std::stringstream ss;
	ss << "expression parser compile time error" << std::endl;
	ss << "\texpected opening '<' operator (in specialize)" << std::endl;
	for ( auto &ref : refs )
		if ( ref != end )
		{
			ss << "see note: " << std::endl;
			ss << "\tin scope '" << ref->get_block( ) << "' (" << ref->get_position( ).get_row( ) << ")" << std::endl;
		}
	ss << "see note: " << std::endl;
	ss << "\tat line (" << where->get_position( ).get_row( ) << ")" << std::endl;
	format_ = ss.str( );
}

std::string ep::expected_opening_specialize_operator_error::pretty_format( ) const
{
	return format_;
}

ep::expected_closing_specialize_operator_error::expected_closing_specialize_operator_error( std::vector<tokenizer_it> refs, tokenizer_it where, tokenizer_it end )
{
	std::stringstream ss;
	ss << "expression parser compile time error" << std::endl;
	ss << "\texpected closing '>' operator (in specialize)" << std::endl;
	for ( auto &ref : refs )
		if ( ref != end )
		{
			ss << "see note: " << std::endl;
			ss << "\tin scope '" << ref->get_block( ) << "' (" << ref->get_position( ).get_row( ) << ")" << std::endl;
		}
	ss << "see note: " << std::endl;
	ss << "\tat line (" << where->get_position( ).get_row( ) << ")" << std::endl;
	format_ = ss.str( );
}

std::string ep::expected_closing_specialize_operator_error::pretty_format( ) const
{
	return format_;
}

ep::expected_method_after_specialize_error::expected_method_after_specialize_error( std::vector<tokenizer_it> refs, tokenizer_it where, tokenizer_it end )
{
	std::stringstream ss;
	ss << "expression parser compile time error" << std::endl;
	ss << "\texpected method after 'specialize' statement" << std::endl;
	for ( auto &ref : refs )
		if ( ref != end )
		{
			ss << "see note: " << std::endl;
			ss << "\tin scope '" << ref->get_block( ) << "' (" << ref->get_position( ).get_row( ) << ")" << std::endl;
		}
	ss << "see note: " << std::endl;
	ss << "\tat line (" << where->get_position( ).get_row( ) << ")" << std::endl;
	format_ = ss.str( );
}

std::string ep::expected_method_after_specialize_error::pretty_format( ) const
{
	return format_;
}

ep::invalid_specialization_expression_error::invalid_specialization_expression_error( ep::method * real_method, ep::method_ptr spec_method )
{ }

std::string ep::invalid_specialization_expression_error::pretty_format( ) const
{
	return format_;
}

ep::dyanamic_object_no_initialization::dyanamic_object_no_initialization( std::vector<tokenizer_it> refs, tokenizer_it where, tokenizer_it end )
{ 
	std::stringstream ss;
	ss << "expression parser compile time error" << std::endl;
	ss << "\tdynamic object do not allow inline initialization" << std::endl;
	for ( auto &ref : refs )
		if ( ref != end )
		{
			ss << "see note: " << std::endl;
			ss << "\tin scope '" << ref->get_block( ) << "' (" << ref->get_position( ).get_row( ) << ")" << std::endl;
		}
	ss << "see note: " << std::endl;
	ss << "\tat line (" << where->get_position( ).get_row( ) << ")" << std::endl;
	format_ = ss.str( );
}

std::string ep::dyanamic_object_no_initialization::pretty_format( ) const
{
	return format_;
}
