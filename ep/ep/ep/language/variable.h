/********************************************************
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*					variable.h					*	*
*  	*			   Created by Yamiez				*	*
*  	*	Do not forget to give credit when using		*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*********************************************************/
#pragma once
#include "all_defines.h"


namespace ep
{

	class variable
	{
	public:
		variable( std::string name, value_reference_ptr reference );
		variable( std::string name, value_reference_ptr reference, bool is_ref, bool is_lvalue, bool is_public );
		variable( std::string name, value_reference_ptr reference, bool is_ref, bool is_lvalue );
		variable( std::string name, value_reference_ptr reference, bool is_ref );
		~variable( );

		const std::string &get_name( ) const;

		std::string &get_name( );

		value_reference_ptr get_ref( );

		void set_ref( value_reference_ptr ref );

		value_reference_ptr assign_ref( value_reference_ptr ref, dispatch_engine_ptr engine );

		void destroy( );

		bool match( variable_ptr other ) const;

		bool is_reference( ) const;

		bool is_result( ) const;

		bool stolen_rvalue( ) const;

		bool is_rvalue( ) const;

		bool is_lvalue( ) const;

		bool is_public( ) const;

		bool is_private( ) const;

		bool is_dynamic( ) const;

		void make_lvalue( );

		void make_rvalue( );

		void make_return( );

		void make_dynamic( );

		void set_name( std::string name );

		value_reference_ptr steal( );

		variable_ptr copy( );

		variable_ptr ref_clone( );

		method_ptr get_method( const std::string &id );

	private:
		bool is_ref_, stolen_, lvalue_, is_ret_, is_public_;
		std::string name_;
		value_reference_ptr vreference_;
		
	};

}