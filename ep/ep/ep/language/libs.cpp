/********************************************************
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*				    libs.cpp					*	*
*  	*			   Created by Yamiez				*	*
*  	*	Do not forget to give credit when using		*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*********************************************************/
#include "libs.h"
#include "../ep.h"
#include <iomanip>
#include <iostream>

namespace ep
{

	value_reference_ptr make_floating( double d, dispatch_engine_ptr &engine )
	{
		return std::make_shared<value_reference>( new double( d ),
												  engine->get_type_info( "floating" ) );
	}

	value_reference_ptr make_string( std::string d, dispatch_engine_ptr &engine )
	{
		return std::make_shared<value_reference>( new std::string( std::move( d ) ),
												  engine->get_type_info( "string_literal" ) );
	}

	void import_bo( dispatch_engine_ptr engine )
	{
		method_argument_list arguments( { std::make_shared<method_argument>( "lhs", true ),
										std::make_shared<method_argument>( "rhs", true ) } );
	
		engine->add_method( std::make_shared<method>( "=", arguments, []( variable_list arguments, dispatch_engine_ptr engine )
		{
			auto a1 = arguments[0], a2 = arguments[1];
			auto a1r = a1->get_ref( ), a2r = a2->get_ref( );
			if ( a2r->is_anonymous_method( ) && a1r->is_uninitalized() )
			{
				a1r->make_anonymous_method( a2r->get_anonymous_method( ) );
				return a1->get_ref();
			}
			a1->set_name( "this" );
			return a1->assign_ref( a2r, std::move( engine ) );
		} ) );

		engine->add_method( std::make_shared<method>( "+=", arguments, []( variable_list arguments, dispatch_engine_ptr engine )
		{
			auto a1 = arguments[0], a2 = arguments[1];
			auto a1r = a1->get_ref( ), a2r = a2->get_ref( );
			a1->set_name( "this" );
			return a1r->type_info( )->get_method( "+=" )->get_fun( )( std::move( arguments ), std::move( engine ) );
		} ) );

		engine->add_method( std::make_shared<method>( "-=", arguments, []( variable_list arguments, dispatch_engine_ptr engine )
		{
			auto a1 = arguments[0], a2 = arguments[1];
			auto a1r = a1->get_ref( ), a2r = a2->get_ref( );
			a1->set_name( "this" );
			return a1r->type_info( )->get_method( "-=" )->get_fun( )( std::move( arguments ), std::move( engine ) );
		} ) );

		engine->add_method( std::make_shared<method>( "*=", arguments, []( variable_list arguments, dispatch_engine_ptr engine )
		{
			auto a1 = arguments[0], a2 = arguments[1];
			auto a1r = a1->get_ref( ), a2r = a2->get_ref( );
			a1->set_name( "this" );
			return a1r->type_info( )->get_method( "*=" )->get_fun( )( std::move( arguments ), std::move( engine ) );
		} ) );

		engine->add_method( std::make_shared<method>( "/=", arguments, []( variable_list arguments, dispatch_engine_ptr engine )
		{
			auto a1 = arguments[0], a2 = arguments[1];
			auto a1r = a1->get_ref( ), a2r = a2->get_ref( );
			a1->set_name( "this" );
			return a1r->type_info( )->get_method( "/=" )->get_fun( )( std::move( arguments ), std::move( engine ) );
		} ) );

		engine->add_method( std::make_shared<method>( "&=", arguments, []( variable_list arguments, dispatch_engine_ptr engine )
		{
			auto a1 = arguments[0], a2 = arguments[1];
			auto a1r = a1->get_ref( ), a2r = a2->get_ref( );
			a1->set_name( "this" );
			return a1r->type_info( )->get_method( "&=" )->get_fun( )( std::move( arguments ), std::move( engine ) );
		} ) );

		engine->add_method( std::make_shared<method>( "|=", arguments, []( variable_list arguments, dispatch_engine_ptr engine )
		{
			auto a1 = arguments[0], a2 = arguments[1];
			auto a1r = a1->get_ref( ), a2r = a2->get_ref( );
			a1->set_name( "this" );
			return a1r->type_info( )->get_method( "|=" )->get_fun( )( std::move( arguments ), std::move( engine ) );
		} ) );

		engine->add_method( std::make_shared<method>( "^=", arguments, []( variable_list arguments, dispatch_engine_ptr engine )
		{
			auto a1 = arguments[0], a2 = arguments[1];
			auto a1r = a1->get_ref( ), a2r = a2->get_ref( );
			a1->set_name( "this" );
			return a1r->type_info( )->get_method( "^=" )->get_fun( )( std::move( arguments ), std::move( engine ) );
		} ) );

		engine->add_method( std::make_shared<method>( "%=", arguments, []( variable_list arguments, dispatch_engine_ptr engine )
		{
			auto a1 = arguments[0], a2 = arguments[1];
			auto a1r = a1->get_ref( ), a2r = a2->get_ref( );
			a1->set_name( "this" );
			return a1r->type_info( )->get_method( "%=" )->get_fun( )( std::move( arguments ), std::move( engine ) );
		} ) );



		engine->add_method( std::make_shared<method>( "+", arguments, []( variable_list arguments, dispatch_engine_ptr engine )
		{
			auto a1 = arguments[0], a2 = arguments[1];
			auto a1r = a1->get_ref( ), a2r = a2->get_ref( );
			a1->set_name( "this" );
			return a1r->type_info( )->get_method( "+" )->get_fun( )( std::move( arguments ), std::move( engine ) );
		} ) );
	
		engine->add_method( std::make_shared<method>( "==", arguments, []( variable_list arguments, dispatch_engine_ptr engine )
		{
			auto a1 = arguments[0], a2 = arguments[1];
			auto a1r = a1->get_ref( ), a2r = a2->get_ref( );
			a1->set_name( "this" );
			return a1r->type_info( )->get_method( "==" )->get_fun( )( std::move( arguments ), std::move( engine ) );
		} ) );
	
		engine->add_method( std::make_shared<method>( "!=", arguments, []( variable_list arguments, dispatch_engine_ptr engine )
		{
			auto a1 = arguments[0], a2 = arguments[1];
			auto a1r = a1->get_ref( ), a2r = a2->get_ref( );
			a1->set_name( "this" );
			return a1r->type_info( )->get_method( "!=" )->get_fun( )( std::move( arguments ), std::move( engine ) );
		} ) );
	
		engine->add_method( std::make_shared<method>( "||", arguments, []( variable_list arguments, dispatch_engine_ptr engine )
		{
			auto a1 = arguments[0], a2 = arguments[1];
			auto a1r = a1->get_ref( ), a2r = a2->get_ref( );
			a1->set_name( "this" );
			return a1r->type_info( )->get_method( "||" )->get_fun( )( std::move( arguments ), std::move( engine ) );
		} ) );
	
		engine->add_method( std::make_shared<method>( "&&", arguments, []( variable_list arguments, dispatch_engine_ptr engine )
		{
			auto a1 = arguments[0], a2 = arguments[1];
			auto a1r = a1->get_ref( ), a2r = a2->get_ref( );
			a1->set_name( "this" );
			return a1r->type_info( )->get_method( "&&" )->get_fun( )( std::move( arguments ), std::move( engine ) );
		} ) );

		engine->add_method( std::make_shared<method>( "<<", arguments, []( variable_list arguments, dispatch_engine_ptr engine )
		{
			auto a1 = arguments[0], a2 = arguments[1];
			auto a1r = a1->get_ref( ), a2r = a2->get_ref( );
			a1->set_name( "this" );
			return a1r->type_info( )->get_method( "<<" )->get_fun( )( std::move( arguments ), std::move( engine ) );
		} ) );

		engine->add_method( std::make_shared<method>( ">>", arguments, []( variable_list arguments, dispatch_engine_ptr engine )
		{
			auto a1 = arguments[0], a2 = arguments[1];
			auto a1r = a1->get_ref( ), a2r = a2->get_ref( );
			a1->set_name( "this" );
			return a1r->type_info( )->get_method( ">>" )->get_fun( )( std::move( arguments ), std::move( engine ) );
		} ) );
	
		engine->add_method( std::make_shared<method>( "-", arguments, []( variable_list arguments, dispatch_engine_ptr engine )
		{
			auto a1 = arguments[0], a2 = arguments[1];
			auto a1r = a1->get_ref( ), a2r = a2->get_ref( );
			a1->set_name( "this" );
			return a1r->type_info( )->get_method( "-" )->get_fun( )( std::move( arguments ), std::move( engine ) );
		} ) );
	
		engine->add_method( std::make_shared<method>( "*", arguments, []( variable_list arguments, dispatch_engine_ptr engine )
		{
			auto a1 = arguments[0], a2 = arguments[1];
			auto a1r = a1->get_ref( ), a2r = a2->get_ref( );
			a1->set_name( "this" );
			return a1r->type_info( )->get_method( "*" )->get_fun( )( std::move( arguments ), std::move( engine ) );
		} ) );
	
		engine->add_method( std::make_shared<method>( "/", arguments, []( variable_list arguments, dispatch_engine_ptr engine )
		{
			auto a1 = arguments[0], a2 = arguments[1];
			auto a1r = a1->get_ref( ), a2r = a2->get_ref( );
			a1->set_name( "this" );
			return a1r->type_info( )->get_method( "/" )->get_fun( )( std::move( arguments ), std::move( engine ) );
		} ) );
	
		engine->add_method( std::make_shared<method>( "&", arguments, []( variable_list arguments, dispatch_engine_ptr engine )
		{
			auto a1 = arguments[0], a2 = arguments[1];
			auto a1r = a1->get_ref( ), a2r = a2->get_ref( );
			a1->set_name( "this" );
			return a1r->type_info( )->get_method( "&" )->get_fun( )( std::move( arguments ), std::move( engine ) );
		} ) );
	
		engine->add_method( std::make_shared<method>( "|", arguments, []( variable_list arguments, dispatch_engine_ptr engine )
		{
			auto a1 = arguments[0], a2 = arguments[1];
			auto a1r = a1->get_ref( ), a2r = a2->get_ref( );
			a1->set_name( "this" );
			return a1r->type_info( )->get_method( "|" )->get_fun( )( std::move( arguments ), std::move( engine ) );
		} ) );
	
		engine->add_method( std::make_shared<method>( "^", arguments, []( variable_list arguments, dispatch_engine_ptr engine )
		{
			auto a1 = arguments[0], a2 = arguments[1];
			auto a1r = a1->get_ref( ), a2r = a2->get_ref( );
			a1->set_name( "this" );
			return a1r->type_info( )->get_method( "^" )->get_fun( )( std::move( arguments ), std::move( engine ) );
		} ) );
	
		engine->add_method( std::make_shared<method>( ">", arguments, []( variable_list arguments, dispatch_engine_ptr engine )
		{
			auto a1 = arguments[0], a2 = arguments[1];
			auto a1r = a1->get_ref( ), a2r = a2->get_ref( );
			a1->set_name( "this" );
			return a1r->type_info( )->get_method( ">" )->get_fun( )( std::move( arguments ), std::move( engine ) );
		} ) );
	
		engine->add_method( std::make_shared<method>( "<", arguments, []( variable_list arguments, dispatch_engine_ptr engine )
		{
			auto a1 = arguments[0], a2 = arguments[1];
			auto a1r = a1->get_ref( ), a2r = a2->get_ref( );
			a1->set_name( "this" );
			return a1r->type_info( )->get_method( "<" )->get_fun( )( std::move( arguments ), std::move( engine ) );
		} ) );
	
		engine->add_method( std::make_shared<method>( "%", arguments, []( variable_list arguments, dispatch_engine_ptr engine )
		{
			auto a1 = arguments[0], a2 = arguments[1];
			auto a1r = a1->get_ref( ), a2r = a2->get_ref( );
			a1->set_name( "this" );
			return a1r->type_info( )->get_method( "%" )->get_fun( )( std::move( arguments ), std::move( engine ) );
		} ) );
	
		engine->add_method( std::make_shared<method>( "!",
											  method_argument_list{ std::make_shared<method_argument>( "x" ) },
											  []( variable_list args, dispatch_engine_ptr engine )
		{
			auto ar = args[0]->get_ref( );
			int value = 0;
			if ( ar->is_integral( ) )
				value = *ar->get_pointer<int>( );
			else if ( ar->is_floating( ) )
				value = *ar->get_pointer<double>( );
			else
				throw conversion_error( nullptr,
										engine->get_method( "!" ),
										ar->type_info( ),
										{ engine->get_type_info( "integral" ) } );
			return std::make_shared<value_reference>( new int( !value ),
													  ar->type_info( ) );
		} ) );

	}
	
	void import_math( dispatch_engine_ptr engine )
	{
		method_argument_list arguments( { std::make_shared<method_argument>( "a" ),
										  std::make_shared<method_argument>( "b" ) } );
	
		engine->add_method( std::make_shared<method>( "MATH_PI", method_argument_list{ }, []( variable_list, dispatch_engine_ptr engine )
		{
			return std::make_shared<value_reference>( new double( 3.14159265358979323846 ), 
													  engine->get_type_info( "floating" ) );
		} ) );
	
		engine->add_method( std::make_shared<method>( "sin",
											  method_argument_list{ std::make_shared<method_argument>( "x" ) },
											  []( variable_list args, dispatch_engine_ptr engine )
		{
			auto value = make_floating( std::sin( *args[0]->get_ref( )->get_pointer<double>( ) ), engine );
			return std::move( value );
		} ) );
	
		engine->add_method( std::make_shared<method>( "cos",
											  method_argument_list{ std::make_shared<method_argument>( "x" ) },
											  []( variable_list args, dispatch_engine_ptr engine )
		{
			auto value = make_floating( std::cos( *args[0]->get_ref( )->get_pointer<double>( ) ), engine );
			return std::move( value );
		} ) );
	
		engine->add_method( std::make_shared<method>( "pow",
											  method_argument_list{ std::make_shared<method_argument>( "x" ),
											  std::make_shared<method_argument>( "am" ) },
											  []( variable_list args, dispatch_engine_ptr engine )
		{
			auto value = make_floating( pow( *args[0]->get_ref( )->get_pointer<double>( ), 
											 *args[1]->get_ref( )->get_pointer<double>( ) ),
										engine );
			return std::move( value );
		} ) );
	
		engine->add_method( std::make_shared<method>( "tan",
											  method_argument_list{ std::make_shared<method_argument>( "x" ) },
											  []( variable_list args, dispatch_engine_ptr engine )
		{
			auto value = make_floating( std::tan( *args[0]->get_ref( )->get_pointer<double>( ) ),
										engine );
			return std::move( value );
		} ) );
	
		engine->add_method( std::make_shared<method>( "acos",
											  method_argument_list{ std::make_shared<method_argument>( "x" ) },
											  []( variable_list args, dispatch_engine_ptr engine )
		{
			auto value = make_floating( std::acos( *args[0]->get_ref( )->get_pointer<double>( ) ),
										engine );
			return std::move( value );
		} ) );
	
		engine->add_method( std::make_shared<method>( "asin",
											  method_argument_list{ std::make_shared<method_argument>( "x" ) },
											  []( variable_list args, dispatch_engine_ptr engine )
		{
			auto value = make_floating( std::asin( *args[0]->get_ref( )->get_pointer<double>( ) ),
										engine );
			return std::move( value );
		} ) );
	
		engine->add_method( std::make_shared<method>( "abs",
											  method_argument_list{ std::make_shared<method_argument>( "x" ) },
											  []( variable_list args, dispatch_engine_ptr engine )
		{
			auto value = make_floating( std::abs( *args[0]->get_ref( )->get_pointer<double>( ) ),
										engine );
			return std::move( value );
		} ) );
	
		engine->add_method( std::make_shared<method>( "hypot",
											  method_argument_list{ std::make_shared<method_argument>( "x" ),
											  std::make_shared<method_argument>( "y" ) },
											  []( variable_list args, dispatch_engine_ptr engine )
		{
			auto value = make_floating( std::hypot( *args[0]->get_ref( )->get_pointer<double>( ), 
													*args[1]->get_ref( )->get_pointer<double>( ) ),
										engine );
			return std::move( value );
		} ) );
	
		engine->add_method( std::make_shared<method>( "sqrt",
											  method_argument_list{ std::make_shared<method_argument>( "x" ) },
											  []( variable_list args, dispatch_engine_ptr engine )
		{
			auto value = make_floating( std::sqrt( *args[0]->get_ref( )->get_pointer<double>( ) ),
										engine );
			return std::move( value );
		} ) );
	
		engine->add_method( std::make_shared<method>( "pow10",
											  method_argument_list{ std::make_shared<method_argument>( "x" ) },
											  []( variable_list args, dispatch_engine_ptr engine )
		{
			auto value = make_floating( std::pow( *args[0]->get_ref( )->get_pointer<double>( ), 10 ),
										engine );
			return std::move( value );
		} ) );
	
		engine->add_method( std::make_shared<method>( "ceil",
											  method_argument_list{ std::make_shared<method_argument>( "x" ) },
											  []( variable_list args, dispatch_engine_ptr engine )
		{
			auto value = make_floating( std::ceil( *args[0]->get_ref( )->get_pointer<double>( ) ),
										engine );
			return std::move( value );
		} ) );
	
		engine->add_method( std::make_shared<method>( "floor",
											  method_argument_list{ std::make_shared<method_argument>( "x" ) },
											  []( variable_list args, dispatch_engine_ptr engine )
		{
			auto value = make_floating( std::floor( *args[0]->get_ref( )->get_pointer<double>( ) ),
										engine );
			return std::move( value );
		} ) );
	
		engine->add_method( std::make_shared<method>( "fabs",
											  method_argument_list{ std::make_shared<method_argument>( "x" ) },
											  []( variable_list args, dispatch_engine_ptr engine )
		{
			auto value = make_floating( std::fabs( *args[0]->get_ref( )->get_pointer<double>( )  ), 
										engine );
			return std::move( value );
		} ) );
	
		engine->add_method( std::make_shared<method>( "atan",
											  method_argument_list{ std::make_shared<method_argument>( "x" ) },
											  []( variable_list args, dispatch_engine_ptr engine )
		{
			auto value = make_floating( std::atan( *args[0]->get_ref( )->get_pointer<double>( ) ),
										engine );
			return std::move( value );
		} ) );
	
		engine->add_method( std::make_shared<method>( "cosh",
											  method_argument_list{ std::make_shared<method_argument>( "x" ) },
											  []( variable_list args, dispatch_engine_ptr engine )
		{
			auto value = make_floating( std::cosh( *args[0]->get_ref( )->get_pointer<double>( ) ),
										engine );
			return std::move( value );
		} ) );
	
		engine->add_method( std::make_shared<method>( "sinh",
											  method_argument_list{ std::make_shared<method_argument>( "x" ) },
											  []( variable_list args, dispatch_engine_ptr engine )
		{
			auto value = make_floating( std::sinh( *args[0]->get_ref( )->get_pointer<double>( ) ),
										engine );
			return std::move( value );
		} ) );
	
		engine->add_method( std::make_shared<method>( "tanh",
											  method_argument_list{ std::make_shared<method_argument>( "x" ) },
											  []( variable_list args, dispatch_engine_ptr engine )
		{
			auto value = make_floating( std::tanh( *args[0]->get_ref( )->get_pointer<double>( ) ),
										engine );
			return std::move( value );
		} ) );
	
		engine->add_method( std::make_shared<method>( "exp",
											  method_argument_list{ std::make_shared<method_argument>( "x" ) },
											  []( variable_list args, dispatch_engine_ptr engine )
		{
			auto value = make_floating( std::exp( *args[0]->get_ref( )->get_pointer<double>( ) ),
										engine );
			return std::move( value );
		} ) );
	
		engine->add_method( std::make_shared<method>( "log",
											  method_argument_list{ std::make_shared<method_argument>( "x" ) },
											  []( variable_list args, dispatch_engine_ptr engine )
		{
			auto value = make_floating( std::log( *args[0]->get_ref( )->get_pointer<double>( ) ),
										engine );
			return std::move( value );
		} ) );
	
		engine->add_method( std::make_shared<method>( "log10",
											  method_argument_list{ std::make_shared<method_argument>( "x" ) },
											  []( variable_list args, dispatch_engine_ptr engine )
		{
			auto value = make_floating( std::log10( *args[0]->get_ref( )->get_pointer<double>( ) ),
										engine );
			return std::move( value );
		} ) );
	
		engine->add_method( std::make_shared<method>( "fmod", arguments, []( variable_list args, dispatch_engine_ptr engine )
		{
			auto value = make_floating( std::fmod( *args[0]->get_ref( )->get_pointer<double>( ),
												   *args[1]->get_ref( )->get_pointer<double>( ) ),
										engine );
			return std::move( value );
		} ) );
	}
	

	void import_vector( dispatch_engine_ptr engine )
	{ 
		// Vector functions
		method_list vector_methods, iterator_methods;
		
		vector_methods.emplace_back( std::make_shared<method>( "emplace_back",
													method_argument_list{ std::make_shared<ep::method_argument>( "element", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
		{
			auto ar = args[0]->get_ref( );
			auto e = args[1];
			auto vec = ar->get_pointer<std::vector<variable_ptr>>( );
			vec->emplace_back( std::make_shared<variable>( "", e->get_ref( )->clone( ) ) );
			return ep::void_return;
		} ) );

		vector_methods.emplace_back( std::make_shared<method>( "erase",
													method_argument_list{ std::make_shared<ep::method_argument>( "it", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
		{
			auto ar = args[0]->get_ref( );
			auto itr = args[1]->get_ref( );
			auto it = itr->get_pointer<std::vector<variable_ptr>::iterator>( );
			auto vec = ar->get_pointer<std::vector<variable_ptr>>( );
			auto vref = std::make_shared<value_reference>( new std::vector<variable_ptr>::iterator( vec->erase( *it ) ),
														   engine->get_type_info( "vector::iterator" ) );
			return std::move( vref );
		} ) );

		vector_methods.emplace_back( std::make_shared<method>( "begin",
													method_argument_list{ },
													[]( variable_list args, dispatch_engine_ptr engine )
		{
			auto ar = args[0]->get_ref( );
			auto vec = ar->get_pointer<std::vector<variable_ptr>>( );
			auto vref = std::make_shared<value_reference>( new std::vector<variable_ptr>::iterator( vec->begin( ) ),
														   engine->get_type_info( "vector::iterator" ) );
			return std::move( vref );
		} ) );

		vector_methods.emplace_back( std::make_shared<method>( "end",
													method_argument_list{ },
													[]( variable_list args, dispatch_engine_ptr engine )
		{
			auto ar = args[0]->get_ref( );
			auto vec = ar->get_pointer<std::vector<variable_ptr>>( );
			auto vref = std::make_shared<value_reference>( new std::vector<variable_ptr>::iterator( vec->end( ) ), 
														   engine->get_type_info( "vector::iterator" ) );
			return std::move( vref );
		} ) );

		vector_methods.emplace_back( std::make_shared<method>( "for_each",
													method_argument_list{ std::make_shared<ep::method_argument>( "method", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
		{
			auto ar = args[0]->get_ref( );
			auto method = engine->get_method( *args[1]->get_ref( )->get_pointer<std::string>( ) );
			if ( method->get_arguments_size( ) != 2 )
				throw vector_for_each_error( method );

			auto vec = ar->get_pointer<std::vector<variable_ptr>>( );
			for ( auto it = vec->begin( ), end = vec->end( );
				  it < end;
				  ++it )
			{
				auto &x = *it;
				auto ptr = x->get_ref( );
				variable_list variables;
				variables.emplace_back( std::make_shared<variable>( "it", std::make_shared<value_reference>( &it,
																											 engine->get_type_info( "vector::iterator" ),
																											 false ),
																	true ) );
				variables.emplace_back( std::make_shared<variable>( "elem", ptr, true ) );
				method->invoke( std::move( variables ), engine );
			}
			return ep::void_return;
		} ) );

		vector_methods.emplace_back( std::make_shared<method>( "pop_back",
													method_argument_list{ },
													[]( variable_list args, dispatch_engine_ptr engine )
		{
			auto ar = args[0]->get_ref( );
			auto vec = ar->get_pointer<std::vector<variable_ptr>>( );
			vec->pop_back( );
			return ep::void_return;
		} ) );

		vector_methods.emplace_back( std::make_shared<method>( "clear",
													method_argument_list{ },
													[]( variable_list args, dispatch_engine_ptr engine )
		{
			auto ar = args[0]->get_ref( );
			auto vec = ar->get_pointer<std::vector<variable_ptr>>( );
			vec->clear( );
			return ep::void_return;
		} ) );

		vector_methods.emplace_back( std::make_shared<method>( "empty",
													method_argument_list{ },
													[]( variable_list args, dispatch_engine_ptr engine )
		{
			auto ar = args[0]->get_ref( );
			auto vec = ar->get_pointer<std::vector<variable_ptr>>( );
			return std::move( std::make_shared<value_reference>( new int( vec->empty( ) ),
																 engine->get_type_info( "integral" ) ) );
		} ) );

		vector_methods.emplace_back( std::make_shared<method>( "size",
													method_argument_list{ },
													[]( variable_list args, dispatch_engine_ptr engine )
		{
			auto ar = args[0]->get_ref( );
			auto vec = ar->get_pointer<std::vector<variable_ptr>>( );
			return std::move( std::make_shared<value_reference>( new int( vec->size( ) ),
																 engine->get_type_info( "integral" ) ) );
		} ) );

		vector_methods.emplace_back( std::make_shared<method>( "capacity",
													method_argument_list{ },
													[]( variable_list args, dispatch_engine_ptr engine )
		{
			auto ar = args[0]->get_ref( );
			auto vec = ar->get_pointer<std::vector<variable_ptr>>( );
			return std::move( std::make_shared<value_reference>( new int( vec->capacity( ) ),
																 engine->get_type_info( "integral" ) ) );
		} ) );

		engine->add_type( std::make_shared<type_info>( "vector",
													   []( value_reference *ptr )
		{
			auto vec = ptr->get_pointer<std::vector<variable_ptr>>( );
			delete vec;
		},
													   []( value_reference *ptr )
		{
			auto vec = ptr->get_pointer<std::vector<variable_ptr>>( );
			auto nvec = new std::vector<variable_ptr>( );
			for ( auto &x : *vec )
				if ( x->is_reference( ) )
					nvec->emplace_back( x );
				else
					nvec->emplace_back( new variable( "", x->get_ref( )->clone( ), false, true ) );
			return std::make_shared<value_reference>( nvec, ptr->type_info( ) );

		},
													   std::move( vector_methods ) ) );



		// vector::iterator
		iterator_methods.emplace_back( std::make_shared<method>( "<",
													method_argument_list{ std::make_shared<ep::method_argument>( "it1", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
		{
			auto ar_it1 = args[0]->get_ref( ),
				ar_it2 = args[1]->get_ref( );
			auto it1 = *ar_it1->get_pointer<std::vector<variable_ptr>::iterator>( ),
				it2 = *ar_it2->get_pointer<std::vector<variable_ptr>::iterator>( );
			auto vref = std::make_shared<value_reference>( new int( it1 < it2 ),
														   engine->get_type_info( "integral" ) );
			return std::move( std::move( vref ) );
		} ) );

		iterator_methods.emplace_back( std::make_shared<method>( ">",
													method_argument_list{ std::make_shared<ep::method_argument>( "it1", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
		{
			auto ar_it1 = args[0]->get_ref( ),
				ar_it2 = args[1]->get_ref( );
			auto it1 = *ar_it1->get_pointer<std::vector<variable_ptr>::iterator>( ),
				it2 = *ar_it2->get_pointer<std::vector<variable_ptr>::iterator>( );
			auto vref = std::make_shared<value_reference>( new int( it1 > it2 ),
														   engine->get_type_info( "integral" ) );
			return std::move( std::move( vref ) );
		} ) );

		iterator_methods.emplace_back( std::make_shared<method>( "increment",
													method_argument_list{ },
													[]( variable_list args, dispatch_engine_ptr engine )
		{
			auto ar_it = args[0]->get_ref( );
			auto it = ar_it->get_pointer<std::vector<variable_ptr>::iterator>( );
			it->operator++( );
			return void_return;
		} ) );

		iterator_methods.emplace_back( std::make_shared<method>( "decrement",
													method_argument_list{ },
													[]( variable_list args, dispatch_engine_ptr engine )
		{
			auto ar_it = args[0]->get_ref( );
			auto it = ar_it->get_pointer<std::vector<variable_ptr>::iterator>( );
			it->operator--( );
			return void_return;
		} ) );

		iterator_methods.emplace_back( std::make_shared<method>( "access",
													method_argument_list{ },
													[]( variable_list args, dispatch_engine_ptr engine )
		{
			auto ar_it = args[0]->get_ref( );
			auto it = *ar_it->get_pointer<std::vector<variable_ptr>::iterator>( );
			return ( *it )->get_ref( );
		} ) );


		engine->add_type( std::make_shared<type_info>( "vector::iterator",
													   []( value_reference *ptr )
		{
			delete ptr->get_pointer<std::vector<variable_ptr>::iterator>( );
		},
													   []( value_reference *ptr )
		{
			auto it = ptr->get_pointer<std::vector<variable_ptr>::iterator>( );
			return std::make_shared<value_reference>( new std::vector<variable_ptr>::iterator( *it ), 
													  ptr->type_info( ) );
		},
													   std::move( iterator_methods ) ) );

		engine->add_method( std::make_shared<method>( "vector",
													  method_argument_list{ },
													  []( variable_list, dispatch_engine_ptr engine )
		{ 
			auto vref = std::make_shared<value_reference>( new std::vector<variable_ptr>( ), 
														   engine->get_type_info( "vector" ) );
			return std::move( std::move( vref ) );
		} ) );
		
	}

	bool is_std_bo( const std::string & name )
	{
		return name == "+" || name == "==" || name == "!=" ||
			name == "||" || name == "&&" || name == "*" ||
			name == "-" || name == "/"  || name == "|" ||
			name == "!" || name == "^" || name == ">" ||
			name == "<" || name == "%"  || name == "<<" || 
			name == ">>";
	}

	void indent( std::ostream &stream, int amount )
	{
		for ( auto i = 0;
			  i < amount;
			  ++i )
			stream << '\t';
	}
	
	struct _Indent
	{
		int amount;
	};

	std::ostream &operator<<( std::ostream &str, const _Indent &ind )
	{
		indent( str, ind.amount );
		return str;
	}

	void code_output( ep::statement_list &block, std::ostream &out, int recursion_level );
	void code_output_new_scope( ep::statement_list &block, std::ostream &out, int recursion_level );

	void code_output_new_scope( ep::statement_list &block, std::ostream &out, int recursion_level )
	{
		indent( out, recursion_level - 1 );
		out << "{" << std::endl;
		code_output( block, out, recursion_level );
		indent( out, recursion_level - 1 );
		out << "}" << std::endl;
	}


	void code_output( ep::statement_list &block, std::ostream &out, int recursion_level )
	{
		for ( auto &x : block )
		{
			indent( out, recursion_level );
			if ( x->is_if( ) )
			{
				out << "if " << x->get_expression( )->pretty_format( ) << std::endl;
				code_output_new_scope( x->get_dups( ), out, recursion_level + 1 );
			}
			else if ( x->is_else_if( ) )
			{
				out << "else if " << x->get_expression( )->pretty_format( ) << std::endl;
				code_output_new_scope( x->get_dups( ), out, recursion_level + 1 );
			}
			else if ( x->is_else( ) )
			{
				out << "else" << std::endl;
				code_output_new_scope( x->get_dups( ), out, recursion_level + 1 );
			}
			else if ( x->is_while( ) )
			{
				out << "while " << x->get_expression( )->pretty_format( ) << std::endl;
				code_output_new_scope( x->get_dups( ), out, recursion_level + 1 );
			}
			else if ( x->is_var_decl( ) )
				out << "var " << x->get_var_decl( ) << ";\n";
			else if ( x->is_var_decl_initalizer( ) )
				out << "auto " << x->get_var_decl( ) << " = " << x->get_expression( )->pretty_format( ) << "\n";
			else if ( x->is_function_call( ) || x->is_expression( ) || x->is_empty_expression( ) )
				out << x->get_expression( )->pretty_format( ) << "\n";
			else if ( x->is_return( ) )
				out << "return " << x->get_expression( )->pretty_format( ) << "\n";
			else if ( x->is_break_statement( ) )
				out << "break;\n";
			else if ( x->is_continue_statement( ) )
				out << "continue;\n";
			else
				out << "<unintepretable code block>\n";
		}
	}

	void dump_method( std::ostream &stream, int rec, method_ptr v, bool anon = false )
	{
		auto name = v->get_name( );
		v->get_name( ) = "";
		stream << _Indent{ rec } << "method " << v->pretty_format( ) << "\n" <<
			_Indent{ rec } << "{";
		if ( v->get_statements( ).size( ) == 0 )
		{
			stream << " }\n";
		}
		else
		{
			stream << "\n";
			code_output( v->get_statements( ), stream, rec + 1 );
			stream << _Indent{ rec } << "}\n";
		}
		v->get_name( ) = name;
	}

	void dump_var( std::ostream &stream, int rec, variable_ptr v )
	{
		auto ref = v->get_ref( );
		indent( stream, rec );
		stream << "[" <<
			( v->is_dynamic( ) ? "dynamic [size= " + std::to_string( ref->get_members( ).size( ) ) + "]]" :
			  ( ref->is_anonymous_method( ) ? "anonymous method]" :
			    ref->type_info( )->get_name( ) + (v->is_rvalue() ? "&&" : "") + " [size=" + std::to_string( ref->type_info( )->get_members( ).size( ) ) + "]]" ) ) <<
			"\n" << _Indent{ rec } << "{";
		if ( ref->is_anonymous_method( ) )
		{
			stream << "\n";
			dump_method( stream, rec + 1, ref->get_anonymous_method( ), true );
			stream << _Indent{ rec } << "}\n";
		}
		else if ( ref->get_members( ).empty( ) )
		{
			stream << " }\n";
		}
		else
		{
			stream << "\n";
			for ( auto &x : ref->get_members( ) )
				dump_var( stream, rec + 1, x );
			stream << _Indent{ rec } << "}\n";
		}
	}

	void import_ios( dispatch_engine_ptr engine )
	{
		// TODO:
		//	resign ios library to be oop
		method_list fstream_methods, istream_methods, ostream_methods;
	
		engine->add_method( std::make_shared<method>( "print_methods",
											  method_argument_list{ },
											  []( variable_list, dispatch_engine_ptr engine )
		{
			std::cout << "-->> Method table:" << std::endl;
			for ( auto &x : engine->get_methods( ) )
				std::cout << "---->> " << x->pretty_format( ) << std::endl;
			return void_return;
		} ) );

		engine->add_method( std::make_shared<method>( "print_types",
													  method_argument_list{ },
													  []( variable_list, dispatch_engine_ptr engine )
		{
			std::cout << "-->> Type system:" << std::endl;
			for ( auto &x : engine->get_type_infos( ) )
			{
				std::cout << "---->> " << x->get_name( ) << std::endl;
				try
				{
					std::cout << "-------->> " << x->get_name( ) << "::" << engine->get_method( x->get_name( ) )->pretty_format( ) << "" << std::endl;
				} catch ( ep::ep_error & ){ }
				std::cout << "-------->> " << x->get_name( ) << "::~" << x->get_name( ) << "( )" << std::endl;
				for ( auto &j : x->get_methods( ) )
					std::cout << "-------->> " << x->get_name( ) << "::" << j->pretty_format( ) << std::endl;
				std::cout << std::endl;
			}
			return void_return;
		} ) );
	
		// fstream function



	
		// IO FUNCTIONS
		engine->add_method( std::make_shared<method>( "fstream",
											  method_argument_list{ std::make_shared<method_argument>( "fname", true ),
											  std::make_shared<method_argument>( "ftype", true ) },
											  []( variable_list args, dispatch_engine_ptr engine )
		{
			auto name = args[0]->get_ref( );
			auto type = args[1]->get_ref( );
			if ( !name->is_string( ) )
				throw conversion_error( engine->get_type_info( "fstream" ),
										engine->get_type_info( "fstream" )->get_method( "fstream" ),
										name->type_info( ),
										{ engine->get_type_info( "string_literal" ) } );
			if ( !type->is_integral( ) )
				throw conversion_error( engine->get_type_info( "fstream" ),
										engine->get_type_info( "fstream" )->get_method( "putln" ),
										type->type_info( ),
										{ engine->get_type_info( "integral" ) } );
			auto file = new std::fstream( *name->get_pointer<std::string>( ), *type->get_pointer<size_t>( ) );
			return std::make_shared<value_reference>( file, engine->get_type_info( "fstream" ) );
		} ) );
	
		fstream_methods.emplace_back( std::make_shared<method>( "close",
											  method_argument_list{ },
											  []( variable_list args, dispatch_engine_ptr engine )
		{
			auto handle = args[0]->get_ref( );
			auto file = handle->get_pointer<std::fstream>( );
			file->close( );
			return void_return;
		} ) );
	
		fstream_methods.emplace_back( std::make_shared<method>( "eof",
											  method_argument_list{ },
											  []( variable_list args, dispatch_engine_ptr engine )
		{
			auto handle = args[0]->get_ref( );
			auto file = handle->get_pointer<std::fstream>( );
			return std::make_shared<value_reference>( new int( file->eof( ) ),
													  engine->get_type_info( "integral" ) );
		} ) );
	
		fstream_methods.emplace_back( std::make_shared<method>( "getln",
											  method_argument_list{ },
											  []( variable_list args, dispatch_engine_ptr engine )
		{
			auto handle = args[0]->get_ref( );
			auto file = handle->get_pointer<std::fstream>( );
			std::string output;
			std::getline( *file, output );
			return std::make_shared<value_reference>( new std::string( std::move( output ) ), 
													  engine->get_type_info( "string_literal" ) );
		} ) );

		fstream_methods.emplace_back( std::make_shared<method>( "putln",
																method_argument_list{ std::make_shared<method_argument>( "input", true ) },
																[]( variable_list args, dispatch_engine_ptr engine )
		{
			auto handle = args[0]->get_ref( );
			auto input = args[1]->get_ref( );
			auto file = handle->get_pointer<std::fstream>( );
			if ( input->is_string( ) )
				*file << *input->get_pointer<std::string>( );
			else if ( input->is_integral( ) )
				*file << *input->get_pointer<int>( );
			else if ( input->type_info( )->get_name( ) == "std::endl" )
				*file << std::endl;
			else
				throw conversion_error( engine->get_type_info( "fstream" ),
										engine->get_type_info( "fstream" )->get_method( "putln" ),
										input->type_info( ),
										{ engine->get_type_info( "integral" ), engine->get_type_info( "floating" ), engine->get_type_info( "string_literal" ) } );
			return std::move( handle );
		} ) );

		fstream_methods.emplace_back( std::make_shared<method>( "put",
																method_argument_list{ std::make_shared<method_argument>( "input", true ) },
																[]( variable_list args, dispatch_engine_ptr engine )
		{
			auto handle = args[0]->get_ref( );
			auto input = args[1]->get_ref( );
			auto file = handle->get_pointer<std::fstream>( );
			if ( input->is_string( ) )
				*file << *input->get_pointer<std::string>( );
			else if ( input->is_integral( ) )
				*file << *input->get_pointer<int>( );
			else if ( input->type_info( )->get_name( ) == "std::endl" )
				*file << std::endl;
			else
				throw conversion_error( engine->get_type_info( "fstream" ),
										engine->get_type_info( "fstream" )->get_method( "put" ),
										input->type_info( ),
										{ engine->get_type_info( "integral" ), engine->get_type_info( "floating" ), engine->get_type_info( "string_literal" ) } );
			return std::move( handle );
		} ) );

		fstream_methods.emplace_back( std::make_shared<method>( "<<",
																method_argument_list{ std::make_shared<method_argument>( "input", true ) },
																[]( variable_list args, dispatch_engine_ptr engine )
		{
			auto handle = args[0]->get_ref( );
			auto input = args[1]->get_ref( );
			auto file = handle->get_pointer<std::fstream>( );
			if ( input->is_dynamic( ) || input->is_anonymous_method( ) )
				dump_var( *file, 0, args[1] );
			else if ( input->is_string( ) )
				*file << *input->get_pointer<std::string>( );
			else if ( input->is_integral( ) )
				*file << *input->get_pointer<int>( );
			else if ( input->is_floating( ) )
				*file << *input->get_pointer<double>( );
			else if ( input->type_info( )->get_name( ) == "std::endl" )
				*file << std::endl;
			else
				dump_var( *file, 0, args[1] );
			return std::move( handle );
		} ) );

		fstream_methods.emplace_back( std::make_shared<method>( ">>",
																method_argument_list{ std::make_shared<method_argument>( "output", true ) },
																[]( variable_list args, dispatch_engine_ptr engine )
		{
			auto handle = args[0]->get_ref( );
			auto output = args[1]->get_ref( );
			auto file = handle->get_pointer<std::fstream>( );
			if ( output->is_string( ) )
				*file >> *output->get_pointer<std::string>( );
			else if ( output->is_integral( ) )
				*file >> *output->get_pointer<int>( );
			else if ( output->is_floating( ) )
				*file >> *output->get_pointer<double>( );
			else
				throw conversion_error( engine->get_type_info( "fstream" ),
										engine->get_type_info( "fstream" )->get_method( ">>" ),
										output->type_info( ),
										{ engine->get_type_info( "integral" ), engine->get_type_info( "floating" ), engine->get_type_info( "string_literal" ) } );
			return std::move( handle );
		} ) );

		engine->add_type( std::make_shared<type_info>( "fstream",
													   []( value_reference *ptr )
		{
			delete ptr->get_pointer<void>( );
		},
						  []( value_reference * ptr )
		{
			return std::make_shared<value_reference>( ptr->get_pointer<void>( ), ptr->type_info( ) );
		},
			std::move( fstream_methods ) ) );



		// OSTREAM
		ostream_methods.emplace_back( std::make_shared<method>( "putln",
																method_argument_list{ std::make_shared<method_argument>( "input", true ) },
																[]( variable_list args, dispatch_engine_ptr engine )
		{
			auto handle = args[0]->get_ref( );
			auto input = args[1]->get_ref( );
			auto file = handle->get_pointer<std::ostream>( );
			if ( input->is_string( ) )
				*file << *input->get_pointer<std::string>( ) << std::endl;
			else if ( input->is_integral( ) )
				*file << *input->get_pointer<int>( ) << std::endl;
			else if ( input->is_floating( ) )
				*file << *input->get_pointer<double>( ) << std::endl;
			else
				throw conversion_error( engine->get_type_info( "ostream" ),
										engine->get_type_info( "ostream" )->get_method( "putln" ),
										input->type_info( ),
										{ engine->get_type_info( "integral" ), engine->get_type_info( "floating" ), engine->get_type_info( "string_literal" ) } );
			return void_return;
		} ) );

		ostream_methods.emplace_back( std::make_shared<method>( "put",
																method_argument_list{ std::make_shared<method_argument>( "input", true ) },
																[]( variable_list args, dispatch_engine_ptr engine )
		{
			auto handle = args[0]->get_ref( );
			auto input = args[1]->get_ref( );
			auto file = handle->get_pointer<std::ostream>( );
			if ( input->is_string( ) )
				*file << *input->get_pointer<std::string>( );
			else if ( input->is_integral( ) )
				*file << *input->get_pointer<int>( );
			else if ( input->is_floating( ) )
				*file << *input->get_pointer<double>( );
			else
				throw conversion_error( engine->get_type_info( "ostream" ),
										engine->get_type_info( "ostream" )->get_method( "put" ),
										input->type_info( ),
										{ engine->get_type_info( "integral" ), engine->get_type_info( "floating" ), engine->get_type_info( "string_literal" ) } );
			return void_return;
		} ) );

		ostream_methods.emplace_back( std::make_shared<method>( "<<",
																method_argument_list{ std::make_shared<method_argument>( "input", true ) },
																[]( variable_list args, dispatch_engine_ptr engine )
		{
			auto handle = args[0]->get_ref( );
			auto input = args[1]->get_ref( );
			auto file = handle->get_pointer<std::ostream>( );
			if ( input->is_dynamic( ) || input->is_anonymous_method( ) )
				dump_var( *file, 0, args[1] );
			else if ( input->is_string( ) )
				*file << *input->get_pointer<std::string>( );
			else if ( input->is_integral( ) )
				*file << *input->get_pointer<int>( );
			else if ( input->is_floating( ) )
				*file << *input->get_pointer<double>( );
			else if ( input->type_info( )->get_name( ) == "std::endl" )
				*file << std::endl;
			else
				dump_var( *file, 0, args[1] );
			return std::move( handle );
		} ) );

		engine->add_type( std::make_shared<type_info>( "ostream",
													   []( value_reference *ptr )
		{
			
		},
													   []( value_reference * ptr )
		{
			return std::make_shared<value_reference>( ptr->get_pointer<void>( ), ptr->type_info( ) );
		},
			std::move( ostream_methods ) ) );


		// istream

		istream_methods.emplace_back( std::make_shared<method>( "getln",
																method_argument_list{ },
																[]( variable_list args, dispatch_engine_ptr engine )
		{
			auto handle = args[0]->get_ref( );
			auto file = handle->get_pointer<std::istream>( );
			std::string output;
			std::getline( *file, output );
			return std::make_shared<value_reference>( new std::string( std::move( output ) ),
													  engine->get_type_info( "string_literal" ) );
		} ) );

		istream_methods.emplace_back( std::make_shared<method>( ">>",
																method_argument_list{ std::make_shared<method_argument>( "output", true ) },
																[]( variable_list args, dispatch_engine_ptr engine )
		{
			auto handle = args[0]->get_ref( );
			auto output = args[1]->get_ref( );
			auto file = handle->get_pointer<std::fstream>( );
			if ( output->is_string( ) )
				*file >> *output->get_pointer<std::string>( );
			else if ( output->is_integral( ) )
				*file >> *output->get_pointer<int>( );
			else if ( output->is_floating( ) )
				*file >> *output->get_pointer<double>( );
			else
				throw conversion_error( engine->get_type_info( "istream" ),
										engine->get_type_info( "istream" )->get_method( ">>" ),
										output->type_info( ),
										{ engine->get_type_info( "integral" ), engine->get_type_info( "floating" ), engine->get_type_info( "string_literal" ) } );
			return std::move( handle );
		} ) );

		engine->add_type( std::make_shared<type_info>( "istream",
													   []( value_reference *ptr )
		{
		},
													   []( value_reference * ptr )
		{
			return std::make_shared<value_reference>( ptr->get_pointer<void>( ), ptr->type_info( ) );
		},
			std::move( istream_methods ) ) );

		engine->add_type( std::make_shared<type_info>( "std::endl",
													   []( value_reference *ptr )
		{
		},
													   []( value_reference * ptr )
		{
			return std::make_shared<value_reference>( nullptr, ptr->type_info( ) );
		} ) );
	
		engine->add_variable( std::make_shared<variable>( "cin", std::make_shared<value_reference>( &std::cin, engine->get_type_info( "istream" ) ) ) );
		engine->add_variable( std::make_shared<variable>( "cout", std::make_shared<value_reference>( &std::cout, engine->get_type_info( "ostream" ) ) ) );
		engine->add_variable( std::make_shared<variable>( "ios_binary", std::make_shared<value_reference>( new int( std::ios::binary ), engine->get_type_info( "integral" ) ) ) );
		engine->add_variable( std::make_shared<variable>( "ios_in", std::make_shared<value_reference>( new int( std::ios::in ), engine->get_type_info( "integral" ) ) ) );
		engine->add_variable( std::make_shared<variable>( "ios_out", std::make_shared<value_reference>( new int( std::ios::out ), engine->get_type_info( "integral" ) ) ) );
		engine->add_variable( std::make_shared<variable>( "ios_app", std::make_shared<value_reference>( new int( std::ios::app ), engine->get_type_info( "integral" ) ) ) );
		engine->add_variable( std::make_shared<variable>( "ios_trunc", std::make_shared<value_reference>( new int( std::ios::trunc ), engine->get_type_info( "integral" ) ) ) );
		engine->add_variable( std::make_shared<variable>( "endl", std::make_shared<value_reference>( nullptr, engine->get_type_info( "std::endl" ) ) ) );
	}

}