/********************************************************
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*			  expression_action.h				*	*
*  	*			   Created by Yamiez				*	*
*  	*	Do not forget to give credit when using		*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*********************************************************/
#pragma once
#include "all_defines.h"

namespace ep
{

	class expression_action
	{
	public:
		//expression_action( double operand );
		//expression_action( std::string value );
		expression_action( value_reference_ptr value );
		expression_action( std::string name, bool is_method );

		value_reference_ptr get_ref( );

		const std::string &get_ref_name( ) const;

		std::string &get_ref_name( );

		bool is_variable( ) const;
		
		bool is_ref( ) const;

		bool is_method( ) const;

	private:
		value_reference_ptr ref_;
		std::string id_;
		bool method_;
	};

}