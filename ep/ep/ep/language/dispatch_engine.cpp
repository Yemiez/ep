/********************************************************
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*			  dispatch_engine.cpp				*	*
*  	*			   Created by Yamiez				*	*
*  	*	Do not forget to give credit when using		*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*********************************************************/
#include "../ep.h"
#include <iomanip>
#include <iostream>

ep::dispatch_engine::dispatch_engine( )
	: methods_( ), state_( nullptr ), dbg_( nullptr ),
	tis_( )
{
	state_ = create_new_dispatch_state( "@@GLOBAL", nullptr );

	add_method( ep::make_method( "exit", 
								 []( )
	{
		throw exit_exception( );
		return ep::void_return;
	} ) );

	auto integral = std::make_shared<type_info>( "integral",
												 []( value_reference *ptr )
	{
		delete ptr->get_pointer<int>( );
	},
												 []( value_reference *ptr )
	{
		return std::make_shared<value_reference>( new int( *ptr->get_pointer<int>( ) ), ptr->type_info( ) );
	} );

	integral->add_method( std::make_shared<method>( "=",
													method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );

		if ( ar->get_pointer<void>() != nullptr )
		{
			if ( ar->type_info( )->bare_equal( *rhs->type_info( ) ) )
				*ar->get_pointer<int>( ) = *rhs->get_pointer<int>( );
			else if ( rhs->type_info( )->get_name( ) == "floating" )
				*ar->get_pointer<int>( ) = *rhs->get_pointer<double>( );
			else
				throw conversion_error( engine->get_type_info( "integral" ),
										engine->get_type_info( "integral" )->get_method( "=" ),
										rhs->type_info( ),
										{ engine->get_type_info( "integral" ) } );
		}
		else
		{
			if ( ar->type_info( )->bare_equal( *rhs->type_info( ) ) )
				ar->set_pointer( new int( *rhs->get_pointer<int>( ) ) );
			else if ( rhs->type_info( )->get_name( ) == "floating" )
				ar->set_pointer( new int( *rhs->get_pointer<double>( ) ) );
			else
				throw conversion_error( engine->get_type_info( "integral" ),
										engine->get_type_info( "integral" )->get_method( "=" ),
										rhs->type_info( ),
										{ engine->get_type_info( "integral" ) } );

		}
		return ep::void_return;
	} ) );

	integral->add_method( std::make_shared<method>( "+",
													method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new int( *ar->get_pointer<int>( ) + *rhs->get_pointer<int>( ) ), ar->type_info( ) );
		return std::move( vref );
	} ) );

	integral->add_method( std::make_shared<method>( "-",
													method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new int( *ar->get_pointer<int>( ) - *rhs->get_pointer<int>( ) ), ar->type_info( ) );
		return std::move( vref );
	} ) );

	integral->add_method( std::make_shared<method>( "*",
													method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new int( *ar->get_pointer<int>( ) * *rhs->get_pointer<int>( ) ), ar->type_info( ) );
		return std::move( vref );
	} ) );

	integral->add_method( std::make_shared<method>( "/",
													method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new int( *ar->get_pointer<int>( ) / *rhs->get_pointer<int>( ) ), ar->type_info( ) );
		return std::move( vref );
	} ) );

	integral->add_method( std::make_shared<method>( "%",
													method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new int( *ar->get_pointer<int>( ) % *rhs->get_pointer<int>( ) ), ar->type_info( ) );
		return std::move( vref );
	} ) );

	integral->add_method( std::make_shared<method>( "&",
													method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new int( *ar->get_pointer<int>( ) & *rhs->get_pointer<int>( ) ), ar->type_info( ) );
		return std::move( vref );
	} ) );

	integral->add_method( std::make_shared<method>( "|",
													method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new int( *ar->get_pointer<int>( ) | *rhs->get_pointer<int>( ) ), ar->type_info( ) );
		return std::move( vref );
	} ) );

	integral->add_method( std::make_shared<method>( "&&",
													method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new int( *ar->get_pointer<int>( ) && *rhs->get_pointer<int>( ) ), ar->type_info( ) );
		return std::move( vref );
	} ) );

	integral->add_method( std::make_shared<method>( "||",
													method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new int( *ar->get_pointer<int>( ) || *rhs->get_pointer<int>( ) ), ar->type_info( ) );
		return std::move( vref );
	} ) );

	integral->add_method( std::make_shared<method>( "==",
													method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new int( *ar->get_pointer<int>( ) == *rhs->get_pointer<int>( ) ), ar->type_info( ) );
		return std::move( vref );
	} ) );


	integral->add_method( std::make_shared<method>( "!=",
													method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new int( *ar->get_pointer<int>( ) / *rhs->get_pointer<int>( ) ), ar->type_info( ) );
		return std::move( vref );
	} ) );

	integral->add_method( std::make_shared<method>( ">",
													method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new int( *ar->get_pointer<int>( ) > *rhs->get_pointer<int>( ) ), ar->type_info( ) );
		return std::move( vref );
	} ) );

	integral->add_method( std::make_shared<method>( "<",
													method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new int( *ar->get_pointer<int>( ) < *rhs->get_pointer<int>( ) ), ar->type_info( ) );
		return std::move( vref );
	} ) );

	integral->add_method( std::make_shared<method>( ">=",
													method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new int( *ar->get_pointer<int>( ) >= *rhs->get_pointer<int>( ) ), ar->type_info( ) );
		return std::move( vref );
	} ) );

	integral->add_method( std::make_shared<method>( "<=",
													method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new int( *ar->get_pointer<int>( ) <= *rhs->get_pointer<int>( ) ), ar->type_info( ) );
		return std::move( vref );
	} ) );


	integral->add_method( std::make_shared<method>( "~",
													method_argument_list{ },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new int( ~*ar->get_pointer<int>( ) ), ar->type_info( ) );
		return std::move( vref );
	} ) );

	integral->add_method( std::make_shared<method>( "^",
													method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new int( *ar->get_pointer<int>( ) ^ *rhs->get_pointer<int>( ) ), ar->type_info( ) );
		return std::move( vref );
	} ) );



	auto floating = std::make_shared<type_info>( "floating",
												 []( value_reference *ptr )
	{
		delete ptr->get_pointer<void>( );
	},
												 []( value_reference *ptr )
	{
		return std::make_shared<value_reference>( new double( *ptr->get_pointer<double>( ) ), ptr->type_info( ) );
	} );

	floating->add_method( std::make_shared<method>( "=",
													method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		if ( ar->get_pointer<void>( ) != nullptr )
		{
			if ( ar->type_info( )->bare_equal( *rhs->type_info( ) ) )
				*ar->get_pointer<double>( ) = *rhs->get_pointer<double>( );
			else if ( rhs->type_info( )->get_name( ) == "integral" )
				*ar->get_pointer<double>( ) = *rhs->get_pointer<int>( );
			else
				throw conversion_error( engine->get_type_info( "floating" ),
										engine->get_type_info( "floating" )->get_method( "=" ),
										rhs->type_info( ),
										{ engine->get_type_info( "floating" ) } );
		}
		else
		{
			if ( ar->type_info( )->bare_equal( *rhs->type_info( ) ) )
				ar->set_pointer( new double( *rhs->get_pointer<double>( ) ) );
			else if ( rhs->type_info( )->get_name( ) == "integral" )
				ar->set_pointer( new double( *rhs->get_pointer<int>( ) ) );
			else
				throw conversion_error( engine->get_type_info( "floating" ),
										engine->get_type_info( "floating" )->get_method( "=" ),
										rhs->type_info( ),
										{ engine->get_type_info( "floating" ) } );

		}
		return ep::void_return;
	} ) );

	floating->add_method( std::make_shared<method>( "+",
													method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new double( *ar->get_pointer<double>( ) + *rhs->get_pointer<double>( ) ), ar->type_info( ) );
		return std::move( vref );
	} ) );

	floating->add_method( std::make_shared<method>( "-",
													method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new double( *ar->get_pointer<double>( ) - *rhs->get_pointer<double>( ) ), ar->type_info( ) );
		return std::move( vref );
	} ) );

	floating->add_method( std::make_shared<method>( "*",
													method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new double( *ar->get_pointer<double>( ) * *rhs->get_pointer<double>( ) ), ar->type_info( ) );
		return std::move( vref );
	} ) );

	floating->add_method( std::make_shared<method>( "/",
													method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new double( *ar->get_pointer<double>( ) / *rhs->get_pointer<double>( ) ), ar->type_info( ) );
		return std::move( vref );
	} ) );

	floating->add_method( std::make_shared<method>( "&&",
													method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new double( *ar->get_pointer<double>( ) && *rhs->get_pointer<double>( ) ), ar->type_info( ) );
		return std::move( vref );
	} ) );

	floating->add_method( std::make_shared<method>( "||",
													method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new double( *ar->get_pointer<double>( ) || *rhs->get_pointer<double>( ) ), ar->type_info( ) );
		return std::move( vref );
	} ) );

	floating->add_method( std::make_shared<method>( "==",
													method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new double( *ar->get_pointer<double>( ) == *rhs->get_pointer<double>( ) ), ar->type_info( ) );
		return std::move( vref );
	} ) );


	floating->add_method( std::make_shared<method>( "!=",
													method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new double( *ar->get_pointer<double>( ) / *rhs->get_pointer<double>( ) ), ar->type_info( ) );
		return std::move( vref );
	} ) );

	floating->add_method( std::make_shared<method>( ">",
													method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new double( *ar->get_pointer<double>( ) > *rhs->get_pointer<double>( ) ), ar->type_info( ) );
		return std::move( vref );
	} ) );

	floating->add_method( std::make_shared<method>( "<",
													method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new double( *ar->get_pointer<double>( ) < *rhs->get_pointer<double>( ) ), ar->type_info( ) );
		return std::move( vref );
	} ) );

	floating->add_method( std::make_shared<method>( ">=",
													method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new double( *ar->get_pointer<double>( ) >= *rhs->get_pointer<double>( ) ), ar->type_info( ) );
		return std::move( vref );
	} ) );

	floating->add_method( std::make_shared<method>( "<=",
													method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new double( *ar->get_pointer<double>( ) <= *rhs->get_pointer<double>( ) ), ar->type_info( ) );
		return std::move( vref );
	} ) );


	auto string = std::make_shared<type_info>( "string_literal",
															   []( value_reference *ptr )
	{
		delete ptr->get_pointer<std::string>( );
	},
															   []( value_reference *ptr )
	{
		return std::make_shared<value_reference>( new std::string( *ptr->get_pointer<std::string>( ) ), ptr->type_info( ) );
	} );

	string->add_method( std::make_shared<method>( "=",
												  method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
												  []( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		if ( ar->type_info( )->bare_equal( *rhs->type_info( ) ) )
			*ar->get_pointer<std::string>( ) = *rhs->get_pointer<std::string>( );
		else if ( rhs->type_info( )->get_name( ) == "integral" )
			*ar->get_pointer<std::string>( ) = std::to_string( *rhs->get_pointer<int>( ) );
		else if ( rhs->type_info( )->get_name( ) == "floating" )
			*ar->get_pointer<std::string>( ) = std::to_string( *rhs->get_pointer<double>( ) );
		return ep::void_return;
	} ) );

	string->add_method( std::make_shared<method>( "==",
													method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
													[]( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new bool( *ar->get_pointer<std::string>( ) == *rhs->get_pointer<std::string>( ) ), 
													   engine->get_type_info( "integral" ) );
		return std::move( vref );
	} ) );


	string->add_method( std::make_shared<method>( "!=",
												  method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
												  []( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new bool( *ar->get_pointer<std::string>( ) != *rhs->get_pointer<std::string>( ) ),
													   engine->get_type_info( "integral" ) );
		return std::move( vref );
	} ) );

	string->add_method( std::make_shared<method>( "+",
												  method_argument_list{ std::make_shared<method_argument>( "rhs", true ) },
												  []( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		auto rhs = args[1]->get_ref( );
		auto vref = std::make_shared<value_reference>( new std::string( *ar->get_pointer<std::string>( ) + *rhs->get_pointer<std::string>( ) ),
													   engine->get_type_info( "string_literal" ) );
		return std::move( vref );
	} ) );

	// type info method..
	struct _type
	{
	public:
		_type( type_info *type )
			: type_( type )
		{ }

		bool bare_equal( _type *other )
		{
			return type_->bare_equal( *other->type_ );
		}

		const std::string &get_name( )
		{
			return type_->get_name( );
		}

	private:
		type_info *type_;
	};
	
	auto typeinfo = std::make_shared<type_info>( "type_info",
												 []( value_reference *ptr )
	{
		delete ptr->get_pointer<_type>( );
	},
												 []( value_reference *clone )
	{
		return std::make_shared<value_reference>( new _type( *clone->get_pointer<_type>( ) ),
												  clone->type_info( ) );
	} );

	typeinfo->add_method( std::make_shared<method>( "bare_equal",
													method_argument_list{ std::make_shared<method_argument>( "other", true ) },
													[]( variable_list &args, dispatch_engine_ptr engine )
	{
		auto &ar = args[0]->get_ref( );
		auto &other = args[1]->get_ref( );
		return std::make_shared<value_reference>( new int( ar->get_pointer<_type>( )->bare_equal( other->get_pointer<_type>( ) ) ),
												  engine->get_type_info( "integral" ) );
	} ) );

	typeinfo->add_method( std::make_shared<method>( "get_name",
													method_argument_list{ },
													[]( variable_list &args, dispatch_engine_ptr engine )
	{
		auto &ar = args[0]->get_ref( );
		return std::make_shared<value_reference>( new std::string( ar->get_pointer<_type>( )->get_name( ) ),
												  engine->get_type_info( "string_literal" ) );
	} ) );

	typeinfo->add_method( std::make_shared<method>( "==",
													method_argument_list{ std::make_shared<method_argument>( "other", true ) },
													[]( variable_list &args, dispatch_engine_ptr engine )
	{
		auto &ar = args[0]->get_ref( );
		auto &other = args[1]->get_ref( );
		return std::make_shared<value_reference>( new int( ar->get_pointer<_type>( )->bare_equal( other->get_pointer<_type>( ) ) ),
												  engine->get_type_info( "integral" ) );
	} ) );

	typeinfo->add_method( std::make_shared<method>( "!=",
													method_argument_list{ std::make_shared<method_argument>( "other", true ) },
													[]( variable_list &args, dispatch_engine_ptr engine )
	{
		auto &ar = args[0]->get_ref( );
		auto &other = args[1]->get_ref( );
		return std::make_shared<value_reference>( new int( !ar->get_pointer<_type>( )->bare_equal( other->get_pointer<_type>( ) ) ),
												  engine->get_type_info( "integral" ) );
	} ) );

	typeinfo->add_method( std::make_shared<method>( "=",
													method_argument_list{ std::make_shared<method_argument>( "other", true ) },
													[]( variable_list &args, dispatch_engine_ptr engine )
	{
		auto &ar = args[0]->get_ref( );
		auto &other = args[1]->get_ref( );
		if ( !other->type_info( )->bare_equal( *ar->type_info( ) ) )
			throw conversion_error( engine->get_type_info( "type_info" ),
									engine->get_type_info( "type_info" )->get_method( "=" ),
									ar->type_info( ),
									{ engine->get_type_info( "type_info" ) } );
		return std::make_shared<value_reference>( new _type( *other->get_pointer<_type>( ) ),
												  ar->type_info( ) );
	} ) );


	auto nullptr_t = std::make_shared<ep::type_info>( "nullptr_t",
													  []( ep::value_reference *ptr )
	{},
													  []( ep::value_reference *ptr )
	{
		return std::make_shared<ep::value_reference>( nullptr,
													  ptr->type_info( ) );
	} );
	
	add_variable( std::make_shared<ep::variable>( "nullptr", std::make_shared<value_reference>( nullptr, nullptr_t ), true, true ) );
	add_type( std::move( floating ) );
	add_type( std::move( integral ) );
	add_type( std::move( string ) );
	add_type( std::move( typeinfo ) );
	add_type( std::move( nullptr_t ) );

	add_method( std::make_shared<method>( "floating",
										  method_argument_list{ std::make_shared<method_argument>( "other", true ) },
										  []( variable_list &args, dispatch_engine_ptr engine )
	{
		auto &argument = args[0];
		auto ar = argument->get_ref( );
		double value = 0;
		if ( ar->type_info( )->get_name( ) == "floating" )
			value = *ar->get_pointer<double>( );
		else if ( ar->type_info( )->get_name( ) == "integral" )
			value = *ar->get_pointer<int>( );
		else
			throw conversion_error( engine->get_type_info( "floating" ),
									engine->get_method( "floating" ),
									ar->type_info( ),
									{ engine->get_type_info( "floating" ) } );
		return std::make_shared<value_reference>( new double( value ),
												  engine->get_type_info( "floating" ) );
	} ) );

	add_method( std::make_shared<method>( "integral",
										  method_argument_list{ std::make_shared<method_argument>( "other", true ) },
										  []( variable_list &args, dispatch_engine_ptr engine )
	{
		auto &argument = args[0];
		auto ar = argument->get_ref( );
		int value = 0;
		if ( ar->type_info( )->get_name( ) == "floating" )
			value = *ar->get_pointer<double>( );
		else if ( ar->type_info( )->get_name( ) == "integral" )
			value = *ar->get_pointer<int>( );
		else
			throw conversion_error( engine->get_type_info( "integral" ),
									engine->get_method( "integral" ),
									ar->type_info( ),
									{ engine->get_type_info( "integral" ) } );
		return std::make_shared<value_reference>( new int( value ),
												  engine->get_type_info( "integral" ) );
	} ) ); 
	
	add_method( std::make_shared<method>( "string_literal",
										  method_argument_list{ std::make_shared<method_argument>( "other", true ) },
										  []( variable_list &args, dispatch_engine_ptr engine )
	{
		auto &argument = args[0];
		auto ar = argument->get_ref( );
		std::string value;
		if ( ar->type_info( )->get_name( ) == "floating" )
			value = std::to_string( *ar->get_pointer<double>( ) );
		else if ( ar->type_info( )->get_name( ) == "integral" )
			value = std::to_string( *ar->get_pointer<int>( ) );
		else if ( ar->type_info( )->get_name( ) == "string_literal" )
			value = *ar->get_pointer<std::string>( );
		else
			throw conversion_error( engine->get_type_info( "string_literal" ),
									engine->get_method( "string_literal" ),
									ar->type_info( ),
									{ engine->get_type_info( "string_literal" ) } );
		return std::make_shared<value_reference>( new std::string( value ),
												  engine->get_type_info( "string_literal" ) );
	} ) );

	add_method( std::make_shared<method>( "type_info",
										  method_argument_list{ std::make_shared<method_argument>( "other", true ) },
										  []( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		return std::make_shared<value_reference>( new _type( ar->type_info( ).get( ) ),
												  engine->get_type_info( "type_info" ) );
	} ) );

	add_method( std::make_shared<method>( "typeid",
										  method_argument_list{ std::make_shared<method_argument>( "other", true ) },
										  []( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		return std::make_shared<value_reference>( new _type( ar->type_info( ).get( ) ),
												  engine->get_type_info( "type_info" ) );
	} ) );

	add_method( std::make_shared<method>( "decltype",
										  method_argument_list{ std::make_shared<method_argument>( "name", true ) },
										  []( variable_list args, dispatch_engine_ptr engine )
	{
		auto ar = args[0]->get_ref( );
		if ( !ar->is_string( ) )
			throw conversion_error( nullptr,
									engine->get_method( "decltype" ),
									ar->type_info( ),
									{ engine->get_type_info( "string_literal" ) } );
		return std::make_shared<ep::value_reference>( new _type( engine->get_type_info( *ar->get_pointer<std::string>( ) ).get( ) ),
													  engine->get_type_info( "type_info" ) );
	} ) );

}

ep::dispatch_state_ptr ep::dispatch_engine::create_new_dispatch_state( std::string name, const method_argument_list & args, variable_list actual_args, dispatch_state_ptr parent )
{
	variable_list fixed_args( std::move( actual_args ) );
	auto mIt = args.begin( ), mBeg = args.begin( ), mEnd = args.end( );
	auto fIt = fixed_args.begin( ), fEnd = fixed_args.end( );
	for ( ;
		  mIt < mEnd && fIt < fEnd;
		  ++fIt )
		if ( ( *fIt )->get_name( ) != "this" )
			( *fIt )->get_name( ) = ( *(mIt++) )->get_name( );
	return dispatch_state_ptr( new dispatch_state( std::move( fixed_args ), this, parent ) );
}


ep::variable_ptr ep::dispatch_engine::get_variable( const std::string & id )
{
	return state_->get_variable( id );
}

ep::method_ptr ep::dispatch_engine::get_method( const std::string & id )
{
	auto it = find_method( id );
	return it == bad_method_it( ) ? 
		throw undefined_identifier_error( id ) : 
		*it;
}

ep::dispatch_state_ptr ep::dispatch_engine::create_new_dispatch_state( std::string name, const method_argument_list & args, variable_list actual_args )
{
	variable_list fixed_args( std::move( actual_args ) );
	auto mIt = args.begin( ), mEnd = args.end( );
	auto fIt = fixed_args.begin( ), fEnd = fixed_args.end( );
	for ( ;
		  mIt < mEnd && fIt < fEnd;
		  ++mIt, ++fIt )
		  ( *fIt )->get_name( ) = ( *mIt )->get_name( );
	return dispatch_state_ptr( new dispatch_state( std::move( fixed_args ), this ) );
}

ep::dispatch_state_ptr ep::dispatch_engine::create_new_dispatch_state( std::string name, dispatch_state_ptr parent )
{
	return dispatch_state_ptr( new dispatch_state( std::move( parent ), this ) );
}

void ep::dispatch_engine::add_method( method_ptr method )
{
	if ( find_method( method->get_name( ) ) != bad_method_it( ) )
		throw symbol_collision_error( nullptr, method, "" );
	methods_.emplace_back( std::move( method ) );
}

ep::variable_list & ep::dispatch_engine::get_variables( )
{
	return state_->get_variables( );
}

void ep::dispatch_engine::add_variable( variable_ptr global )
{
	state_->add_variable( std::move( global ) );
}

ep::dispatch_state_ptr ep::dispatch_engine::get_global_state( )
{
	return state_;
}

ep::debugger_ptr & ep::dispatch_engine::get_debugger( )
{
	return dbg_;
}

void ep::dispatch_engine::add_type( type_info_ptr ti )
{ 
	auto existing = find_ti( ti->get_name( ) );
	if ( existing != bad_ti_it( ) )
		throw type_redefiniton_error( *existing, ti );
	tis_.emplace_back( std::move( ti ) );
}

ep::type_info_ptr ep::dispatch_engine::get_type_info( const std::string & id )
{
	auto it = find_ti( id );
	return it != bad_ti_it( ) ?
		*it :
		throw undefined_type_error( id );
}

ep::type_info_list & ep::dispatch_engine::get_type_infos( )
{
	return tis_;
}

ep::method_list &ep::dispatch_engine::get_methods( )
{
	return methods_;
}

ep::method_list::iterator ep::dispatch_engine::find_method( const std::string & id )
{
	for ( auto it = methods_.begin( ), end = methods_.end( );
		  it < end;
		  ++it )
		if ( ( *it )->get_name( ) == id )
			return it;
	return bad_method_it( );
}

ep::method_list::iterator ep::dispatch_engine::bad_method_it( )
{
	return methods_.end( );
}

ep::type_info_list::iterator ep::dispatch_engine::find_ti( const std::string & id )
{
	for ( auto it = tis_.begin( ), end = tis_.end( );
		  it < end;
		  ++it )
		if ( ( *it )->get_name( ) == id )
			return it;
	return bad_ti_it( );
}

ep::type_info_list::iterator ep::dispatch_engine::bad_ti_it( )
{
	return tis_.end( );
}
