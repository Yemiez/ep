/********************************************************
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*				   method.cpp					*	*
*  	*			   Created by Yamiez				*	*
*  	*	Do not forget to give credit when using		*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*********************************************************/
#include "method.h"
#include "eval_error.h"
#include "dispatch_engine.h"
#include "dispatch_state.h"
#include "variable.h"
#include "value_reference.h"
#include "expression_action.h"
#include "expression.h"
#include "statement.h"
#include "method_argument.h"
#include <algorithm>

ep::method::method( std::string name, method_argument_list args, statement_list statements )
	: name_( std::move( name ) ), args_( std::move( args ) ), statements_( std::move( statements ) ),
	  lambda_( nullptr )
{ }

ep::method::method( std::string name, method_argument_list args, std::function<value_reference_ptr( variable_list, dispatch_engine_ptr )> fun )
	: name_( std::move( name ) ), args_( std::move( args ) ), statements_( ), lambda_( std::move( fun ) )
{ }

const std::string & ep::method::get_name( ) const
{
	return name_;
}

std::string & ep::method::get_name( )
{
	return name_;
}

const ep::method_argument_list & ep::method::get_arguments( ) const
{
	return args_;
}

ep::method_argument_list & ep::method::get_arguments( )
{
	return args_;
}

size_t ep::method::get_arguments_size( ) const
{
	return args_.size( );
}

const ep::statement_list & ep::method::get_statements( ) const
{
	return statements_;
}

ep::statement_list & ep::method::get_statements( )
{
	return statements_;
}

size_t ep::method::get_statements_size( ) const
{
	return statements_.size( );
}

ep::value_reference_ptr ep::method::invoke( variable_list list, dispatch_engine_ptr engine )
{
	std::reverse( list.begin( ), list.end( ) );
	if ( !lambda_ )
		create_lambda( );
	return !lambda_ ? 
		throw lambda_creation_failed_error( this ) : 
		lambda_( std::move( list ), std::move( engine ) );
}

std::string ep::method::pretty_format( ) const
{
	std::stringstream stream;
	stream << name_ << "( " << ( args_.empty( ) ? ")" : "" );
	if ( !args_.empty( ) )
	{
		for ( auto it = args_.begin( ), end = args_.end( );
			  it < end;
			  ++it )
		{
			if ( ( it + 1 ) == end )
			{
				if ( ( *it )->is_reference( ) )
					stream << "&";
				stream << ( *it )->get_name( ) << " )";
			}
			else
			{
				if ( ( *it )->is_reference( ) )
					stream << "&";
				stream << ( *it )->get_name( ) << ", ";
			}
		}
	}
	return stream.str( );
}

std::function<ep::value_reference_ptr( ep::variable_list, ep::dispatch_engine_ptr )>& ep::method::get_fun( )
{
	if ( !lambda_ )
		create_lambda( );
	return lambda_;
}

void ep::method::create_lambda( )
{
	if ( specializations_.empty() )
	{
		lambda_ = [this]( variable_list arguments, dispatch_engine_ptr engine )
		{
			auto state = engine->create_new_dispatch_state( get_name( ), get_arguments( ), std::move( arguments ), engine->get_global_state( ) );
			for ( auto &x : get_statements( ) )
				if ( x->eval_or_eval_and_return( engine, state, engine->get_debugger( ) ) )
					return std::move( x->get_return( ) );
			return ep::void_return;
		};
	}
	else
	{
		lambda_ = [this]( variable_list arguments, dispatch_engine_ptr engine )
		{
			auto state = engine->create_new_dispatch_state( get_name( ), get_arguments( ), std::move( arguments ), engine->get_global_state( ) );
			for ( auto &x : specializations_ )
			{
				auto vref = x->evaluate_expression( engine, state );
				if ( !vref->is_integral( ) )
					throw invalid_specialization_expression_error( this, x );
				if ( *vref->get_pointer<int>( ) )
				{
					for ( auto &y : x->get_statements( ) )
						if ( y->eval_or_eval_and_return( engine, state, engine->get_debugger( ) ) )
							return std::move( y->get_return( ) );
					return ep::void_return;
				}
			}
			for ( auto &x : get_statements( ) )
				if ( x->eval_or_eval_and_return( engine, state, engine->get_debugger( ) ) )
					return std::move( x->get_return( ) );
			return ep::void_return;
		};
	}
}

void ep::method::add_specialization( method_ptr method )
{
	specializations_.emplace_back( std::move( method ) );
}

void ep::method::make_specialization( expression_ptr expr_spec )
{ 
	expr_ = std::move( expr_spec );
}

ep::value_reference_ptr ep::method::evaluate_expression( dispatch_engine_ptr engine, dispatch_state_ptr state )
{
	return expr_->evaluate_actions( engine, state, false, false );
}
