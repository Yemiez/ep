/********************************************************
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*			   method_argument.h				*	*
*  	*			   Created by Yamiez				*	*
*  	*	Do not forget to give credit when using		*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*********************************************************/
#pragma once
#include "all_defines.h"

namespace ep
{

	class method_argument
	{
	public:
		method_argument( std::string id, bool is_ref = false, bool is_public = true );
		
		const std::string &get_name( ) const;

		std::string &get_name( );

		bool is_reference( ) const;

		bool is_public( ) const;

		bool is_private( ) const;

	private:
		std::string id_;
		bool is_reference_, is_public_;
	};

}