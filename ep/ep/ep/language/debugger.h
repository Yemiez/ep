#pragma once
#include "all_defines.h"

namespace ep
{

	class debugger
	{
	public:
		debugger( std::ostream &output );

		void step_into( method_ptr &method, dispatch_state_ptr &state );

		void step_into( expression_ptr &expression, dispatch_state_ptr &state );

		void step_into( statement_ptr &statement, dispatch_state_ptr &state );

	private:
		std::ostream &op_;
	};

}