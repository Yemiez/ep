/********************************************************
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*			  value_reference.cpp				*	*
*  	*			   Created by Yamiez				*	*
*  	*	Do not forget to give credit when using		*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*********************************************************/
#include "value_reference.h"
#include "eval_error.h"
#include "method.h"
#include "variable.h"
#include "type_system.h"
#include "method.h"
#include "method_argument.h"

ep::value_reference::value_reference( )
	: ptr_( nullptr ), init_( false ), internal_( nullptr ), is_dyn_( false ), destruct_( true ), size_( 0 )
{ }

ep::value_reference::value_reference( void * ptr, type_info_ptr ti, bool destruct )
	: ptr_( ptr ), ti_( std::move( ti ) ), destruct_( destruct ), init_( true ), ati_( &typeid( void ) ), internal_( nullptr ), size_( 0 ), is_dyn_( false )
{ }

ep::value_reference::~value_reference( )
{
	if ( ti_ && destruct_ )
		ti_->get_dtor( )( this );
}

void ep::value_reference::set_pointer( void * ptr )
{
	if ( ptr_ && ti_ && destruct_ )
		ti_->get_dtor( )( this );
	ati_ = &typeid( void );
	ptr_ = ptr;
}

bool ep::value_reference::is_uninitalized( ) const
{
	return !ptr_ && !init_;
}

bool ep::value_reference::is_string( ) const
{
	return ti_ && ti_->get_name( ) == "string_literal";
}

bool ep::value_reference::is_floating( ) const
{
	return ti_ && ti_->get_name( ) == "floating";
}

bool ep::value_reference::is_integral( ) const
{
	return ti_->get_name( ) == "integral";
}

bool ep::value_reference::is_dynamic( ) const
{
	return is_dyn_;
}

bool ep::value_reference::is_anonymous_method( ) const
{
	return internal_ != nullptr;
}

size_t ep::value_reference::get_type_size( ) const
{
	return size_;
}

void ep::value_reference::make_anonymous_method( ep::method_ptr method )
{
	internal_ = method;
}

ep::value_reference_ptr ep::value_reference::clone( )
{
	return ti_->get_clone( )( this );
}

ep::type_info_ptr & ep::value_reference::type_info( )
{
	return ti_;
}

ep::method_ptr ep::value_reference::get_method( const std::string & id )
{;
	return ti_ ? 
		ti_->get_method( id ) :
		throw undefined_identifier_error( id );
}

ep::method_ptr ep::value_reference::get_anonymous_method( )
{
	return internal_;
}

ep::variable_ptr ep::value_reference::get_member( const std::string & id )
{
	for ( auto &x : members_ )
	{
		if ( x->get_name( ) == id )
			return x;
	}
	throw undefined_identifier_error( id );
}

ep::variable_list & ep::value_reference::get_members( )
{
	return members_;
}

ep::method_list & ep::value_reference::get_methods( )
{
	return ti_->get_methods( );
}

void ep::value_reference::make_dynamic( )
{ 
	is_dyn_ = true;
}

void ep::value_reference::assign_refs( value_reference_ptr &vreference_, 
									   value_reference_ptr &ref, 
									   dispatch_engine_ptr engine )
{
	if ( !vreference_->type_info( ) )
		vreference_->type_info( ) = std::move( ref->type_info( ) );
	auto method = vreference_->type_info( )->get_method( "=" );
	method->invoke( { std::make_shared<variable>( method->get_arguments( ).front( )->get_name( ),
												  std::move( ref ),
												  method->get_arguments( ).front( )->is_reference( ),
												  true,
												  method->get_arguments().front()->is_public( )),
					std::make_shared<variable>( "this",
												vreference_,
												true,
												true ) },
					engine );
}
