/********************************************************
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*				expression.cpp					*	*
*  	*			   Created by Yamiez				*	*
*  	*	Do not forget to give credit when using		*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*********************************************************/
#include "dispatch_engine.h"
#include "dispatch_state.h"
#include "expression_action.h"
#include "expression.h"
#include "eval_error.h"
#include "method.h"
#include "variable.h"
#include "method_argument.h"
#include "value_reference.h"
#include "../../cpputils/tokenizer.h"
#include "type_system.h"
#include "libs.h"
#include <iostream>
#include <stack>
#include "statement.h"
#include <algorithm>


ep::expression::expression( std::vector<tokenizer_it> refs,
							tokenizer_it & it,
							const tokenizer_it & end, 
							dispatch_engine_ptr engine,
							bool skip )
{
	create_expression_list( refs, it, end, std::move( engine ), skip );
}

ep::expression::expression( const expression & expr )
	: list_( expr.list_ ), os_( expr.os_.str() ), where_( expr.where_ )
	
{ }

ep::expression::expression( expression && expr )
	: list_( std::move( expr.list_ ) ), os_( std::move( expr.os_ ) ), where_( expr.where_ )
{ }

const ep::expression_action_list & ep::expression::get_actions( ) const
{
	return list_;
}

ep::expression_action_list & ep::expression::get_actions( )
{
	return list_;
}

size_t ep::expression::get_actions_size( ) const
{
	return list_.size( );
}

ep::value_reference_ptr ep::expression::evaluate_actions( dispatch_engine_ptr engine,  
														  dispatch_state_ptr state, 
														  bool is_ret,
														  bool as_ref )
{
	std::stack<expression_action_ptr> stack;
	for ( auto it = list_.cbegin( ), end = list_.cend( ); 
		  it < end;
		  ++it )
	{
		auto &x = *it;
		if ( x->is_variable( ) || x->is_ref( ) )
			stack.push( x );
		else if ( x->get_ref_name( ) == "->" )
		{
			if ( std::distance( it, end ) >= 3 )
			{
				auto member = *++it;
				auto object = *++it;
				value_reference_ptr vref( nullptr );
				if ( object->is_method( ) ) // found a a->b->c example
				{
					--it;
					vref = stack.top( )->get_ref( );
					stack.pop( );
				}
				else if ( object->is_ref( ) )
					vref = std::move( object->get_ref( ) );
				else
					vref = state->get_variable( object->get_ref_name( ) )->get_ref( );

				if ( member->is_method( ) )
				{
					auto method = vref->get_method( member->get_ref_name( ) );
					if ( stack.size( ) < method->get_arguments_size( ) )
						throw arity_error( vref->type_info( ),
										   method,
										   this );
					auto args = arg_compile( method->get_arguments( ),
											 stack,
											 state );
					args.emplace_back( std::make_shared<variable>( "this", std::move( vref ) ) );
					stack.push( std::make_shared<expression_action>( method->invoke( std::move( args ), engine ) ) );
				}
				else if ( member->is_variable( ) )
				{
					variable_ptr mem{ nullptr };
					try
					{
						mem = vref->get_member( member->get_ref_name( ) ); // 0x00aa826c
					}
					catch( undefined_identifier_error &err )
					{
						if ( vref->is_dynamic( ) )
						{
							vref->get_members( ).emplace_back( std::make_shared<variable>( member->get_ref_name( ),
																						   std::make_shared<value_reference>( ),
																						   false,
																						   true,
																						   true ) );
							mem = vref->get_members( ).back( );
						}
						else
							throw err;
					}
					if ( mem->is_private( ) && object->get_ref_name( ) != "this" )
					{
						auto this_ = state->get_this( );
						if ( this_ &&
							 this_->get_ref( )->type_info( ) &&
							 this_->get_ref( )->type_info( )->get_name( ) == vref->type_info( )->get_name( ) )
							;
						else
							throw member_is_inaccessible( std::move( vref ), std::move( mem ), this );
					}
					stack.push( std::make_shared<expression_action>( mem->get_ref( ) ) );
				}
			}
			else if ( std::distance( it, end ) == 2 )
			{
				auto member = *++it;
				auto object = stack.top( );
				stack.pop( );
				value_reference_ptr vref( nullptr );
				if ( object->is_method( ) ) // found a a->b->c example
				{
					--it;
					vref = stack.top( )->get_ref( );
					stack.pop( );
				}
				else if ( object->is_ref( ) )
					vref = std::move( object->get_ref( ) );
				else
					vref = state->get_variable( object->get_ref_name( ) )->get_ref( );

				if ( member->is_method( ) )
				{
					method_ptr method( nullptr );
					try
					{
						method = vref->get_method( member->get_ref_name( ) );
					}
					catch ( ep::undefined_identifier_error &err )
					{
						if ( !stack.top( )->is_ref( ) )
							throw err;
						vref = stack.top( )->get_ref( );
						stack.pop( );
						stack.push( object );
						method = vref->get_method( member->get_ref_name( ) );
					}
					if ( stack.size( ) < method->get_arguments_size( ) )
						throw arity_error( vref->type_info( ),
										   method,
										   this );
					auto args = arg_compile( method->get_arguments( ),
											 stack,
											 state );
					args.emplace_back( std::make_shared<variable>( "this", std::move( vref ) ) );
					stack.push( std::make_shared<expression_action>( method->invoke( std::move( args ), engine ) ) );
				}
				else if ( member->is_variable( ) )
				{
					auto mem = vref->get_member( member->get_ref_name( ) );
					if ( mem->is_private( ) && object->get_ref_name( ) != "this" )
						throw member_is_inaccessible( std::move( vref ), std::move( mem ), this );
					stack.push( std::make_shared<expression_action>( mem->get_ref( ) ) );
				}
			}
			else
				throw invalid_access_usage_error( this );
		}
		else if ( x->is_method( ) )
		{
			method_ptr method{ nullptr };

			try
			{
				method = state->get_method( x->get_ref_name( ) );
			}
			catch ( undefined_identifier_error & )
			{
				auto var = state->get_variable( x->get_ref_name( ) );
				if ( var->get_ref( )->is_anonymous_method( ) )
					method = var->get_ref( )->get_anonymous_method( );
				else
					method = var->get_method( "()" );
			}

			if ( stack.size( ) < method->get_arguments_size( ) )
				throw arity_error( nullptr,
								   method,
								   this );
			
			auto arguments = std::move( arg_compile( method->get_arguments( ),
													 stack,
													 state ) );
			stack.push( std::make_shared<expression_action>( std::move( method->invoke( std::move( arguments ), engine ) ) ) );
		}
	}

	value_reference_ptr vref( nullptr );
	if ( stack.top( )->is_ref( ) ) // Copy the value reference, this is an rvalue.
		vref = std::move( stack.top( )->get_ref( ) );
	else
		if ( is_ret ) // If we are picking a local variable, and returning it. Steal it.
			vref = std::move( state->get_variable( stack.top( )->get_ref_name( ) )->steal( ) );
		else if ( as_ref )
			vref = std::move( state->get_variable( stack.top( )->get_ref_name( ) )->get_ref( ) );
		else // Else copy the value
			vref = std::move( state->get_variable( stack.top( )->get_ref_name( ) )->get_ref( )->clone( ) );
	return std::move( vref ); // 0x01318238 0x01318238
}

bool ep::expression::is_constexpr( ) const
{
	if ( list_.size( ) < 3 )
	{
		for ( auto it = list_.begin( ), end = list_.end( );
			  it < end;
			  ++it )
			if ( ( *it )->is_variable( ) || ( ( *it )->is_method( ) && !is_std_bo( ( *it )->get_ref_name( ) ) ) )
				return false;
		return true;
	}

	for ( auto it = list_.begin( ) + 1, end = list_.end( );
		  it < ( end - 1 );
		  ++it )
		if ( ( *it )->is_variable( ) || ( ( *it )->is_method( ) && !is_std_bo( ( *it )->get_ref_name( ) ) ) )
			return false;
	return !list_.empty( ) && list_.back( )->get_ref_name( ) == "=";
	//for ( auto &x : list_ )
	//	if ( x->is_variable( ) || ( x->is_method( ) && !is_std_bo( x->get_ref_name( ) ) ) )
	//		return false;
	//return true;
}

bool ep::expression::has_access( ) const
{
	return std::find_if( list_.cbegin( ), list_.cend( ), []( const expression_action_ptr &ptr )
	{ 
		return ptr->get_ref_name( ) == "->";
	} ) != list_.end( );
}

bool ep::expression::has_fcalls( ) const
{
	for ( auto &x : list_ )
		if ( x->is_method( ) )
			return true;
	return false;
}

void ep::expression::set_string_format( std::string fmt )
{ 
	std::ostringstream( ).swap( os_ );
	os_ << fmt;
}

std::string ep::expression::pretty_format( ) const
{
	return os_.str( );
}

cpputils::file_position ep::expression::get_position( ) const
{
	return where_;
}


void ep::expression::create_expression_list( std::vector<tokenizer_it> &refs,
											 tokenizer_it & it,
											 const tokenizer_it & end,
											 dispatch_engine_ptr engine,
											 bool skip )
{
	where_ = it->get_position( );
	auto copyIt = it;
	bool bFoundSemi = false;
	std::stack<tokenizer_it> stack;
	auto& output = list_;
	for ( ;
		  it < end;
		  ++it )
	{
		if ( it->is_decimal( ) || it->is_hexadecimal( ) ) 
			output.emplace_back(
				new expression_action(
					std::make_shared<value_reference>(
						new int(
							it->get_t<int>( ) ),
						engine->get_type_info( "integral" ) ) ) );
		else if ( it->is_string( ) )
			output.emplace_back( new expression_action( std::make_shared<value_reference>( new std::string( it->get_block( ) ), engine->get_type_info( "string_literal" ) ) ) );
		else if ( it->get_block( ) == "method" )
		{
			output.emplace_back( parse_anonymous_method( refs, it, end, engine ) );
		}
		else if ( it->is_identifier( ) && ( ( it + 1 )->get_block( ) == "->" || ( it + 1 )->get_block( ) == "." ) )
		{
			// i'M RETARDEDRD, THIS WILL NEVER HAPPEN FUCK
			if ( ( it + 2 )->is_identifier( ) && ( it + 3 )->get_block( ) != "(" )
			{
				auto object = it++;
				auto op = it++;
				auto member = it;
				output.emplace_back( new expression_action( "->", true ) );
				output.emplace_back( new expression_action( member->get_block( ), false ) );
				output.emplace_back( new expression_action( object->get_block( ), false ) );
			}
			else // method access
			{
				stack.push( it++ );
				stack.push( it );
			}
		}
		else if ( it->is_identifier( ) && ( it + 1 )->get_block( ) != "(" )
			output.emplace_back( new expression_action( it->get_block( ), false ) );
		else if ( it->is_identifier( ) )
			stack.push( it );
		else if ( it->is_operator( ) )
		{
			if ( it->get_block( ) == "," )
			{
				while ( !stack.empty( ) && stack.top( )->get_block( ) != "(" )
				{
					insert_to_output( refs, stack.top( ), end, output, engine );
					stack.pop( );
				}

				if ( stack.top( )->get_block( ) != "(" )
					throw comma_was_misplaced_error( refs,
													 stack.top( ),
													 end );
			} // ','
			else if ( it->get_block( ) == "(" )
				stack.push( it );
			else if ( it->get_block( ) == ")" )
			{
				while ( !stack.empty( ) && stack.top( )->get_block( ) != "(" )
				{
					insert_to_output( refs, stack.top( ), end, output, engine );
					stack.pop( );
				}

				if ( stack.top( )->get_block( ) != "(" )
					throw comma_was_misplaced_error( refs,
													 stack.top( ),
													 end );
				stack.pop( );
				if ( !stack.empty( ) && stack.top( )->is_identifier( ) )
				{
					auto meth = stack.top( )->get_block( );
					stack.pop( );
					if ( !stack.empty( ) && ( stack.top( )->get_block( ) == "->" || stack.top( )->get_block( ) == "." ) )
					{
						output.emplace_back( new expression_action( "->", true ) );
						output.emplace_back( new expression_action( meth, true ) );
						stack.pop( );
						if ( !stack.empty( ) && stack.top( )->is_identifier( ) )
						{
							insert_to_output( refs, stack.top( ), end, output, engine );
							stack.pop( );
						}
					}
					else
						output.emplace_back( new expression_action( meth, true ) );
				}

				if ( skip )
				{
					bool exit = true;
					for ( auto &x : stack._Get_container( ) )
						if ( x->get_block( ) == "(" )
							exit = false;
					if ( exit )
					{
						++it;
						break;
					}
				}
			} // ')'
			else if ( it->get_block( ) == ";" || it->get_block( ) == "," )
			{
				++it;
				bFoundSemi = true;
				break;
			}
			else // Operator
			{
				while ( !stack.empty( ) && stack.top( )->is_operator( ) && stack.top( )->get_block( ) != "(" && stack.top( )->get_block( ) != ")" )
				{
					auto top = stack.top( );
					if ( !top->is_right_associated( ) && it->get_presedence( ) >= top->get_presedence( ) )
						output.emplace_back( new expression_action( top->get_block( ), true ) );
					else if ( top->is_right_associated( ) && it->get_presedence( ) > top->get_presedence( ) )
						output.emplace_back( new expression_action( top->get_block( ), true ) );
					else
						break;
					stack.pop( );
				}
				stack.push( it );
			} // Operator

		} // it->is_operator()
		else 
			break;
	} // For loop

	if ( !bFoundSemi && !skip )
		throw expected_semicolon_error( refs, copyIt, end );

	while ( !stack.empty( ) )
	{
		if ( stack.top( )->get_block( ) == "(" )
			throw expected_closing_parenthesis_error( refs, stack.top( ), end );
		else if ( stack.top( )->get_block( ) == ")" )
			throw expected_opening_parenthesis_error( refs, stack.top( ), end );
		insert_to_output( refs, stack.top( ), end, output, engine );
		stack.pop( );
	}
	for ( ;
		  copyIt < it;
		  ++copyIt )
	{
		if ( copyIt->is_string( ) )
			os_ << "\"" << copyIt->get_block( ) << "\" ";
		else if ( ( copyIt + 1 ) < it && ( ( copyIt + 1 )->get_block( ) == "->" || ( copyIt + 1 )->get_block( ) == "." ) )
			os_ << copyIt->get_block( );
		else if ( copyIt->get_block( ) == "->" || copyIt->get_block( ) == "." )
			os_ << copyIt->get_block( );
		else
			os_ << copyIt->get_block( ) << ( ( copyIt + 1 ) < it ?
			                               ( ( copyIt + 1 )->get_block( ) == ";" || ( copyIt + 1 )->get_block( ) == "," || ( copyIt + 1 )->get_block( ) == "(" ?
			                               "" :
			                               " " )
											 : "" );
	}
}

void ep::expression::insert_to_output( std::vector<tokenizer_it> &refs,
									   tokenizer_it & it,
									   const tokenizer_it &end,
									   expression_action_list & output,
									   dispatch_engine_ptr &engine )
{ 
	if ( it->is_identifier( ) && ( it + 1 )->get_block( ) == "(" )
		output.emplace_back( new expression_action( it->get_block( ), true ) );
	else if ( it->is_identifier( ) )
		output.emplace_back( new expression_action( it->get_block( ), false ) );
	else if ( it->get_block( ) == "." )
		output.emplace_back( new expression_action( "->", true ) );
	else if ( it->is_operator( ) )
		output.emplace_back( new expression_action( it->get_block( ), true ) );
	else if ( it->is_decimal( ) || it->is_hexadecimal( ) )
		output.emplace_back( new expression_action( std::make_shared<value_reference>( new double( it->get_t<double>( ) ), engine->get_type_info( "floating" ) ) ) );
	else if ( it->is_string( ) )
		output.emplace_back( new expression_action( std::make_shared<value_reference>( new std::string( it->get_block( ) ), engine->get_type_info( "string_literals" ) ) ) );
	else
		throw unknown_token_error( refs, it, end );
}

ep::expression_action_ptr ep::expression::parse_anonymous_method( std::vector<tokenizer_it>& refs, tokenizer_it & it, const tokenizer_it & end, dispatch_engine_ptr engine )
{
	++it;
	ep::statement_list statements;
	ep::method_argument_list arguments;
	if ( it->get_block( ) == "{" )
	{
		++it;
		while ( it->get_block( ) != "}" )
		{
			statements.emplace_back( new statement( refs, it, end, engine ) );
		}
		++it;
	}
	else if ( it->get_block( ) == "(" )
	{
		++it;
		while ( true )
		{
			if ( it == end )
				throw ran_out_of_tokens_error( { }, --it, end );
			bool is_ref = false;
			if ( it->get_block( ) == "&" )
				++it, is_ref = true;

			if ( it->is_identifier( ) )
				arguments.emplace_back( new method_argument( ( it++ )->get_block( ), is_ref ) );
			else
				throw expected_identifier_error( { }, it, end );

			if ( it->get_block( ) == ")" )
				break;
			else if ( it->get_block( ) != "," )
				throw expected_closing_parenthesis_error( { }, it, end );
			++it;
		}

		++it;
		if ( it->get_block( ) == "{" )
		{
			++it;
			while ( it->get_block( ) != "}" )
			{
				statements.emplace_back( new statement( refs, it, end, engine ) );
			}
			++it;
		}
		else
			throw expected_opening_bracket_error( refs, it, end );

	}
	else
		throw expected_opening_parenthesis_error( refs, it, end );
	--it;
	auto vref = std::make_shared<ep::value_reference>( );
	vref->make_anonymous_method( std::make_shared<ep::method>( "@anon@", std::move( arguments ), std::move( statements ) ) );
	return std::make_shared<ep::expression_action>( std::move( vref ) );
}

ep::variable_list ep::expression::arg_compile( method_argument_list & args, std::stack<expression_action_ptr>& stack, dispatch_state_ptr & state )
{
	variable_list arguments;
	for ( auto it = args.rbegin( ), end = args.rend( );
		  it < end;
		  ++it )
	{
		auto top = stack.top( );
		if ( top->is_ref( ) )
			arguments.emplace_back( new ep::variable( ( *it )->get_name( ), std::move( top->get_ref( ) ), ( *it )->is_reference( ) ) );
		else if ( top->is_variable( ) )
		{
			if ( ( *it )->is_reference( ) )
				arguments.emplace_back(
					new ep::variable(
					( *it )->get_name( ),
						state->get_variable( top->get_ref_name( ) )->get_ref( ),
						true,
						true ) );
			else
				arguments.emplace_back(
					new ep::variable(
					( *it )->get_name( ),
						std::move( state->get_variable( top->get_ref_name( ) )->get_ref( )->clone( ) ) ) );
		}
		stack.pop( );
	}
	return arguments;
}

