/********************************************************
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*				variable.cpp					*	*
*  	*			  Created by Yamiez					*	*
*  	*	Do not forget to give credit when using		*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*********************************************************/
#include "variable.h"
#include "value_reference.h"
#include "../ep.h"

ep::variable::variable( std::string name, value_reference_ptr reference )
	: name_( std::move( name ) ), vreference_( std::move( reference ) ), is_ref_( false ), lvalue_( false ),
	stolen_( false ), is_ret_( false )
{ }

ep::variable::variable( std::string name, value_reference_ptr reference, bool is_ref, bool is_lvalue, bool is_public )
	: name_( std::move( name ) ), vreference_( std::move( reference ) ), is_ref_( is_ref ), lvalue_( is_lvalue ),
	stolen_( false ), is_ret_( false ), is_public_( is_public )
{ }

ep::variable::variable( std::string name, value_reference_ptr reference, bool is_ref, bool is_lvalue )
	: name_( std::move( name ) ), vreference_( std::move( reference ) ), is_ref_( is_ref ), lvalue_( is_lvalue  ),
	stolen_( false ), is_ret_( false )
{ }

ep::variable::variable( std::string name, value_reference_ptr reference, bool is_ref )
	: name_( std::move( name ) ), vreference_( std::move( reference ) ), is_ref_( is_ref ), lvalue_( false ),
	stolen_( false ), is_ret_( false )
{ }

ep::variable::~variable( )
{ 
	if ( !stolen_ && !is_ref_ && !is_ret_ )
		destroy( );
}

const std::string & ep::variable::get_name( ) const
{
	return name_;
}

std::string &ep::variable::get_name( )
{
	return name_;
}

ep::value_reference_ptr ep::variable::get_ref( )
{
	return vreference_;
}

void ep::variable::set_ref( value_reference_ptr ref )
{ 
	vreference_ = std::move( ref );
}

ep::value_reference_ptr ep::variable::assign_ref( value_reference_ptr ref, dispatch_engine_ptr engine )
{
	if ( !vreference_->type_info( ) )
		vreference_->type_info( ) = ref->type_info( );
	auto method = vreference_->type_info( )->get_method( "=" );
	return method->invoke( { std::make_shared<variable>( method->get_arguments( ).front( )->get_name( ),
														  std::move( ref ),
														  method->get_arguments( ).front( )->is_reference( ),
														  true ),
							  std::make_shared<variable>( "this",
														  vreference_,
														  true,
														  true ) },
							engine );
}

void ep::variable::destroy( )
{
	vreference_ = nullptr;
}

bool ep::variable::match( variable_ptr other ) const
{
	return vreference_->type_info( )->bare_equal( *other->vreference_->type_info( ) );
}

bool ep::variable::is_reference( ) const
{
	return is_ref_;;
}

bool ep::variable::is_result( ) const
{
	return is_ret_;
}

bool ep::variable::stolen_rvalue( ) const
{
	return false;
}

bool ep::variable::is_rvalue( ) const
{
	return !lvalue_;
}

bool ep::variable::is_lvalue( ) const
{
	return lvalue_;
}

bool ep::variable::is_public( ) const
{
	return is_public_;
}

bool ep::variable::is_private( ) const
{
	return !is_public_;
}

bool ep::variable::is_dynamic( ) const
{
	return vreference_->is_dynamic();
}

void ep::variable::make_lvalue( )
{ 
	lvalue_ = true;
}

void ep::variable::make_rvalue( )
{ 
	lvalue_ = false;
}

void ep::variable::make_return( )
{ 
	is_ret_ = true;
}

void ep::variable::make_dynamic( )
{
	if ( vreference_ )
		vreference_->make_dynamic( );
}

void ep::variable::set_name( std::string name )
{ 
	name_ = std::move( name );
}

ep::value_reference_ptr ep::variable::steal( )
{
	if ( stolen_rvalue( ) )
		throw bad_vref_steal_error( name_ );
	stolen_ = true;
	return vreference_;
}

ep::variable_ptr ep::variable::copy( )
{
	return std::make_shared<variable>( "@", vreference_->clone( ) );
}

ep::variable_ptr ep::variable::ref_clone( )
{
	return std::make_shared<variable>( name_, vreference_, true, true );
}

ep::method_ptr ep::variable::get_method( const std::string & id )
{
	return vreference_->get_method( id );
}


