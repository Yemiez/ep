/********************************************************
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*				    method.h					*	*
*  	*			   Created by Yamiez				*	*
*  	*	Do not forget to give credit when using		*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*********************************************************/
#pragma once
#include "all_defines.h"

namespace ep
{

	class method
	{
	public:
		method( std::string name, method_argument_list args, statement_list statements );
		method( std::string name, method_argument_list args, std::function<value_reference_ptr( variable_list, dispatch_engine_ptr )> fun );

		const std::string &get_name( ) const;

		std::string &get_name( );

		const method_argument_list &get_arguments( ) const;

		method_argument_list &get_arguments( );

		size_t get_arguments_size( ) const;

		const statement_list &get_statements( ) const;

		statement_list &get_statements( );
		
		size_t get_statements_size( ) const;

		value_reference_ptr invoke( variable_list list, dispatch_engine_ptr engine );

		std::string pretty_format( ) const;

		std::function<value_reference_ptr( variable_list, dispatch_engine_ptr )> &get_fun( );

		void create_lambda( );

		void add_specialization( method_ptr method );

		void make_specialization( expression_ptr expr_spec );

		value_reference_ptr evaluate_expression( dispatch_engine_ptr engine, dispatch_state_ptr state );
	private:

	private:
		std::string name_;
		method_argument_list args_;
		statement_list statements_;
		std::function<value_reference_ptr( variable_list, dispatch_engine_ptr )> lambda_;
		expression_ptr expr_;
		std::vector<method_ptr> specializations_;
	};

}