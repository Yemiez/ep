/********************************************************
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*				    module.h					*	*
*  	*			   Created by Yamiez				*	*
*  	*	Do not forget to give credit when using		*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*********************************************************/
#pragma once
#include "all_defines.h"
#include "variable.h"
#include "value_reference.h"

namespace ep
{

	namespace details 
	{
		template<typename..._Params>
		struct _Variable_builder;

		template<typename _Next, typename ..._Params>
		struct _Variable_builder<_Next, _Params...>
		{
			static void _Build( variable_list &list,
								method_argument_list::iterator &arg,
								_Next &&next, 
								_Params&&...rest )
			{
				list.emplace_back( new variable( (*arg)->get_name( ), std::make_shared<value_reference>( std::forward<_Next>( next ) ) ) );
				_Variable_builder<_Params...>::_Build( list, ++arg, std::forward<_Params>( rest )... );
			}
		};

		template<typename ..._Params>
		struct _Variable_builder<variable_ptr, _Params...>
		{
			static void _Build( variable_list &list,
								method_argument_list::iterator &arg,
								variable_ptr next,
								_Params&&...rest )
			{
				list.emplace_back( new variable( ( *arg )->get_name( ), next->get_ref( )->clone( ) ) );
				_Variable_builder<_Params...>::_Build( list, ++arg, std::forward<_Params>( rest )... );
			}
		};

		template<typename ..._Params>
		struct _Variable_builder<variable_ptr&, _Params...>
		{
			static void _Build( variable_list &list,
								method_argument_list::iterator &arg,
								variable_ptr &next,
								_Params&&...rest )
			{
				list.emplace_back( new variable( ( *arg )->get_name( ), next->steal( ) ) );
				_Variable_builder<_Params...>::_Build( list, ++arg, std::forward<_Params>( rest )... );
			}
		};

		template<typename _Last>
		struct _Variable_builder<_Last>
		{
			static void _Build( variable_list &list,
								method_argument_list::iterator &arg,
								_Last &&last )
			{
				list.emplace_back( new variable( ( *arg )->get_name( ), std::make_shared<value_reference>( std::forward<_Last>( last ) ) ) );
			}
		};

		template<>
		struct _Variable_builder<variable_ptr&>
		{
			static void _Build( variable_list &list,
								method_argument_list::iterator &arg,
								variable_ptr &last )
			{
				list.emplace_back( new variable( ( *arg )->get_name( ), last->get_ref( ) ) );
			}
		};

		template<>
		struct _Variable_builder<variable_ptr>
		{
			static void _Build( variable_list &list,
								method_argument_list::iterator &arg,
								variable_ptr last )
			{
				list.emplace_back( new variable( ( *arg )->get_name( ), last->get_ref( )->clone( ) ) );
			}
		};

		template<>
		struct _Variable_builder<>
		{
			static void _Build( variable_list &list,
								method_argument_list::iterator &arg )
			{ }
		};

		template<typename T>
		struct _Function_builder;

		template<typename _Ret, typename ..._Params>
		struct _Function_builder<_Ret( _Params... )>
		{
			using fn_type = std::function<_Ret( _Params... )>;
			static fn_type
				_Build( method_ptr method, dispatch_engine_ptr engine )
			{
				return [method, engine]( _Params&&...args )->_Ret
				{
					variable_list arguments;
					_Variable_builder<_Params...>::_Build( arguments,
														   method->get_arguments( ).begin( ),
														   std::forward<_Params>( args )... );
					auto result = method->get_fun( )( std::move( arguments ), engine );
					return result->assume<_Ret>( );
				};
			}
		};

		template<typename..._Params>
		struct _Function_builder<void( _Params... )>
		{
			using fn_type = std::function<void( _Params... )>;
			static fn_type
				_Build( method_ptr method, dispatch_engine_ptr engine )
			{
				return [method, engine]( _Params&&...args )->void
				{
					variable_list arguments;
					_Variable_builder<_Params...>::_Build( arguments,
														   method->get_arguments( ).begin( ),
														   std::forward<_Params>( args )... );
					method->get_fun( )( std::move( arguments ), engine );
				};
			}
		};

		template<typename..._Params>
		struct _Function_builder<value_reference( _Params... )>
		{
			using fn_type = std::function<value_reference( _Params... )>;
			static fn_type
				_Build( method_ptr method, dispatch_engine_ptr engine )
			{
				return [method, engine]( _Params&&...args )
				{
					variable_list arguments;
					_Variable_builder<_Params...>::_Build( arguments,
														   method->get_arguments( ).begin( ),
														   std::forward<_Params>( args )... );
					return method->get_fun( )( std::move( arguments ), engine );
				};
			}
		};

		template<typename _Sig>
		struct _Method_caller;

		template<typename _Ret, typename ..._Params>
		struct _Method_caller<_Ret( _Params... )>
		{
			static _Ret _Do_call( method_ptr method, dispatch_engine_ptr engine, _Params&&...params )
			{
				variable_list arguments;
				_Variable_builder<_Params...>::_Build( arguments,
													   method->get_arguments( ).begin( ),
													   std::forward<_Params>( params )... );
				auto result = method->get_fun( )( arguments, engine );
				return result->assume<_Ret>( );
			}
		};

		template<typename ..._Params>
		struct _Method_caller<void( _Params... )>
		{
			static void _Do_call( method_ptr method, dispatch_engine_ptr engine, _Params&&...params )
			{
				variable_list arguments;
				_Variable_builder<_Params...>::_Build( arguments,
													   method->get_arguments( ).begin( ),
													   std::forward<_Params>( params )... );
				method->get_fun( )( arguments, engine );
			}
		};

		template<typename ..._Params>
		struct _Method_caller<value_reference( _Params... )>
		{
			static value_reference _Do_call( method_ptr method, dispatch_engine_ptr engine, _Params&&...params )
			{
				variable_list arguments;
				_Variable_builder<_Params...>::_Build( arguments,
													   method->get_arguments( ).begin( ),
													   std::forward<_Params>( params )... );
				return method->get_fun( )( arguments, engine );
			}
		};
	}


	class module
	{
	public:
		friend class expression;

	public:
		module( );
		module( plugin_list plugins );
		~module( );

		statement_list compile( std::string contents );

		statement_list compile_file( const std::string &file );

		void eval( statement_list &statements );
		
		void eval( std::string contents );

		void eval_file( std::string file );

		dispatch_engine_ptr get_engine( );

		method_list &get_methods( );

		method_ptr get_method( const std::string &id );

		void add_method( method_ptr method );

		variable_list get_globals( );

		variable_ptr get_global( const std::string &id );

		void add_global( variable_ptr ptr );

		template<typename Sig>
		typename details::_Function_builder<Sig>::fn_type 
			get_method_object( const std::string &id )
		{
			auto method = get_method( id );
			return details::_Function_builder<Sig>::_Build( method, engine_ );
		}

		template<typename Ret, typename ...Params>
		Ret call_method( const std::string &id, Params&&...params )
		{
			auto method = get_method( id );
			return details::_Method_caller<Ret( Params... )>::_Do_call( method, engine_, std::forward<Params>( params )... );
		}

		void add_lib_fun( std::string name, std::function<void( dispatch_engine_ptr )> fun );

		bool is_included( const std::string &file ) const;

	private:

		statement_list deobfuscate_contents( std::string &contents );

		bool obfuscation_algorithm( std::string &contents );
		
		void fix_block( method_argument_list &members, statement_list &list ) const;

		void constructor_fix_statements( method_ptr ctor, 
										 method_argument_list members, 
										 method_ptr &method,
										 tokenizer_it where ) const;

		std::function<value_reference_ptr( value_reference * )> make_clone( method_list &findables ) const;

		ep::method_ptr make_constructor( const tokenizer_it &id ) const;

		ep::method_ptr make_destructor( const tokenizer_it &id ) const;


		statement_list parse( tokenizer_it curr, tokenizer_it end );

		void internal_eval( statement_list &block );

		bool is_method( tokenizer_it &curr, const tokenizer_it &end ) const;

		bool is_group( tokenizer_it &curr, const tokenizer_it &end ) const;

		bool is_specialize( tokenizer_it &curr, const tokenizer_it &end ) const;

		ep::type_info_ptr parse_group( tokenizer_it &curr, const tokenizer_it &end ) const;

		ep::method_ptr parse_constructor( const tokenizer_it &group, tokenizer_it &curr, const tokenizer_it &end ) const;

		ep::method_ptr parse_destructor( const tokenizer_it &group, tokenizer_it &curr, const tokenizer_it &end ) const;

		void parse_import( tokenizer_it &curr, const tokenizer_it &end, statement_list &output );

		ep::method_ptr parse_specialize( const tokenizer_it &group,
										 tokenizer_it &curr,
										 const tokenizer_it &end ) const;

		method_ptr parse_method( const tokenizer_it &group, 
								 tokenizer_it &curr, 
								 const tokenizer_it &end ) const;

		void parse_statement( tokenizer_it &curr, const tokenizer_it &end, statement_list &output );

		statement_list parse_method_body( const tokenizer_it &group, 
										  const tokenizer_it &method, 
										  tokenizer_it &curr, 
										  const tokenizer_it &end ) const;

		std::vector<std::pair<std::string, std::function<void( dispatch_engine_ptr )>>>::iterator find_import_fun( const std::string &id );

		std::vector<std::pair<std::string, std::function<void( dispatch_engine_ptr )>>>::iterator bad_import_fun( );


	private:
		std::vector<std::string> includes_, libs_;
		optimizer_ptr optimizer_;
		dispatch_engine_ptr engine_, cfolder_;
		std::vector<std::pair<std::string, std::function<void( dispatch_engine_ptr )>>> import_funs_;
	};

}