/********************************************************
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*			   value_reference.h				*	*
*  	*			   Created by Yamiez				*	*
*  	*	Do not forget to give credit when using		*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*********************************************************/
#pragma once
#include "all_defines.h"
#include "eval_error.h"
#include <typeinfo>

namespace ep
{

	class value_reference
	{
	public:
		value_reference( );
		value_reference( void *ptr, type_info_ptr ti, bool destruct = true );
		template<typename T>
		value_reference( T *ptr, type_info_ptr ti, bool destruct = true )
			: ati_( &typeid( T ) ), ti_( std::move( ti ) ), ptr_( ptr ), destruct_( destruct ), init_( true ),
			size_( sizeof( T ) ), is_dyn_( false )
		{}
		
		~value_reference( );

		template<typename T>
		T *get_pointer( ) const
		{
			if ( ati_->hash_code( ) != typeid( T ).hash_code( ) )
				throw bad_value_cast_error( ati_, &typeid( T ) );
			return reinterpret_cast< T* > ( ptr_ );
		}

		template<typename T>
		T *get_pointer_unsafe( ) const
		{
			return reinterpret_cast< T* > ( ptr_ );
		}

		template<>
		void *get_pointer<void>( ) const
		{
			return ptr_;
		}

		void set_pointer( void *ptr );

		template<typename T>
		void set_pointer( T *ptr )
		{
			if ( ptr_ && ti_ && destruct_ )
				ti_->get_dtor( )( this );
			size_ = sizeof( T );
			ati_ = &typeid( T );
			ptr_ = ptr;
		}

		bool is_uninitalized( ) const;

		bool is_string( ) const;

		bool is_floating( ) const;

		bool is_integral( ) const;

		bool is_dynamic( ) const;

		bool is_anonymous_method( ) const;

		size_t get_type_size( ) const;

		void make_anonymous_method( ep::method_ptr method );

		value_reference_ptr clone( ); // Always call this one

		type_info_ptr &type_info( );

		method_ptr get_method( const std::string &id );

		method_ptr get_anonymous_method( );

		variable_ptr get_member( const std::string &id );

		variable_list &get_members( );

		method_list &get_methods( );

		void make_dynamic( );

		static void assign_refs( value_reference_ptr &vreference, 
								 value_reference_ptr &other,
								 dispatch_engine_ptr engine );

	private:
		const std::type_info *ati_;
		variable_list members_;
		type_info_ptr ti_;
		method_ptr internal_;
		void *ptr_;
		bool destruct_, init_, is_dyn_;
		size_t size_;
	};

}