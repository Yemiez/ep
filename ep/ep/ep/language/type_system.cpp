#include "type_system.h"
#include "eval_error.h"
#include "method.h"
#include "method_argument.h"
#include "variable.h"
#include "value_reference.h"

ep::type_info::type_info( std::string name, 
						  std::function<void( value_reference* )> dtor, 
						  std::function<value_reference_ptr( value_reference * )> clone, 
						  method_list methods,
						  method_argument_list members )
	: name_( std::move( name ) ),
	dtor_( std::move( dtor ) ),
	clone_( std::move( clone ) ),
	methods_( std::move( methods ) ),
	members_( std::move( members ) )
{ }

const std::string & ep::type_info::get_name( ) const
{
	return name_;
}

std::string & ep::type_info::get_name( )
{
	return name_;
}

const std::function<void( ep::value_reference* )> & ep::type_info::get_dtor( ) const
{
	return dtor_;
}

std::function<void( ep::value_reference* )> & ep::type_info::get_dtor( )
{
	return dtor_;
}

const std::function<ep::value_reference_ptr( ep::value_reference* )>& ep::type_info::get_clone( ) const
{
	return clone_;
}

std::function<ep::value_reference_ptr( ep::value_reference* )>& ep::type_info::get_clone( )
{
	return clone_;
}

const ep::method_list & ep::type_info::get_methods( ) const
{
	return methods_;
}

ep::method_list & ep::type_info::get_methods( )
{
	return methods_;
}

ep::method_ptr ep::type_info::get_method( const std::string & id )
{
	auto it = find_method( id );
	return it != bad_method_it( ) ?
		*it :
		throw undefined_identifier_error( id );
}

bool ep::type_info::has_method( const std::string & id )
{
	for ( auto &x : methods_ )
		if ( x->get_name( ) == id )
			return true;
	return false;
}

void ep::type_info::add_method( method_ptr method )
{ 
	if ( find_method( method->get_name( ) ) != bad_method_it( ) )
		throw symbol_collision_error( this,
									  method,
									  "" );
	methods_.emplace_back( std::move( method ) );
}

ep::variable_list ep::type_info::create_members( ) const
{
	variable_list list;
	for ( auto &x : members_ )
		list.emplace_back( new variable( x->get_name( ),
										 std::make_shared<value_reference>( ),
										 x->is_reference( ),
										 true,
										 x->is_public( ) ) );
	return list;
}

ep::method_argument_list ep::type_info::get_members( )
{
	return members_;
}

bool ep::type_info::bare_equal( const type_info &other ) const
{
	return name_ == other.name_;
}

bool ep::type_info::operator==( const type_info &other ) const
{
	return bare_equal( other );
}

bool ep::type_info::operator!=( const type_info &other ) const
{
	return !bare_equal( other );
}

ep::method_list::const_iterator ep::type_info::find_method( const std::string &id ) const
{
	for ( auto it = methods_.begin( ), end = methods_.end( );
		  it < end;
		  ++it )
		if ( ( *it )->get_name( ) == id )
			return it;
	return bad_method_it( );
}

ep::method_list::iterator ep::type_info::find_method( const std::string &id )
{
	for ( auto it = methods_.begin( ), end = methods_.end( );
		  it < end;
		  ++it )
		if ( ( *it )->get_name( ) == id )
			return it;
	return bad_method_it( );
}

ep::method_list::const_iterator ep::type_info::bad_method_it( ) const
{
	return methods_.end( );
}

ep::method_list::iterator ep::type_info::bad_method_it( )
{
	return methods_.end( );
}