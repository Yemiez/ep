/********************************************************
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*				 statement.cpp					*	*
*  	*			   Created by Yamiez				*	*
*  	*	Do not forget to give credit when using		*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*********************************************************/
#include "statement.h"
#include "expression.h"
#include "eval_error.h"
#include "variable.h"
#include "value_reference.h"
#include "dispatch_state.h"
#include "method.h"
#include "dispatch_engine.h"
#include "debugger.h"
#include "expression_action.h"
#include "libs.h"
#include <algorithm>

ep::statement::statement( std::vector<tokenizer_it> refs, 
						  tokenizer_it & current, 
						  const tokenizer_it & end, 
						  dispatch_engine_ptr engine, 
						  statement *parent, 
						  bool w )
	: expr_( nullptr ), flag_( -1 ), parent_( parent ), return_( std::make_shared<value_reference>( ) ), 
	while_( w ), this_access_lhs_( false ), refs_( std::move( refs ) ), begin_( current ), is_dyn_( false )
{ 
	parse( current, end, engine );
}

bool ep::statement::eval( dispatch_engine_ptr engine, dispatch_state_ptr state, debugger_ptr debugger )
{
	if ( flag_ == flags::function_call_flag )
		expr_->evaluate_actions( std::move( engine ), std::move( state ) );
	else if ( flag_ == flags::expression_var_decl_flag )
		state->add_variable( std::move( std::make_shared<variable>(
			lhsv_,
			expr_->evaluate_actions(
				std::move( engine ),
				state ),
			false,
			true ) ) );
	else if ( flag_ == flags::var_decl_flag )
	{
		auto var = std::make_shared<variable>( lhsv_, std::make_shared<value_reference>( ), false, true );
		if ( is_dyn_ )
			var->make_dynamic( );
		state->add_variable( std::move( var ) );
		
	}
	else if ( flag_ == flags::empty_expression_flag )
		return false;
	else if ( flag_ == flags::break_flag )
		throw break_exception( );
	else if ( flag_ == flags::continue_flag )
		throw continue_exception( );
	else if ( flag_ == flags::infinite_loop )
	{
		auto wstate = engine->create_new_dispatch_state( "while_statement", state );
		try
		{
			while ( true )
			{
				wstate->reset( );
				try
				{
					for ( auto &x : dups_ )
					{
						if ( debugger )
							debugger->step_into( x, wstate );
						if ( x->eval_or_eval_and_return( engine, wstate, debugger ) )
						{
							return_ = x->get_return( );
							return true;
						}
					}
				}
				catch ( continue_exception & )
				{ }
			}
		}
		catch ( break_exception & )
		{
		}
	}
	else if ( flag_ == flags::while_flag )
	{
		auto wstate = engine->create_new_dispatch_state( "while_statement", state );
		try
		{
			while ( *expr_->evaluate_actions( engine, state )->get_pointer<int>( ) )
			{
				wstate->reset( );
				try
				{
					for ( auto &x : dups_ )
					{
						if ( debugger )
							debugger->step_into( x, wstate );
						if ( x->eval_or_eval_and_return( engine, wstate, debugger ) )
						{
							return_ = x->get_return( );
							return true;
						}
					}
				}
				catch ( continue_exception & )
				{
					continue;
				}
			}
		}
		catch ( break_exception & )
		{}
	}
	else if ( flag_ == flags::if_flag )
	{
		auto ifstate = engine->create_new_dispatch_state( "if_statement", state );
		if ( *expr_->evaluate_actions( engine, state )->get_pointer<int>( ) )
		{
			for ( auto &x : dups_ )
			{
				if ( debugger )
					debugger->step_into( x, ifstate );
				if ( x->eval_or_eval_and_return( engine, ifstate, debugger ) )
				{
					return_ = x->get_return( );
					return true;
				}
			}
		}
		else if ( else_ )
		{
			if ( debugger )
				debugger->step_into( else_, state );
			return else_->eval( std::move( engine ), std::move( state ), std::move( debugger ) );
		}
	}
	else if ( flag_ == flags::else_if_flag )
	{
		auto eifstate = engine->create_new_dispatch_state( "else_if_statement", state );
		if ( *expr_->evaluate_actions( engine, state )->get_pointer<int>( ) )
		{
			for ( auto &x : dups_ )
			{
				if ( debugger )
					debugger->step_into( x, eifstate );
				if ( x->eval_or_eval_and_return( engine, eifstate, debugger ) )
				{
					recursive_set_return( x->get_return( ) );
					return true;
				}
			}
		}
		else if ( else_ )
		{
			if ( debugger )
				debugger->step_into( else_, state );
			return else_->eval( std::move( engine ), std::move( state ), std::move( debugger ) );
		}
	}
	else if ( flag_ == flags::else_flag )
	{
		auto estate = engine->create_new_dispatch_state( "else_statement", state );
		for ( auto &x : dups_ )
		{
			if ( debugger )
				debugger->step_into( x, estate );
			if ( x->eval_or_eval_and_return( engine, estate, debugger ) )
			{
				recursive_set_return( x->get_return( ) );
				return true;
			}
		}
	}
	else if ( flag_ == flags::evaluate_expression_flag || flag_ == flags::after_added_ref )
		expr_->evaluate_actions( std::move( engine ), std::move( state ) );
	return false;
}

bool ep::statement::eval_or_eval_and_return( dispatch_engine_ptr engine, dispatch_state_ptr state, debugger_ptr debugger )
{
	if ( flag_ == flags::return_flag )
	{
		// Make sure to actually SET the reference. Fuck sake.
		// important fucking note, Oskar: DO NOT USE get_ref() TO ASSIGN THE RVALUE RETURN, IT IS NOT A REFERENCE SO YOU'LL SPEND LITTERALLY
		// 3 HOURS FIXING A RETARDED ASS BUG. THANKS FOR NOT BEING A RETARD IN THE FUTURE OSKAR.
		return_ = std::move( expr_->evaluate_actions( std::move( engine ), std::move( state ), true ) );
		return true;
	}
	return eval( std::move( engine ), std::move( state ), std::move( debugger ) );
}

ep::value_reference_ptr ep::statement::get_return( )
{
	return return_;
}

bool ep::statement::is_return( ) const
{
	return flag_ == flags::return_flag;
}

bool ep::statement::is_empty_expression( ) const
{
	return flag_ == flags::empty_expression_flag;
}

bool ep::statement::is_function_call( ) const
{
	return flag_ == flags::function_call_flag;
}

bool ep::statement::is_var_decl( ) const
{
	return flag_ == flags::var_decl_flag;
}

bool ep::statement::is_var_decl_initalizer( ) const
{
	return flag_ == flags::expression_var_decl_flag;
}

bool ep::statement::is_constexpr( ) const
{
	return expr_ && expr_->is_constexpr( );
}

bool ep::statement::is_constant( ) const
{
	return expr_->get_actions_size( ) == 1 && is_constexpr( );
}

bool ep::statement::is_while( ) const
{
	return flag_ == flags::while_flag;
}

bool ep::statement::is_if( ) const
{
	return flag_ == flags::if_flag;
}

bool ep::statement::is_else_if( ) const
{
	return flag_ == flags::else_if_flag;
}

bool ep::statement::is_else( ) const
{
	return flag_ == flags::else_flag;
}

bool ep::statement::is_block_type( ) const
{
	return is_while( ) || is_if( ) || is_else( ) || is_else_if( );
}

bool ep::statement::is_var_modify( ) const
{
	return flag_ == expression_var_modify || ( flag_ == evaluate_expression_flag && !lhsv_.empty( ) );
}

bool ep::statement::is_inside_of_while( ) const
{
	return while_ || (parent_ ? parent_->is_inside_of_while( ) : false);
}

bool ep::statement::is_this_assignment( ) const
{
	return this_access_lhs_;
}

bool ep::statement::lhsv( const std::string & x ) const
{
	return lhsv_ == x;
}

bool ep::statement::is_expression( ) const
{
	return flag_ == evaluate_expression_flag;
}

bool ep::statement::has_else_statement( ) const
{
	return else_ != nullptr;
}

bool ep::statement::is_break_statement( ) const
{
	return flag_ == flags::break_flag;
}

bool ep::statement::is_continue_statement( ) const
{
	return flag_ == flags::continue_flag;
}

bool ep::statement::has_always_break_statement( ) const
{
	for ( auto &x : dups_ )
		if ( x->is_break_statement( ) )
			return true;
	return false;
}

const std::string & ep::statement::get_var_decl( ) const
{
	return lhsv_;
}

bool ep::statement::this_expr_is_mem_access_of( const std::string & x ) const
{
	if ( !expr_ )
		return false;
	if ( expr_->get_actions_size( ) <= 3 )
		return false;
	if ( expr_->get_actions( ).at( 2 )->get_ref_name( ) != "this" )
		return false;
	return expr_->get_actions( ).at( 1 )->is_variable( ) && 
		expr_->get_actions( ).at( 1 )->get_ref_name( ) == x;
}

ep::expression_ptr ep::statement::get_expression( ) const
{
	return expr_;
}

void ep::statement::set_expression( expression_ptr expr )
{ 
	expr_.swap( std::move( expr ) );
}

void ep::statement::make_infinite_loop( )
{ 
	flag_ = flags::infinite_loop;
	expr_.reset( );
}

void ep::statement::make_if_statement( )
{ 
	flag_ = flags::if_flag;
}

void ep::statement::make_var_decl_initalizer( )
{ 
	flag_ = flags::expression_var_decl_flag;
}

void ep::statement::recursive_set_return( value_reference_ptr & ptr )
{
	if ( parent_ )
		parent_->recursive_set_return( ptr );
	else
		return_ = ptr;
}

const ep::statement_list & ep::statement::get_dups( ) const
{
	return dups_;
}

ep::statement_list & ep::statement::get_dups( )
{
	return dups_;
}

ep::statement_ptr ep::statement::get_else_statement( )
{
	return else_;
}

std::string ep::statement::pretty_format( ) const
{
	std::stringstream ss;
	switch ( flag_ )
	{
		case flags::else_flag:
			ss << "else" << std::endl;
			break;
		case flags::else_if_flag:
			ss << "else if " << expr_->pretty_format( ) << std::endl;
			break;
		case flags::empty_expression_flag:
		case flags::evaluate_expression_flag:
			ss << "[expr] " << expr_->pretty_format( ) << std::endl;
			break;
		case flags::expression_var_decl_flag:
			ss << "[vstore] auto " << lhsv_ << " " << lhsop_ << " " << expr_->pretty_format( ) << std::endl;
			break;
		case flags::expression_var_modify:
			ss << "[vmodify] " << lhsv_ << " " << lhsop_ << " " << expr_->pretty_format( ) << std::endl;
			break;
		case flags::function_call_flag:
			ss << "[fcall]" << expr_->pretty_format( ) << std::endl;
			break;
		case flags::if_flag:
			ss << "if " << expr_->pretty_format( ) << std::endl;
			break;
		case flags::return_flag:
			ss << "return " << expr_->pretty_format( ) << std::endl;
			break;
		case flags::var_decl_flag:
			ss << "[vstore=[uninitalize]] var " << lhsv_ << ";" << std::endl;
			break;
		case flags::while_flag:
			ss << "while " << expr_->pretty_format( ) << std::endl;
			break;
		default:
			break;
	}
	return ss.str( );
}

void ep::statement::make_ref_assign( )
{
	flag_ = after_added_ref;
}

void ep::statement::set_var_decl( )
{
	if ( expr_ && expr_->get_actions_size( ) >= 3 )
	{
		auto actions = expr_->get_actions( );
		if ( !actions.at( 0 )->is_variable( ) )
			return;
		if ( !actions.back( )->is_method( ) )
			return;
		auto back_ref = actions.back( )->get_ref_name( );
		if ( std::count_if( back_ref.begin( ), back_ref.end( ), []( auto &x )
		{
			return x == '=';
		} ) != 1 )
			return;
		lhsv_ = actions.at( 0 )->get_ref_name( );
	}
}

void ep::statement::parse( tokenizer_it & current, const tokenizer_it & end, dispatch_engine_ptr &engine )
{
	if ( current->is_identifier( ) )
	{
		if ( current->get_block( ) == "return" )
		{
			flag_ = flags::return_flag;
			this->expr_ = std::make_shared<expression>( refs_, ++current, end, engine );
		}
		else if ( current->get_block( ) == "var" || 
				  current->get_block( ) == "auto" || 
				  current->get_block() == "dyn" )
			return parse_var( ++current, 
							  end, 
							  engine, 
							  current->get_block( ) == "auto", 
							  current->get_block() == "dyn" );
		else if ( current->get_block( ) == "while" )
		{
			flag_ = flags::while_flag;
			return parse_while( ++current, end, engine );
		}
		else if ( current->get_block( ) == "if" )
		{
			flag_ = flags::if_flag;
			return parse_if( ++current, end, engine );
		}
		else if ( current->get_block( ) == "else" )
			return parse_else( ++current, end, engine );
		else if ( current->get_block( ) == "continue" )
		{
			if ( is_inside_of_while( ) )
				throw misusage_continue_error( refs_, current, end );

			flag_ = continue_flag;
			if ( ( ++current )->get_block( ) != ";" )
				throw expected_semicolon_error( refs_, current, end );
			++current;
		}
		else if ( current->get_block( ) == "break" )
		{
			if ( is_inside_of_while( ) )
				throw misusage_break_error( refs_, current, end );

			flag_ = break_flag;
			if ( ( ++current )->get_block( ) != ";" )
				throw expected_semicolon_error( refs_, current, end );
			++current;
		}
		else
			return parse_optional_identifier( current, end, engine );
	}
	else if ( current->is_decimal( ) || current->is_hexadecimal( ) ) // Empty expression, not needed /ayo/
	{
		flag_ = flags::empty_expression_flag; // OTPMIZEIRMEITET)=?)?
		this->expr_ = std::make_shared<expression>( refs_, current, end, engine );
		if ( expr_->has_fcalls( ) )
		{
			flag_ = flags::evaluate_expression_flag;
			set_var_decl( );
		}
	}
	else if ( current->get_block( ) == ";" )
	{
		flag_ = flags::empty_expression_flag;
		++current;
	}
	else
		throw unknown_token_error( refs_, current, end );
}

void ep::statement::parse_var( tokenizer_it & current, 
							   const tokenizer_it & end, 
							   dispatch_engine_ptr &engine, 
							   bool is_auto,
							   bool is_dyn )
{ 
	if ( !current->is_identifier( ) )
		throw expected_identifier_error( refs_, current, end );
	
	auto id = current++;
	auto refs = refs_;
	refs.emplace_back( id );

	if ( id->get_block( ) == "this" )
		throw this_not_allowed_as_identifier_error( refs_, current, end );

	if ( current->get_block( ) == ";" )
	{
		if ( is_auto )
			throw cannot_deduce_auto_type_error( refs, current, end );
		flag_ = flags::var_decl_flag;
		is_dyn_ = is_dyn;
		lhsv_ = id->get_block( );
		++current;
	}
	else if ( current->is_operator( ) )
	{
		if ( is_dyn )
			throw dyanamic_object_no_initialization( refs, current, end );
		
		if ( current->get_block( ) == "(" )
			throw expected_expression_error( refs, current, end );
		else if ( current->get_block( ) == "," )
		{
			auto block = current->get_block( );
			while ( current < end )
			{
				if ( current->is_identifier( ) )
				{

				}
			}
		}
		else
		{
			if ( is_dyn )
				throw dyanamic_object_no_initialization( refs, current, end );

			if ( current->is_assignment_operator( ) )
			{
				flag_ = flags::expression_var_decl_flag;
				lhsv_ = id->get_block( );
				lhsop_ = ( current++ )->get_block( );
				expr_ = std::make_shared<expression>( refs_, current, end, engine );
			}
			else
				throw expected_semicolon_error( refs, current, end );
		}
	}
	else
		throw expected_expression_error( refs, current, end );
}

void ep::statement::parse_optional_identifier( tokenizer_it & current, const tokenizer_it & end, dispatch_engine_ptr &engine )
{
	auto id = current++;

	if ( !( current < end ) )
		throw ran_out_of_tokens_error( refs_, --current, end );

	if ( !current->is_operator( ) )
		throw expected_expression_error( refs_, current, end );

	if ( current->get_block( ) == ";" )
	{
		flag_ = flags::empty_expression_flag;
		++current;
	}
	else if ( current->get_block( ) == "(" )
	{
		flag_ = flags::function_call_flag;
		expr_ = std::make_shared<expression>( refs_, --current, end, engine );
	}
	else
	{
		expr_ = std::make_shared<expression>( refs_, --current, end, engine );
		if ( expr_->has_fcalls( ) )
		{
			flag_ = flags::evaluate_expression_flag;
			set_var_decl( );
		}
		else
			flag_ = flags::empty_expression_flag;
		return;
	}
}

void ep::statement::parse_while( tokenizer_it & current, const tokenizer_it & end, dispatch_engine_ptr &engine )
{ 
	if ( current->get_block( ) != "(" )
		throw expected_expression_error( refs_, current, end );

	expr_ = std::make_shared<expression>( refs_, current, end, engine, true );

	if ( current->get_block( ) != "{" )
		dups_.emplace_back( new statement( refs_, current, end, engine, this ) );
	else
	{
		++current;
		while ( true )
		{
			if ( !( current < end ) )
				throw ran_out_of_tokens_error( refs_, --current, end );
			if ( current->get_block( ) == "}" )
			{
				++current;
				break;
			}
			else
				dups_.emplace_back( new statement( refs_, current, end, engine, this ) );
		}
	}
}

void ep::statement::parse_if( tokenizer_it & current, const tokenizer_it & end, dispatch_engine_ptr &engine )
{
	if ( current->get_block( ) != "(" )
		throw expected_expression_error( refs_, current, end );

	expr_ = std::make_shared<expression>( refs_, current, end, engine, true );
	
	if ( current->get_block( ) != "{" )
		dups_.emplace_back( new statement( refs_, current, end, engine, this, is_inside_of_while( ) ) );
	else
	{
		++current;
		while ( true )
		{
			if ( !( current < end ) )
				throw ran_out_of_tokens_error( refs_, --current, end );
			if ( current->get_block( ) == "}" )
			{
				++current;
				break;
			}
			else
				dups_.emplace_back( new statement( refs_, current, end, engine, this, is_inside_of_while( ) ) );
		}
	}

	if ( current < end && current->get_block( ) == "else" )
		else_ = std::make_shared<statement>( refs_, current, end, engine, this, is_inside_of_while( ) );
}

void ep::statement::parse_else( tokenizer_it & current, const tokenizer_it & end, dispatch_engine_ptr &engine )
{
	// Else if
	bool one_liner = false;
	if ( current->get_block( ) == "if" )
	{
		if ( ( ++current )->get_block( ) != "(" )
			throw expected_expression_error( refs_, current, end );
		expr_ = std::make_shared<expression>( refs_, current, end, engine, true );
		flag_ = flags::else_if_flag;

		if ( current->get_block( ) != "{" )
		{
			one_liner = true;
			dups_.emplace_back( new statement( refs_, current, end, engine, this, is_inside_of_while( ) ) );
		}
		else
			++current;
	}
	else
	{
		flag_ = flags::else_flag;
		if ( current->get_block( ) != "{" )
		{
			dups_.emplace_back( new statement( refs_, current, end, engine, this, is_inside_of_while( ) ) );
			one_liner = true;
		}
		else
			++current;
	}

	while ( true && !one_liner )
	{
		if ( !( current < end ) )
			throw ran_out_of_tokens_error( refs_, --current, end );
		if ( current->get_block( ) == "}" )
		{
			++current;
			break;
		}
		else
			dups_.emplace_back( new statement( refs_, current, end, engine, this, is_inside_of_while( ) ) );
	}

	if ( is_else_if( ) && current->get_block( ) == "else" )
		else_ = std::make_shared<statement>( refs_, current, end, engine, this, is_inside_of_while( ) );
}
