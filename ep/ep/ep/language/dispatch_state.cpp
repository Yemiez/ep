/********************************************************
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*			  dispatch_state.cpp					*	*
*  	*			   Created by Yamiez				*	*
*  	*	Do not forget to give credit when using		*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*********************************************************/
#include "dispatch_state.h"
#include "variable.h"
#include "value_reference.h"
#include "method_argument.h"
#include "method.h"
#include "dispatch_engine.h"
#include "eval_error.h"

using namespace ep;

ep::dispatch_state::dispatch_state( variable_list arguments, dispatch_engine * engine, dispatch_state_ptr parent )
	: locals_( std::move( arguments ) ), engine_( engine ), parent_( std::move( parent ) )
{
	for ( auto &x : locals_ )
		if ( x->get_name( ) == "this" )
			thisptr_ = x;
}

ep::dispatch_state::dispatch_state( variable_list arguments, dispatch_engine *engine )
	: locals_( std::move( arguments ) ), engine_( engine ), parent_( engine ? engine->get_global_state( ) : nullptr )
{
	for ( auto &x : locals_ )
		if ( x->get_name( ) == "this" )
			thisptr_ = x;
}

ep::dispatch_state::dispatch_state( dispatch_state_ptr parent, dispatch_engine * engine )
	: parent_( std::move( parent ) ), engine_( engine ), locals_( )
{ }

ep::dispatch_state::~dispatch_state( )
{

}

variable_list &ep::dispatch_state::get_variables( )
{
	return locals_;
}

variable_ptr ep::dispatch_state::get_variable( const std::string & id )
{
	auto it = find_local( id );
	if ( it == bad_it( ) )
	{
		try
		{
			if ( thisptr_ )
				return thisptr_->get_ref( )->get_member( id );
		}
		catch ( ep::ep_error & )
		{}
		return parent_ ? 
		parent_->get_variable( id ) : 
		throw undefined_identifier_error( id );
	}
	return *it;
}

variable_ptr ep::dispatch_state::get_this( )
{
	for ( auto &x : this->locals_ )
		if ( x->get_name( ) == "this" )
			return x;
	return parent_ ?
		parent_->get_this( ) :
		nullptr;
}

method_ptr ep::dispatch_state::get_method( const std::string & id )
{
	return engine_->get_method( id );
}

void ep::dispatch_state::add_variable( variable_ptr var )
{ 
	if ( find_local( var->get_name( ) ) != bad_it( ) )
		throw symbol_collision_error( nullptr,
									  nullptr,
									  var->get_name( ) );
	locals_.emplace_back( std::move( var ) );
}

dispatch_engine *ep::dispatch_state::get_engine( )
{
	return engine_;
}

void ep::dispatch_state::reset( )
{ 
	locals_.clear( );
}

std::string ep::dispatch_state::pretty_format( ) const
{
	std::stringstream ss;
	if ( parent_ )
		ss << parent_->pretty_format( );
	for ( auto &x : locals_ )
		ss << x->get_name( ) << std::endl;
	ss << std::endl;
	return ss.str( );
}

variable_list::reverse_iterator ep::dispatch_state::find_local( const std::string & id )
{
	for ( auto it = locals_.rbegin( ), end = locals_.rend( );
		  it < end;
		  ++it )
		if ( ( *it )->get_name( ) == id )
			return it;
	return bad_it( ); // Makes more sense, visually, not logically.
}

variable_list::reverse_iterator ep::dispatch_state::bad_it( )
{
	return locals_.rend( );
}
