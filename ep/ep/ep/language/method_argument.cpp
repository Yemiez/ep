/********************************************************
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*			  method_argument.cpp				*	*
*  	*			   Created by Yamiez				*	*
*  	*	Do not forget to give credit when using		*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*********************************************************/
#include "method_argument.h"

ep::method_argument::method_argument( std::string id, bool is_ref, bool is_pub )
	: id_( std::move( id ) ), is_reference_( is_ref ), is_public_( is_pub )
{ }

const std::string & ep::method_argument::get_name( ) const
{
	return id_;
}

std::string & ep::method_argument::get_name( )
{
	return id_;
}

bool ep::method_argument::is_reference( ) const
{
	return is_reference_;
}

bool ep::method_argument::is_public( ) const
{
	return is_public_;
}

bool ep::method_argument::is_private( ) const
{
	return !is_public_;
}
