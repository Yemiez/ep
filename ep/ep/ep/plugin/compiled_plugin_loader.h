#pragma once
#include "../language/all_defines.h"
#include <random>

namespace ep
{

	class compiled_plugin_loader
	{
	public:
		compiled_plugin_loader( std::vector<std::string> modules );

		plugin_list get_plugins( );

	private:
		std::string random_sequence( );

		std::vector<std::string> create_temporaries( std::vector<std::string> modules );

		std::string write_temporary( std::string &data );

	private:
		std::vector<std::string> temporaries_;
		std::random_device device_;
		std::mt19937 engine_;
		std::uniform_int_distribution<size_t> distributer_, name_distributer_;
		ep::plugin_list plugins_;
	};

}