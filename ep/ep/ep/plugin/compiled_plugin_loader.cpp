#include "compiled_plugin_loader.h"
#include "plugin.h"
#include <experimental\filesystem>

ep::compiled_plugin_loader::compiled_plugin_loader( std::vector<std::string> modules )
	: device_( ), engine_( device_( ) ), distributer_( 0, 42 ), name_distributer_( 20, 43 )
{
	auto temporaries = create_temporaries( std::move( modules ) );
	for ( auto &x : temporaries )
		plugins_.emplace_back( ep::load_plugin( x ) );
}

ep::plugin_list ep::compiled_plugin_loader::get_plugins( )
{
	return plugins_;
}

std::string ep::compiled_plugin_loader::random_sequence( )
{
	static std::string alphabet{ ".,^={}()[]%&!@�$�qwertyuiopasdfghjklzxcvbnm" };
	std::string output = "eptemp_";
	auto size = name_distributer_( engine_ );
	for ( auto i = 0u;
		  i < size;
		  ++i )
		output.push_back( alphabet[distributer_( engine_ )] );
	output.append( ".dll" );
	return output;
}

std::vector<std::string> ep::compiled_plugin_loader::create_temporaries( std::vector<std::string> modules )
{
	std::vector<std::string> temporaries;
	for ( auto &x : modules )
		temporaries.emplace_back( write_temporary( x ) );
	return std::move( temporaries );
}

std::string ep::compiled_plugin_loader::write_temporary( std::string & data )
{
	auto path = std::experimental::filesystem::temp_directory_path( );
	auto filename = ( path.string( ) + random_sequence( ) );
	std::ofstream file( filename, std::ios::binary );
	file.write( data.c_str( ), data.size( ) );
	return std::move( filename );
}
