#include "plugin.h"
#include "../ep.h"
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>


ep::plugin::plugin( std::function<void( dispatch_engine_ptr )> load,
					std::function<const char*( )> name,
					std::function<const char*( )> reqs,
					std::string path )
	: loadFn_( std::move( load ) ),
	nameFn_( std::move( name ) ),
	reqFn_( std::move( reqs ) ),
	path_( std::move( path ) )
{	
}

std::function<void( ep::dispatch_engine_ptr )> ep::plugin::get_load_fn( )
{
	return loadFn_;
}

std::function<const char*( )> ep::plugin::get_name_fn( )
{
	return nameFn_;
}

std::function<const char*( )> ep::plugin::get_prerequisites_fn( )
{
	return reqFn_;
}

const std::string & ep::plugin::get_path( ) const
{
	return path_;
}

std::string & ep::plugin::get_path( )
{
	return path_;
}

template<typename T>
auto _fcast( void *f )->
typename ep::details::callable_traits<T>::signature_type
{
	return reinterpret_cast< typename ep::details::callable_traits<T>::signature_type >( f );
}

ep::plugin_ptr ep::load_plugin( const std::string &module )
{
	auto mod = LoadLibraryA( module.c_str( ) );
	return std::make_shared<plugin>( _fcast<void( dispatch_engine_ptr )>( GetProcAddress( mod, "load_plugin" ) ),
									 _fcast<const char*( )>( GetProcAddress( mod, "get_name" ) ),
									 _fcast<const char*( )>( GetProcAddress( mod, "get_prerequisites" ) ),
									 module );
}
