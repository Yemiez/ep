#pragma once
#include "../language/all_defines.h"

namespace ep
{
	
	class plugin
	{
	public:
		plugin( std::function<void( dispatch_engine_ptr )> load,
				std::function<const char*( )> name,
				std::function<const char*( )> preq,
				std::string path );

	public:
		std::function<void( dispatch_engine_ptr )> get_load_fn( );

		std::function<const char*( )> get_name_fn( );

		std::function<const char*( )> get_prerequisites_fn( );

		const std::string &get_path( ) const;

		std::string &get_path( );

	private:
		std::function<void( dispatch_engine_ptr )> loadFn_;
		std::function<const char*( )> nameFn_, reqFn_;
		std::string path_;
	};

	extern plugin_ptr load_plugin( const std::string &module );

}