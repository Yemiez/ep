/********************************************************
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*					main.cpp					*	*
*  	*			   Created by Yamiez				*	*
*  	*	Do not forget to give credit when using		*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
*********************************************************/
#include <functional>
#include <iostream>
#include <memory>
#include <stack>
#include <map>
#include <chrono>
#include <algorithm>
#include <numeric>
#include <iomanip>
#include <Windows.h>
#include "ep\ep.h"


void indent( std::ostream &out, int rec )
{
	for ( auto i = 0; 
		  i < rec; 
		  ++i )
		out << "\t";
}

void code_output( ep::module_ptr &module, ep::statement_list &block, std::ostream &out, int recursion_level = 0 );
void code_output_new_scope( ep::module_ptr &module, ep::statement_list &block, std::ostream &out, int recursion_level );

void code_output_new_scope( ep::module_ptr &module, ep::statement_list &block, std::ostream &out, int recursion_level )
{
	indent( out, recursion_level - 1 );
	out << "{" << std::endl;
	code_output( module, block, out, recursion_level );
	indent( out, recursion_level - 1 );
	out << "}" << std::endl;
}

void method_output( ep::module_ptr &module, std::ostream &out, ep::type_info_ptr info )
{
	for ( auto &x : info->get_methods( ) )
	{
		out << "method " << info->get_name( ) << "::" << x->pretty_format( );
		if ( x->get_statements_size( ) == 0 )
			out << ";\n";
		else
			code_output_new_scope( module, x->get_statements( ), out << "\n", 1 );
		out << "\n";
	}
}

void method_output( ep::module_ptr & module, std::ostream & out )
{ 
	for ( auto &x : module->get_methods( ) )
	{
		ep::type_info_ptr type{ nullptr };
		try
		{
			type = module->get_engine( )->get_type_info( x->get_name( ) );
			out << type->get_name( ) << "::" << x->pretty_format( );
		}
		catch ( ep::undefined_type_error & )
		{
			out << "method " << x->pretty_format( );
		}
		if ( !x->get_statements_size( ) )
			out << ";\n";
		else
			code_output_new_scope( module, x->get_statements( ), out << "\n", 1 );
		out << "\n";

		if ( type )
			method_output( module, out, type );
	}
}

void code_output( ep::module_ptr &module, ep::statement_list &block, std::ostream &out, int recursion_level )
{
	for ( auto &x : block )
	{
		indent( out, recursion_level );
		if ( x->is_if( ) )
		{
			out << "if " << x->get_expression( )->pretty_format( ) << std::endl;
			code_output_new_scope( module, x->get_dups( ), out, recursion_level + 1 );
		}
		else if ( x->is_else_if( ) )
		{
			out << "else if " << x->get_expression( )->pretty_format( ) << std::endl;
			code_output_new_scope( module, x->get_dups( ), out, recursion_level + 1 );
		}
		else if ( x->is_else( ) )
		{
			out << "else" << std::endl;
			code_output_new_scope( module, x->get_dups( ), out, recursion_level + 1 );
		}
		else if ( x->is_while( ) )
		{
			out << "while " << x->get_expression( )->pretty_format( ) << std::endl;
			code_output_new_scope( module, x->get_dups( ), out, recursion_level + 1 );
		}
		else if ( x->is_var_decl( ) )
			out << "var " << x->get_var_decl( ) << ";\n";
		else if ( x->is_var_decl_initalizer( ) )
			out << "auto " << x->get_var_decl( ) << " = " << x->get_expression( )->pretty_format( ) << "\n";
		else if ( x->is_function_call( ) || x->is_expression( ) || x->is_empty_expression( ) )
			out << x->get_expression( )->pretty_format( ) << "\n";
		else if ( x->is_return( ) )
			out << "return " << x->get_expression( )->pretty_format( ) << "\n";
		else if ( x->is_break_statement( ) )
			out << "break;\n";
		else if ( x->is_continue_statement( ) )
			out << "continue;\n";
		else
			out << "<unintepretable code block>\n";
	}
}

void code_output_main( ep::module_ptr &module, ep::statement_list &block, std::ostream &out )
{
	method_output( module, out );
	indent( out, 0 );
	out << "$block\n";
	indent( out, 0 );
	out << "{\n";
	for ( auto &x : block )
	{
		indent( out, 1 );
		if ( x->is_if( ) )
		{
			out << "if " << x->get_expression( )->pretty_format( ) << std::endl;
			code_output_new_scope( module, x->get_dups( ), out, 2 );
		}
		else if ( x->is_else_if( ) )
		{
			out << "else if " << x->get_expression( )->pretty_format( ) << std::endl;
			code_output_new_scope( module, x->get_dups( ), out, 2 );
		}
		else if ( x->is_else( ) )
		{
			out << "else" << std::endl;
			code_output_new_scope( module, x->get_dups( ), out, 2 );
		}
		else if ( x->is_while( ) )
		{
			out << "while " << x->get_expression( )->pretty_format( ) << std::endl;
			code_output_new_scope( module, x->get_dups( ), out, 2 );
		}
		else if ( x->is_var_decl( ) )
			out << "var " << x->get_var_decl( ) << ";\n";
		else if ( x->is_var_decl_initalizer( ) )
			out << "auto " << x->get_var_decl( ) << " = " << x->get_expression( )->pretty_format( ) << "\n";
		else if ( x->is_function_call( ) || x->is_expression( ) || x->is_empty_expression( ) )
			out << x->get_expression( )->pretty_format( ) << "\n";
		else if ( x->is_return( ) )
			out << "return " << x->get_expression( )->pretty_format( ) << "\n";
		else if ( x->is_break_statement( ) )
			out << "break;\n";
		else if ( x->is_continue_statement( ) )
			out << "continue;\n";
		else
			out << "<unintepretable code block>\n";
	}
	indent( out, 0 );
	out << "}\n";
}

std::string get_code( )
{
	return R"(
import ( bo, // operators
         ios // cout
         );
 
method regular_function( regular_argument, &referenced_argument )
{
	if ( regular_argument == referenced_argument )
		return 1;	
	return 0;
};

method 'an identifier with any characters allowed'( x, y )
{
	// do something...
};

group Group
{
members:
	var x;
	var y;
methods:
	Group( &x, &y )
	{
		x = x;
		y = y;
	};

	
methods:
	method '='( &other )
	{
		this->x = other->x;
		this->y = other->y;
	};
};

method invoke( &lambda )
{
	cout << lambda( ) << endl;
};



dyn dynamic_object;
dynamic_object->'()' = method { return 25; };

invoke( dynamic_object );
invoke( dynamic_object->'()' );

)";
}


int main( )
{
	std::locale::global( std::locale( "sv-SE" ) );
	auto module = std::make_shared<ep::module>( );
	
	try
	{
		auto block = module->compile( get_code( ) );

		code_output_main( module, block, std::cout );
		module->eval( block );
		module->get_engine( )->get_global_state( )->reset( );
	}
	catch ( ep::ep_error &err )
	{
		std::cerr << err.pretty_format( );
	}
	std::cin.get( );
}